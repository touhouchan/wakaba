#!/bin/sh
WDIR="/path/to/script/directory"
cd $WDIR
kill -0 $(cat /tmp/wk_thchan.pid) > /dev/null 2>&1
if [ $? -eq 0 ] ; then
    #kill $(cat /tmp/wk_thchan.pid)
    echo "Already running...";
else
    echo "Spawning wakaba..."
    /usr/bin/spawn-fcgi -a 127.0.0.1 -p 9910 -u alice -g alice -f  "./wakaba.pl" -P /tmp/wk_thchan.pid
fi
