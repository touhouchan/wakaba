#!/bin/sh
WDIR="/path/to/script/directory"
cd $WDIR
kill -0 $(cat /tmp/wk_thchan.pid) > /dev/null 2>&1
if [ $? -eq 0 ] ; then
    echo "Killing wakaba..."
    kill $(cat /tmp/wk_thchan.pid)
else
    echo "Already killed...";
fi
