# post include

use constant POST_VIEW_INCLUDE => q{
<if !$parent><div id="post_<var $num>" class="oppost"></if>

<if $parent><table id="post_<var $num>" class="post"><tbody><tr>
<td class="reply" id="reply<var $num>"></if>

<a id="<var $num>"></a>

<div class="post_head">

<label>
<input type="checkbox" name="delete" value="<var $num>" />
<span class="filetitle"><var $subject></span>
<if $email><span class="postername"><a href="<var $email>"><var $name></a></span><if $trip><span class="postertrip"><a href="<var $email>"><var $trip></a></span></if><if $id><span class="posterid">&nbsp;<var $id></span></if></if>
<if !$email><span class="postername"><var $name></span><if $trip><span class="postertrip"><var $trip></span></if><if $id><span class="posterid">&nbsp;<var $id></span></if></if>
<if $admin_post><span class="adminname">## Team ##</span></if>
<if $admin><span class="ipaddr">(IP: <var dec_to_dot($ip)><if $adminpost>; <strong title="Moderator Post">M</strong></if>)</span></if>
<span class="date desktop"><var make_date($timestamp, $board-\>option('DATE_STYLE'), S_WEEKDAYS, S_MONTHS)></span>
<span class="date mobile"><var make_date($timestamp, "2ch", S_WEEKDAYS, S_MONTHS)></span>
</label>

<if $board-\>option('ENABLE_COUNTRY_FLAGS') && $location><img alt="" src="<var get_flag($location)>" title="<var $location>" /></if>
<if $aluhut>
	<span class="ssl" title="<var $aluhut>"><img src="/img/icons/ssl.png" alt="SSL" /></span>
</if>

<span class="reflink">
	<if $parent><a href="<var get_reply_link($parent,0)>#i<var $num>">No.<var $num></a>
	<else><a href="<var get_reply_link($num,0)>#i<var $num>">No.<var $num></a></if>
</span>

<if !$parent>
	<if $sticky><span class="sticky"><img src="/img/icons/pin.png" title="<var S_STICKYTITLE>" alt="Pin" /></span></if>
	<if $locked><span class="locked"><img src="/img/icons/locked.png" title="<var S_LOCKEDTITLE>" alt="Lock" /></span></if>
	<!-- <if !$autosage><if $email><span class="sage"><const S_SAGE></span></if></if> -->
	<if !$sticky && $autosage><span class="sage">Bump-limit</span></if>
</if>&nbsp;

<if !$thread && !$parent>
	<if !$admin>
		<if !$locked>[<a href="<var get_reply_link($num,0)>"><const S_REPLY></a>]
		<else>[<a href="<var get_reply_link($num,0)>"><const S_VIEW></a>]</if>
	<else>
		[<a href="<var get_reply_link($num,0,$admin)>"><const S_REPLY></a>]
	</if>
</if>

<if $admin>
	<if !$parent>	
		<if !$sticky>
			<span class="sticky" title="Sticky"><a href="<var $self>?task=sticky&amp;num=<var $num>&amp;board=<var $board-\>path()>"><img src="/img/icons/sticky.png" alt="" /></a></span>
		<else>
			<span class="unsticky" title="Unsticky"><a href="<var $self>?task=sticky&amp;num=<var $num>&amp;board=<var $board-\>path()>"><img src="/img/icons/unsticky.png" alt="" /></a></span>
		</if>
			
		<if !$locked>
			<span class="lock" title="Lock"><a href="<var $self>?task=lock&amp;num=<var $num>&amp;board=<var $board-\>path()>"><img src="/img/icons/lock.png" alt="" /></a></span>
		<else>
			<span class="unlock" title="Unlock"><a href="<var $self>?task=lock&amp;num=<var $num>&amp;board=<var $board-\>path()>"><img src="/img/icons/unlock.png" alt="" /></a></span>
		</if>
		
		<if !$autosage>
			<span class="lock" title="Autosage"><a href="<var $self>?task=autosage&amp;num=<var $num>&amp;board=<var $board-\>path()>"><img src="/img/icons/sage.png" alt="" /></a></span>
		<else>
			<span class="autosage" title="Unset Autosage"><a href="<var $self>?task=autosage&amp;num=<var $num>&amp;board=<var $board-\>path()>"><img src="/img/icons/unsage.png" alt=""/></a></span>
		</if>
	</if>

	<span class="ban" title="<const S_MPBAN>"><a href="<var $self>?task=banpost&amp;num=<var $num>&amp;board=<var $board-\>path()>" onclick="return do_ban(this)"><img src="/img/icons/ban.png" alt="" /></a></span>
	<span class="deleteall" title="<const S_MPDELETEALL>"><a href="<var $self>?task=deleteall&amp;ip=<var $ip>&amp;board=<var $board-\>path()>" onclick="return areYouSure(this)"><img src="/img/icons/delete_all.png" alt="" /></a></span>
	<span class="delete_file" title="<const S_MPDELFILE>"><a href="<var $self>?task=delete&amp;admindel=1&amp;delete=<var $num>&amp;fileonly=on&amp;board=<var $board-\>path()>" onclick="return areYouSure(this)"><img src="/img/icons/delete_file.png" alt="" /></a></span>
	<span class="delete_post" title="<const S_MPDELETE>"><a href="<var $self>?task=delete&amp;admindel=1&amp;delete=<var $num>&amp;board=<var $board-\>path()>" onclick="return areYouSure(this)"><img src="/img/icons/delete.png" alt="" /></a></span>
	<span class="edit" title="<const S_MPEDIT>"><a href="<var $self>?task=edit&amp;num=<var $num>&amp;board=<var $board-\>path()>"><img src="/img/icons/edit.png" alt="" /></a></span>&nbsp;
</if>

</div>

<div class="post_body">

<if $image>
<div class="file_container">
    <if $thumbnail><div class="file"></if>
    <if !$thumbnail><div class="file filebg"></if>

    <div class="filename"><a target="_blank" title="<var $origname>" href="<var expand_image_filename($image)>/<var get_urlstring($origname)>"><var get_displayname($origname)></a></div>
	<div class="filesize"><!--compat for dollscript--><if $size><a href="<var expand_image_filename($image)>"></a></if><var get_displaysize($size, DECIMAL_MARK)><if $width && $height>, <var $width>&nbsp;&times;&nbsp;<var $height></if><if $meta>, <var $meta></if></div>

    <if $thumbnail>
        <div class="filelink">
		<a target="_blank" href="<var expand_image_filename($image)>" <if get_extension($image)=~/^JPG|PNG|GIF/>onclick="return expand_image(this, <var $width>, <var $height>, <var $tn_width>, <var $tn_height>, '<var expand_filename($thumbnail)>')"</if>>
			<img src="<var expand_filename($thumbnail)>" width="<var $tn_width>" height="<var $tn_height>" alt="<var $size>" />
		</a>
        </div>
    </if>

	<if !$thumbnail>
		<if $board-\>option('DELETED_THUMBNAIL')>
			<a target="_blank" href="<var expand_image_filename(DELETED_IMAGE)>">
			<img src="<var expand_filename($board-\>option('DELETED_THUMBNAIL'))>" width="<var $tn_width>" height="<var $tn_height>" alt="" /></a>
		<else>
			<div class="filetype">
				<a target="_blank" href="<var expand_image_filename($image)>">
					<var get_extension($origname)>
				</a>
			</div>
		</if>
	</if>
    </div>
</div>
</if>

<blockquote class="text">
<if $abbrev>
	<div class="hidden" id="posttext_full_<var $num>">
    	<var $comment_full>
	</div>
</if>
<div id="posttext_<var $num>">
<var $comment>
	<if $abbrev>
		<p class="abbrev">[<a href="<var get_reply_link($num,$parent)>" onclick="return expand_post('<var $num>')"><var $abbrev></a>]</p>
	</if>
</div>
</blockquote>

</div>

<if !$parent>
	<if $omit>
		<span class="omittedposts">
		<if $omitimages><var sprintf S_ABBRIMG,$omit,$omitimages></if>
		<if !$omitimages><var sprintf S_ABBR,$omit></if>
		</span>
	</if>

	<if !$thread>
		<script type="text/javascript">
			if (hiddenThreads.indexOf('t<var $num>,') != -1)
			{
				toggleHidden('t<var $num>');	
			}
		</script>
	</if>
	</div>
</if>

<if $parent>
	</td></tr></tbody></table>
</if>
};

#post backup
use constant POST_BACKUP_INCLUDE => q{
<if !$parent || $standalone><div id="post_<var $postnum>" class="oppost"></if>

<if $parent && !$standalone><table id="post_<var $postnum>" class="post"><tbody><tr>
<td class="reply" id="reply<var $num>"></if>

<a id="<var $postnum>"></a>

<div class="post_head">

<label>
<input type="checkbox" name="num" value="<var $postnum>" />
<span class="filetitle"><var $subject></span>
<if $email><span class="postername"><a href="<var $email>"><var $name></a></span><if $trip><span class="postertrip"><a href="<var $email>"><var $trip></a></span></if><if $id><span class="posterid">&nbsp;<var $id></span></if></if>
<if !$email><span class="postername"><var $name></span><if $trip><span class="postertrip"><var $trip></span></if><if $id><span class="posterid">&nbsp;<var $id></span></if></if>
<if $admin_post><span class="adminname">## Team ##</span></if>
<span class="ipaddr">(IP: <var dec_to_dot($ip)><if $adminpost>; <strong title="Moderator Post">M</strong></if>)</span>
<span class="date desktop"><var make_date($timestamp, $board-\>option('DATE_STYLE'), S_WEEKDAYS, S_MONTHS)></span>
<span class="date mobile"><var make_date($timestamp, "2ch", S_WEEKDAYS, S_MONTHS)></span>
</label>

<if $board-\>option('ENABLE_COUNTRY_FLAGS') && $location><img alt="" src="<var get_flag($location)>" title="<var $location>" /></if>
<if $aluhut>
	<span class="ssl" title="<var $aluhut>"><img src="/img/icons/ssl.png" alt="SSL" /></span>
</if>

<span class="reflink">
	No.<var $postnum>
</span>

<if !$parent>
	<if $sticky><span class="sticky"><img src="/img/icons/pin.png" title="<var S_STICKYTITLE>" alt="Pin" /></span></if>
	<if $locked><span class="locked"><img src="/img/icons/locked.png" title="<var S_LOCKEDTITLE>" alt="Lock" /></span></if>
	<if !$sticky && $autosage><span class="sage">Bump-limit</span></if>
</if>&nbsp;

<if $standalone><span><em>(Orphaned From Parent: <if !$parent><a href="<var Wakaba::get_reply_link($parent)>"><var $parent></a><else><var $parent></if> )</em></span></if>

<if !$thread && !$parent>
<span class="replylink">
	[<a href="<var $self>?task=postbackups&amp;board=<var $board-\>path()>&amp;page=t<var $postnum>"><const S_VIEW></a>]
</span>
</if>

<!-- buttons here -->
<span><a onclick="return areYouSure(this)" href="<var $self>?task=restorebackups&amp;num=<var $postnum>&amp;handle=delete&amp;board=<var $board-\>path()>"><img src="/img/icons/delete.png"></a></span>
<if $standalone or !$parent>
	<span><a onclick="return areYouSure(this)" href="<var $self>?task=restorebackups&amp;num=<var $postnum>&amp;handle=restore&amp;board=<var $board-\>path()>"><img src="/img/icons/expand.png"></a></span>
</if>

</div>

<div class="post_body">

<if $image>
<div class="file_container">
    <if $thumbnail><div class="file"></if>
    <if !$thumbnail><div class="file filebg"></if>

    <div class="filename"><a target="_blank" title="<var $origname>" href="<var expand_image_filename($image)>/<var get_urlstring($origname)>"><var get_displayname($origname)></a></div>
	<div class="filesize"><!--compat for dollscript--><if $size><a href="<var expand_image_filename($image)>"></a></if><var get_displaysize($size, DECIMAL_MARK)><if $width && $height>, <var $width>&nbsp;&times;&nbsp;<var $height></if><if $meta>, <var $meta></if></div>

    <if $thumbnail>
        <div class="filelink">
		<a target="_blank" href="<var expand_image_filename($image)>" <if get_extension($image)=~/^JPG|PNG|GIF/>onclick="return expand_image(this, <var $width>, <var $height>, <var $tn_width>, <var $tn_height>, '<var expand_filename($thumbnail)>')"</if>>
			<img src="<var expand_filename($thumbnail)>" width="<var $tn_width>" height="<var $tn_height>" alt="<var $size>" />
		</a>
        </div>
    </if>

	<if !$thumbnail>
		<if $board-\>option('DELETED_THUMBNAIL')>
			<a target="_blank" href="<var expand_image_filename(DELETED_IMAGE)>">
			<img src="<var expand_filename($board-\>option('DELETED_THUMBNAIL'))>" width="<var $tn_width>" height="<var $tn_height>" alt="" /></a>
		<else>
			<div class="filetype">
				<a target="_blank" href="<var expand_image_filename($image)>">
					<var get_extension($origname)>
				</a>
			</div>
		</if>
	</if>
    </div>
</div>
</if>

<div id="posttext_<var $postnum>">
<blockquote class="text">
<var $comment>
</blockquote>
</div>

</div>

<if !$parent || $standalone>
	<if $omit>
		<span class="omittedposts">
		<if $omitimages><var sprintf S_ABBRIMG,$omit,$omitimages></if>
		<if !$omitimages><var sprintf S_ABBR,$omit></if>
		</span>
	</if>
	</div>
</if>

<if $parent && !$standalone>
	</td></tr></tbody></table>
</if>
};

1;