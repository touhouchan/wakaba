<if !$parent || $standalone>
	<a id="<var $postnum>"></a>

	<label><input type="checkbox" name="num" value="<var $postnum>" />
	<span class="filetitle"><var $subject></span>
	<if $email><span class="postername"><a href="<var $email>"><var $name></a></span><if $trip><span class="postertrip"><a href="<var $email>"><var $trip></a></span></if><if $id><span class="posterid"><var $id></span></if></if>
	<if !$email><span class="postername"><var $name></span><if $trip><span class="postertrip"><var $trip></span></if><if $id><span class="posterid"><var $id></span></if></if> 
	<if $admin_post><span class="adminname">## Team ##</span></if>
	<span class="ipaddr">(IP: <var dec_to_dot($ip)><if $adminpost>; <strong>Moderator Post</strong></if>)</span> 
	<if !$parent>
		<if $sticky><span class="sticky"><img src="/img/icons/pin.png" title="<var S_STICKYTITLE>" alt="Pin" /></span></if>
		<if $locked><span class="locked"><img src="/img/icons/locked.png" title="<var S_LOCKEDTITLE>" alt="Lock" /></span></if>
		<if !$sticky><if $autosage><span class="sage">Bump-limit</span></if></if>
	</if>
	<var get_date($timestamp)></label>
	<if $aluhut>
		<span class="ssl" title="<var $aluhut>"><img src="/img/icons/ssl.png" alt="SSL" /></span>
	</if>
	<span class="reflink">
		No.<var $postnum>
	</span>&nbsp;
	<if $standalone><span><em>(Orphaned From Parent: <a href="<var get_reply_link($parent)>"><var $parent></a> )</em></span></if>
	<if !$thread>
		[<a href="<var $self>?task=postbackups&amp;board=<var $board-\>path()>&amp;page=t<var $postnum>">View</a>]
	</if>

	<if $image>
		<br /><span class="filesize"><const S_PICNAME><a target="_blank" href="<var expand_image_filename($image)>"><var get_filename($image)></a>
		-(<em><var $size> B, <var $width>x<var $height><if $thread and $origname>, <span title="<var clean_string($origname)>"><var show_filename($origname)></span></if></em>)</span>
		<span class="thumbnailmsg"><const S_THUMB></span><br />

		<if $thumbnail>
			<a target="_blank" href="<var expand_image_filename($image)>" >
			<img src="<var expand_filename($thumbnail)>" width="<var $tn_width>" height="<var $tn_height>" alt="<var $size>" class="thumb" id="img<var get_filename($image)>" /></a>
		</if>
		<if !$thumbnail>
			<if $board-\>option('DELETED_THUMBNAIL')>
				<a target="_blank" href="<var expand_image_filename($board-\>option('DELETED_IMAGE'))>">
				<img src="<var expand_filename($board-\>option('DELETED_THUMBNAIL'))>" width="<var $tn_width>" height="<var $tn_height>" alt="" class="thumb" /></a>
			</if>
			<if !($board-\>option('DELETED_THUMBNAIL'))>
				<div class="nothumb"><a target="_blank" href="<var expand_image_filename($image)>"><const S_NOTHUMB></a></div>
			</if>
		</if>
	</if>

	<br />
	<blockquote class="text">
	<var $comment>
	<if $abbrev><div class="abbrev">}.sprintf(S_ABBRTEXT,'<var $self>?task=postbackups&amp;board=<var $board-\>path>&amp;page=t<var $postnum>').q{</div></if>
	</blockquote>

	<if $omit>
		<span class="omittedposts">
			<if $omitimages><var sprintf S_ABBRIMG,$omit,$omitimages></if>
			<if !$omitimages><var sprintf S_ABBR,$omit></if>
		</span>
	</if>
</if>

<if $parent && !$standalone>
	<table><tbody><tr><td class="doubledash">&gt;&gt;</td>
	<td class="reply" id="reply<var $postnum>">

	<a id="<var $postnum>"></a>
	<label><input type="checkbox" name="num" value="<var $postnum>" />
	<span class="replytitle"><var $subject></span>
	<if $email><span class="postername"><a href="<var $email>"><var $name></a></span><if $trip><span class="postertrip"><a href="<var $email>"><var $trip></a></span></if><if $id><span class="posterid"><var $id></span></if></if>
	<if !$email><span class="postername"><var $name></span><if $trip><span class="postertrip"><var $trip></span></if><if $id><span class="posterid"><var $id></span></if></if> 
	<span class="ipaddr">(IP: <var dec_to_dot($ip)><if $adminpost>; <strong>Moderator Post</strong></if>)</span> 
	<var get_date($timestamp)></label>
	<if $aluhut>
		<span class="ssl" title="<var $aluhut>"><img src="/img/icons/ssl.png" alt="SSL" /></span>
	</if>
	<span class="reflink">
		No.<var $postnum>
	</span>&nbsp;
	<if $image>
		<br />
		<span class="filesize"><const S_PICNAME><a target="_blank" href="<var expand_image_filename($image)>"><var get_filename($image)></a>
		-(<em><var $size> B, <var $width>x<var $height><if $thread and $origname>, <span title="<var clean_string($origname)>"><var show_filename($origname)></span></if></em>)</span>
		<span class="thumbnailmsg"><const S_THUMB></span><br />

		<if $thumbnail>
			<a target="_blank" href="<var expand_image_filename($image)>">
			<img src="<var expand_filename($thumbnail)>" width="<var $tn_width>" height="<var $tn_height>" alt="<var $size>" class="thumb" id="img<var get_filename($image)>" /></a>
		</if>
		<if !$thumbnail>
			<if $board-\>option('DELETED_THUMBNAIL')>
				<a target="_blank" href="<var expand_image_filename($board-\>option('DELETED_IMAGE'))>">
				<img src="<var expand_filename($board-\>option('DELETED_THUMBNAIL'))>" width="<var $tn_width>" height="<var $tn_height>" alt="" class="thumb" /></a>
			</if>
			<if !($board-\>option('DELETED_THUMBNAIL'))>
				<div class="nothumb"><a target="_blank" href="<var expand_image_filename($image)>"><const S_NOTHUMB></a></div>
			</if>
		</if>
				</if>

	<blockquote>
	<var $comment>
	<if $abbrev><div class="abbrev">}.sprintf(S_ABBRTEXT,"<var \$self>?task=postbackups&amp;board=<var \$board-\\>path()>&amp;page=t<var \$parent>#<var \$postnum>").q{</div></if>
	</blockquote>

	</td></tr></tbody></table>
</if>