use strict;
use encoding 'utf8';

BEGIN {
	require "lib/post_view.pl";
	require "lib/wakautils.pl";
}

use constant NORMAL_HEAD_INCLUDE => q{

<!DOCTYPE html>
<html lang="ru">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=<const CHARSET>" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title><var strip_html($board-\>option('TITLE'))> &raquo; <if $title><var strip_html($title)></if><if !$title>/<var strip_html($board-\>path())>/ - <var strip_html($board-\>option('BOARD_NAME'))></if></title>
<link rel="shortcut icon" href="<var expand_filename($board-\>option('FAVICON'))>" />

<link type="text/css" rel="stylesheet" href="/js/wakaba.css" />

<loop $stylesheets>
<link rel="<if !$default>alternate </if>stylesheet" type="text/css" href="<var $path><var $filename>" title="<var $title>" />
</loop>

<script type="text/javascript">var style_cookie="<var $board-\>option('STYLE_COOKIE')>"; var thread_cookie = "<var $board-\>path()>_hidden_threads";</script>
<script type="text/javascript" src="/js/jquery/jquery.min.js"></script>
<script type="text/javascript" src="/js/jquery/jquery.cookie.min.js"></script>
<script type="text/javascript" src="/js/jquery/jquery.blockUI.min.js"></script>
<script type="text/javascript" src="/js/wakaba3.js"></script>

<script type="text/javascript">
/* <![CDATA[ */
  $j = jQuery.noConflict();
  $j(document).ready(function() {
    var match;
    if ((match = /#i([0-9]+)/.exec(document.location.toString())) && !document.forms.postform.shampoo.value) insert(">>" + match[1] + "\n");
    if ((match = /#([0-9]+)/.exec(document.location.toString()))) highlight(match[1]);
    $j('#postform').submit(function() {
		$j('.postarea').block({
			message: '<const S_PLEASEWAIT>',
			css: { fontSize: '2em', color: '#000000', background: '#D7CFC0', border: '1px solid #BFB5A1' },
		});
		setTimeout($j.unblockUI, 5000);
    });
	$j('textarea[name="shampoo"]').keydown(function (e) {
	  if (e.ctrlKey && (e.keyCode == 10 || e.keyCode == 13)) {
	    $j('#postform').trigger('submit');
	  }
	});
	<if $thread>
	$j('#delform').delegate('span.reflink a', 'click', function (ev) {
		var a = ev.target,
		sel = window.getSelection().toString();
		ev.preventDefault();
		insert('>>' + a.href.match(/#i(\d+)$/)[1] + '\n' + (sel ? '>' + sel.replace(/\n/g, '\n>') + '\n' : ''));
	});
	</if>
	initz();
  });
/* ]]> */
</script>

<script type="text/javascript">
	var board = '<var $board-\>path()>', thread_id = <if $thread><var $thread></if><if !$thread>null</if>;
</script>

<link rel="alternate" type="application/rss+xml" title="RSS" href="<var expand_filename("board.rss", 1)>" />

</head>
<if $thread><body class="replypage"></if>
<if !$thread><body></if>

}.include("lib/include/header.html").q{

<div class="adminbar">
<span class="navbar">
[ <loop get_boards_list()>
	<if !$admin><if !$is_hidden><a href="/<var $board_entry>/" title="<var $board_name>">/<var $board_entry>/</a> <if !$last>/</if> </if></if>
	<if $admin><a href="<var get_secure_script_name()>?task=mpanel&amp;board=<var $board_entry>" title="<var $board_name>">/<var $board_entry>/</a> <if !$last>/</if> </if>
</loop> ]
</span>
-
[<a href="<var expand_filename(HOME)>" target="_top"><const S_HOME></a>]
[<a href="<var get_secure_script_name()>?task=search&amp;board=<var $board-\>path()>"><const S_SEARCH></a>]
[<a href="<var expand_filename("board.rss")>" title="Local RSS Feed for This Board">RSS</a>]
-
[<a href="javascript:void(0)" onclick="toggleNavMenu(this,0);"><const S_OPTIONS></a>]
</div>

<div class="header">
	<if $board-\>option('SHOWBANNERS')>
	<div class="banner">
		<a href="/<var $board-\>path()>/">
			<img src="/banner/<var $board-\>path()>" alt="<var $board-\>path()>" />
		</a>
	</div>
	</if>
	<div class="boardname" <if $board-\>option('BOARD_DESC')>style="margin-bottom: 5px;"</if>>/<var $board-\>path()>/ &ndash; <var $board-\>option('BOARD_NAME')></div>
	<if $board-\>option('BOARD_DESC')><div class="slogan">&bdquo;<var $board-\>option('BOARD_DESC')>&ldquo;</div></if>
</div><hr />

<div id="overlay">
 <div id="settings" class="settings">
  <div class="settingsTitle">
    <span><const S_STYLES></span>
  </div>
  <hr />

  <div class="settingsStyles">
	<loop $stylesheets>
		[<a href="javascript:set_stylesheet_frame('<var $title>','list')"><var $title></a>]
	</loop>
  </div>
  <hr />
	[<a onclick="javascript:toggleMommy(); return false;" href="#" title="Alt+N/Shift+Alt+N(OPERA)">NSFW</a>]
  <hr />

  <div class="settingsDone">
  	[<a href="javascript:void(0)" onclick="toggleNavMenu(this,1);"><const S_DONE></a>]
  </div>
 </div>
</div>

};

use constant NORMAL_FOOT_INCLUDE => include("lib/include/footer.html").q{
</body></html>
};

#
# Admin header
#

use constant MANAGER_HEAD_INCLUDE => NORMAL_HEAD_INCLUDE.q{

<if $admin>
	<!--[<a href="<var expand_filename($board-\>option('HTML_SELF'))>"><const S_MANARET></a>]-->
	[<a href="<var $self>?task=show&amp;board=<var $board-\>path()>"><const S_MANAPANEL></a>]
	[<a href="<var $self>?task=bans&amp;board=<var $board-\>path()>"><const S_MANABANS></a>]
	[<a href="<var $self>?task=postbackups&amp;board=<var $board-\>path()>"><const S_MANABACKUPS></a>]
	[<a href="<var $self>?task=orphans&amp;board=<var $board-\>path()>"><const S_MANAORPH></a>]
	<if $usertype eq 'admin'>[<a href="<var $self>?task=moders&amp;board=<var $board-\>path()>"><const S_MANAMODS></a>]
	<else>[<a href="<var $self>?task=editmod&amp;username=<var $username>&amp;board=<var $board-\>path()>"><const S_EDITMOD></a>]</if>
	[<a href="<var $self>?task=logout&amp;board=<var $board-\>path()>"><const S_MANALOGOUT></a>]
	<div class="passvalid"><const S_MANAMODE></div>
</if>
};

use constant PAGE_TEMPLATE => compile_template(MANAGER_HEAD_INCLUDE.q{

<if !$lockedthread or $admin>
<if $thread && !$admin>
	[<a href="<var expand_filename($board-\>option('HTML_SELF'))>"><const S_RETURN></a>]
	[<a href="#bottom"><const S_BOTTOM></a>]
	<div class="theader"><const S_POSTING></div>
</if>


<if $postform or $admin>
	<div class="postarea">
	<form id="postform" action="<var $self>" method="post" enctype="multipart/form-data">

	<input type="hidden" name="board" value="<var $board-\>path()>" />
	<input type="hidden" name="task" value="post" />
	<if $admin><input type="hidden" name="adminpost" value="1" /></if>
	<if !$image_inp and !$thread and $board-\>option('ALLOW_TEXTONLY')>
		<input type="hidden" name="nofile" value="1" />
	</if>
	<if $thread><input type="hidden" name="parent" value="<var $thread>" /></if>
	<if $board-\>option('FORCED_ANON')><input type="hidden" name="name" /></if>
	<if SPAM_TRAP><div class="trap"><const S_SPAMTRAP><input type="text" name="name" size="28" /><input type="text" name="link" size="28" /></div></if>

	<table><tbody>
	<if !($board-\>option('FORCED_ANON')) or $admin><tr><td class="postblock"><label for="name"><const S_NAME></label></td><td><input type="text" name="akane" id="name" size="28" /></td></tr></if>
	<tr><td class="postblock"><label for="email"><const S_EMAIL></label></td><td><input type="text" name="nabiki" id="email" size="28" /></td></tr>
	<tr><td class="postblock"><label for="subject"><const S_SUBJECT></label></td><td><input type="text" name="kasumi" id="subject" size="35" />
	<input type="submit" value="<const S_SUBMIT>" /></td></tr>
	<tr><td class="postblock"><label for="comment"><const S_COMMENT></label></td><td><textarea name="shampoo" id="comment" cols="48" rows="6"></textarea></td></tr>

	<if $image_inp or $admin>
		<tr><td class="postblock"><const S_UPLOADFILE></td><td><input type="file" name="file" size="35" />
		<if $textonly_inp>[<label><input type="checkbox" name="nofile" value="on" /><const S_NOFILE> ]</label></if>
		</td></tr>
	</if>

	<tr id="trgetback"><td class="postblock"><const S_GOTO></td>
	<td>
		<label><input name="gb2" value="board" type="radio" /> <const S_GOTO_BOARD></label>
		<label><input name="gb2" value="thread" checked="checked" type="radio" /> <const S_GOTO_THREAD></label>
	</td></tr>

	<if use_captcha($board-\>option('ENABLE_CAPTCHA'), $loc) && !$admin>
		<tr id="trcap"><td class="postblock"><const S_CAPTCHA> (<var $loc>)</td><td><input type="text" name="captcha" size="10" />
		 <img alt="" src="/captcha.pl?key=<var get_captcha_key($thread)>&amp;dummy=<var $dummy>&amp;board=<var $board-\>path()>" onclick="update_captcha(this);return false;" />
		</td></tr>
	</if>

	<tr><td class="postblock"><label for="password"><const S_DELPASS></label></td><td><input type="password" name="password" id="password" size="8" /> <const S_DELEXPL></td></tr>

	<if $admin><tr><td class="postblock"><const S_OPTIONS></td><td>
	[<label><input type="checkbox" name="no_format" value="on" /> <const S_NOFORMAT> ]</label> [<label><input type="checkbox" name="capcode" value="on" /> <const S_CAPCODE> ]</label>
	<br />[<label><input type="checkbox" name="sticky" value="on" /> Sticky ]</label> [<label><input type="checkbox" name="lock" value="on" /> Locked ]</label> [<label><input type="checkbox" name="autosage" value="on" /> Autosage ]</label>
	</td></tr></if>

	<if !$admin><tr><td colspan="2">
	<div class="rules"><var decode('utf8',encode_string((compile_template(include($board-\>path().'/'."rules.html")))-\>(board=\>$board)))></div></td></tr></if>
	</tbody></table></form></div>
	<script type="text/javascript">set_inputs("postform")</script>

</if>
</if>

<if $lockedthread and !$admin>
	[<a href="<var expand_filename($board-\>option('HTML_SELF'))>"><const S_RETURN></a>]
	[<a href="#bottom"><const S_BOTTOM></a>]
	<p style="font-weight:bold;font-size:1.2em"><const S_LOCKEDANNOUNCE></p>
</if>

<if $postform or $admin> <hr /> </if>

<if !$thread>
	<script type="text/javascript">
		var hiddenThreads=get_cookie(thread_cookie);
	</script>
</if>

<form id="delform" action="<var $self>" method="post">

<loop $threads>
	<loop $posts>
		<if !$parent>
			<div id="t<var $num>_info" style="float:left"></div>
			<if !$thread><span id="t<var $num>_display" style="float:right"><a href="javascript:threadHide('t<var $num>')" id="togglet<var $num>"><const S_HIDETHR></a><ins><noscript><br/>(Javascript Required.)</noscript></ins></span></if>
			<div id="t<var $num>" class="thread">
		</if>

		}.POST_VIEW_INCLUDE.q{

	</loop>
	</div>
	<br style="clear:left" /><hr />

	<if $thread><div id="bottom_lnks">
	[<a href="<var expand_filename($board-\>option('HTML_SELF'))>"><const S_RETURN></a>]
	<span id="getnewposts">[<a href="#" onclick="javascript:updateThread(); return false;"><const S_UPDATETHR></a>]</span>
	[<a href="#top"><const S_TOP></a>]</div></if>

</loop>

<table class="userdelete"><tbody><tr><td>
<input type="hidden" name="board" value="<var $board-\>path()>" />
<input type="hidden" name="task" value="delete" />
<if $admin><input type="hidden" name="admindel" value="on" /></if>
<if $thread><input type="hidden" name="parent" value="<var $thread>" /></if>
<const S_REPDEL>[<label><input type="checkbox" name="fileonly" value="on" /><const S_DELPICONLY></label>]<br />
<const S_DELKEY><input type="password" name="password" size="8" />
<input value="<const S_DELETE>" type="submit" /></td></tr></tbody></table>
</form>
<script type="text/javascript">set_delpass("delform")</script>

<if !$thread>
	<table border="1"><tbody><tr><td>

	<if $prevpage>
	<if $admin>
		<form method="get" action="<var $self>">
			<input type="hidden" name="task" value="show" />
			<input type="hidden" name="board" value="<var $board-\>path()>" />
			<input type="hidden" name="page" value="<var $prevpage>" />
			<input value="<const S_PREV>" type="submit" />
		</form>
	<else>
	  <form method="get" action="<var $prevpage_fn>"><input value="<const S_PREV>" type="submit" /></form>
	</if></if>
	<if !$prevpage><const S_FIRSTPG></if>

	</td><td>

	<loop $pages>
		<if !$current>[<a href="<var $filename>"><var $page></a>]</if>
		<if $current>[<var $page>]</if>
	</loop>

	</td><td>

	<if $nextpage>
	<if $admin>
		<form method="get" action="<var $self>">
			<input type="hidden" name="task" value="show" />
			<input type="hidden" name="board" value="<var $board-\>path()>" />
			<input type="hidden" name="page" value="<var $nextpage>" />
			<input value="<const S_NEXT>" type="submit" />
		</form>
	<else>
	  <form method="get" action="<var $nextpage_fn>"><input value="<const S_NEXT>" type="submit" /></form>
	</if></if>
	<if !$nextpage><const S_LASTPG></if>

	</td></tr></tbody></table><br style="clear:both" />
</if>

}.NORMAL_FOOT_INCLUDE);



use constant ERROR_TEMPLATE => compile_template(NORMAL_HEAD_INCLUDE.q{

<h1 style="text-align: center"><var $error>
<if $banned>
<loop $bans>
 Your IP <strong><var $ip></strong>
 <if $showmask>(<var $network>/<var $setbits>)</if> has been banned
 <if $reason>with reason <strong><var $reason></strong></if>.<br />
 <if $expires>This lock will expire on <strong><var make_date($expires, "2ch")></strong>.</if>
 <if !$expires>This lock is valid for an indefinite period.</if><br />
</loop>
<span>Due to this fact, you're not allowed to post now. Please contact admin if you want to post again!</span>
</if>
<br /><br />
<a href="<var escamp($ENV{HTTP_REFERER})>" onclick="javascript:window.history.back(-1);return false;"><const S_RETURN></a><br /><br />
</h1>

}.NORMAL_FOOT_INCLUDE);



#
# Admin pages
#

use constant ADMIN_LOGIN_TEMPLATE => compile_template(MANAGER_HEAD_INCLUDE.q{

<div align="center"><form action="<var $self>" method="post">
<input type="hidden" name="board" value="<var $board-\>path()>" />
<input type="hidden" name="task" value="admin" />
<table><tbody>
<tr><td class="postblock"><const S_ADMINUSER><td><input type="text" name="desu" size="12" value="" /></td></tr>
<tr><td class="postblock"><const S_ADMINPASS><td><input type="password" name="berra" size="12" value="" /></td></tr>
</tbody></table>
<label><input type="checkbox" name="savelogin" /> <const S_MANASAVE></label>
<br />
<select name="nexttask">
<option value="mpanel" <if $login_task eq "mpanel" || !$login_task>selected="selected"</if>><const S_MANAPANEL></option>
<option value="bans" <if $login_task eq "bans">selected="selected"</if>><const S_MANABANS></option>
</select>
<input type="submit" value="<const S_MANASUB>" />
</form></div>

}.NORMAL_FOOT_INCLUDE);


use constant BAN_PANEL_TEMPLATE => compile_template(MANAGER_HEAD_INCLUDE.q{

<div class="dellist"><const S_MANABANS></div>

<div class="postarea">
<table><tbody><tr><td valign="bottom">

<form action="<var $self>" method="post">
<input type="hidden" name="board" value="<var $board-\>path()>" />
<input type="hidden" name="task" value="addip" />
<input type="hidden" name="type" value="ipban" />
<table><tbody>
<tr><td class="postblock"><const S_BANIPLABEL></td><td><input type="text" name="ip" size="24" /></td></tr>
<tr><td class="postblock"><const S_BANMASKLABEL></td><td><input type="text" name="mask" size="24" /></td></tr>
<tr><td class="postblock"><const S_BANCOMMENTLABEL></td><td><input type="text" name="comment" size="16" />
<input type="submit" value="<const S_BANIP>" /></td></tr>
</tbody></table></form>

</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td valign="bottom">

<form action="<var $self>" method="post">
<input type="hidden" name="board" value="<var $board-\>path()>" />
<input type="hidden" name="task" value="addip" />
<input type="hidden" name="type" value="whitelist" />
<table><tbody>
<tr><td class="postblock"><const S_BANIPLABEL></td><td><input type="text" name="ip" size="24" /></td></tr>
<tr><td class="postblock"><const S_BANMASKLABEL></td><td><input type="text" name="mask" size="24" /></td></tr>
<tr><td class="postblock"><const S_BANCOMMENTLABEL></td><td><input type="text" name="comment" size="16" />
<input type="submit" value="<const S_BANWHITELIST>" /></td></tr>
</tbody></table></form>

</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr><tr><td valign="bottom">

<form action="<var $self>" method="post">
<input type="hidden" name="board" value="<var $board-\>path()>" />
<input type="hidden" name="task" value="addstring" />
<input type="hidden" name="type" value="wordban" />
<table><tbody>
<tr><td class="postblock"><const S_BANWORDLABEL></td><td><input type="text" name="string" size="24" /></td></tr>
<tr><td class="postblock"><const S_BANCOMMENTLABEL></td><td><input type="text" name="comment" size="16" />
<input type="submit" value="<const S_BANWORD>" /></td></tr>
</tbody></table></form>

</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td valign="bottom">

<form action="<var $self>" method="post">
<input type="hidden" name="board" value="<var $board-\>path()>" />
<input type="hidden" name="task" value="addstring" />
<input type="hidden" name="type" value="trust" />
<table><tbody>
<tr><td class="postblock"><const S_BANTRUSTTRIP></td><td><input type="text" name="string" size="24" /></td></tr>
<tr><td class="postblock"><const S_BANCOMMENTLABEL></td><td><input type="text" name="comment" size="16" />
<input type="submit" value="<const S_BANTRUST>" /></td></tr>
</tbody></table></form>

</td></tr></tbody></table>
</div><br />

<table style="margin: 0 auto"><tbody>
<tr class="managehead"><const S_BANTABLE></tr>

<loop $bans>
	<if $divider><tr class="managehead"><th colspan="6"></th></tr></if>

	<tr class="row<var $rowtype>">

	<if $type eq 'ipban'>
		<td>IP</td>
		<td><var dec_to_dot($ival1)>/<var dec_to_dot($ival2)></td>
		<td><if $date><var make_date($date, '2ch')></if></td>
	</if>
	<if $type eq 'wordban'>
		<td>Word</td>
		<td><var $sval1></td>
		<td><if $date><var make_date($date, '2ch')></if></td>
	</if>
	<if $type eq 'trust'>
		<td>NoCap</td>
		<td><var $sval1></td>
		<td><if $date><var make_date($date, '2ch')></if></td>
	</if>
	<if $type eq 'whitelist'>
		<td>Whitelist</td>
		<td><var dec_to_dot($ival1)>/<var dec_to_dot($ival2)></td>
		<td><if $date><var make_date($date, '2ch')></if></td>
	</if>

	<td><var $comment></td>
	<td><a href="<var $self>?task=removeban&amp;num=<var $num>&amp;board=<var $board-\>path()>"><const S_BANREMOVE></a></td>
	</tr>
</loop>

</tbody></table><br />

}.NORMAL_FOOT_INCLUDE);

#
# Moders
#

use constant MODER_PANEL_TEMPLATE => compile_template(
    MANAGER_HEAD_INCLUDE . q{

<div class="dellist"><const S_MANAMODS></div>

<div class="postarea">
<table><tbody><tr><td valign="bottom">

<form action="<var $self>" method="post">
<input type="hidden" name="board" value="<var $board-\>path()>" />
<input type="hidden" name="task" value="addmod" />
<table><tbody>
<tr><td class="postblock">Username:</td><td><input type="text" name="usertocreate" size="10" />
<input type="submit" value="Add" /></td></tr>
<tr><td class="postblock">Password:</td><td><input type="password" name="passtocreate" size="10" /></td></tr>
<tr id="managepass"><td class="postblock">Mana Password:</td><td><input type="password" name="mpass" size="10" /></td></tr>
<tr><td class="postblock">Account Type</td><td><select name="account" onchange="if (this.form.account.value=='mod'){document.getElementById('boardselect').style.display='';} else {document.getElementById('boardselect').style.display='none';} if (this.form.account.value=='admin') {document.getElementById('managepass').style.display='';} else {document.getElementById('managepass').style.display='none';}"><option value="mod">Moderator</option><option value="globmod" selected="selected">Global Moderator</option><option value="admin">Administrator</option></select> <input type="submit" value="Create User" /></td></tr>
<tr id="boardselect" style="display:none"><td class="postblock">Controlled Boards</td>
	<td><loop $boards><label><input type="checkbox" name="boards" value="<var $board_entry>" /> <var $board_entry></label><br /></loop></td></tr>
</tbody></table></form>

</td></tr></tbody></table>
</div><br />

<table style="margin: 0 auto"><tbody>
<tr class="managehead"><const S_MODTABLE></tr>

<loop $mods>
	<if $divider><tr class="managehead"><th colspan="9"></th></tr></if>

	<tr class="row<var $rowtype>">

	<td><var $num></td>
	<td><var $username></td>
	<td><var $class></td>
	<td><var make_date($timestamp,'tiny')></td>
	<td><var make_date($lasthit,'tiny')></td>

	<td>
	<if $lastip>
		<var dec_to_dot($lastip)>
	</if>
	<if !$lastip>
		<i>none</i>
	</if>
	</td>

	<td><if $disabled>Yes</if><if !$disabled>No</if></td>

	<td><a href="<var $self>?task=removemod&amp;username=<var $username>&amp;board=<var $board-\>path()>"><const S_BANREMOVE></a>&nbsp;<a href="<var $self>?task=editmod&amp;username=<var $username>&amp;board=<var $board-\>path()>">Edit</a></td>
	</tr>
</loop>

</tbody></table><br />

} . NORMAL_FOOT_INCLUDE
);

use constant EDIT_USER_TEMPLATE => compile_template(MANAGER_HEAD_INCLUDE.q{
<div align="center"><em>Editing details for "<var $$userinfo{username}>"</em></div>
<div class="postarea"><form id="changePass" action="<var $self>" method="post" enctype="multipart/form-data">
	<table><tbody>
	<input type="hidden" name="board" value="<var $board-\>path()>" />
	<input type="hidden" name="task" value="updatemod" />
	<input type="hidden" name="username" value="<var $$userinfo{username}>" />
	<tr><td class="postblock">New Password</td><td><input type="password" name="newpass" size="10" value="" /></td></tr>
		<if $$userinfo{username} eq $username>
			<tr><td class="postblock">Confirm Original Password</td><td><input type="password" name="origpass" size="10" value="" /> <input type="submit" value="Submit" /></td></tr>
		</if>
	<if $$userinfo{username} ne $username>
		<tr><td class="postblock">New Account Type</td><td><select name="newclass"><option value="mod" <if $account eq 'mod'>selected="selected"</if>>Moderator</option><option value="globmod" <if $account eq 'admin'>selected="selected"</if>>Global Moderator</option><option value="admin" <if $account eq 'admin'>selected="selected"</if>>Administrator</option></select></td></tr>
		<tr><td class="postblock">Jurisdiction</td><td><loop $boards><label><input name="boards" type="checkbox" value="<var $board_entry>" <if $underpower>checked="checked"</if> /> <var $board_entry></label><br /></loop></td></tr>
		<tr><td class="postblock">Management Password</td><td><input type="password" name="mpass" size="10" value="" /></td></tr>
		<tr><td class="postblock">Disable</td><td>[<label><input type="checkbox" name="disable" value="1" <if $$userinfo{disabled}>checked="checked"</if> /> True ]</label><input type="submit" value="Submit" /></td></tr>
	</if>
	<if $$userinfo{class} eq 'mod'><tr><td class="postblock">Allowed boards</td><td><loop $boards><var $board_entry> </loop></td></tr></if>
</tbody></table></form></div>
}.NORMAL_FOOT_INCLUDE);

#
# Post editing
#

use constant EDIT_POST_PANEL_TEMPLATE => compile_template(MANAGER_HEAD_INCLUDE.q{

<div class="dellist"><var sprintf S_EDITHEAD,get_reply_link($num,$parent),$num></div>

<div class="postarea">
<form id="postform" action="<var $self>" method="post" enctype="multipart/form-data">
<input type="hidden" name="task" value="doedit" />
<input type="hidden" name="num" value="<var $num>" />
<if $noformat><input type="hidden" name="noformat" value="on" /></if>
<input type="hidden" name="board" value="<var $board-\>path()>" />

<table><tbody>
<if !($board-\>option('FORCED_ANON'))><tr><td class="postblock"><const S_NAME></td><td><input type="text" name="field1" size="28" value="<var clean_string(strip_html($name))>" /></td></tr></if>
<tr><td class="postblock"><label><const S_EMAIL></label></td><td><input type="text" name="nabiki" size="28" /></td></tr>
<tr><td class="postblock"><const S_SUBJECT></td><td><input type="text" name="field3" size="35" value="<var $subject>" />
<input type="submit" value="<const S_SUBMIT>" /></td></tr>
<tr><td class="postblock"><const S_COMMENT></td><td><textarea name="field4" cols="48" rows="4"><if $noformat><var $comment><else><var tag_killa($comment)></if></textarea></td></tr>
<tr><td class="postblock"><const S_OPTIONS></td><td>
<label><input type="checkbox" name="capcode" value="on" <if $mod>checked="checked"</if> /> Capcode</label>
<if !$noformat>| [ <a href="<var $self>?task=edit&amp;num=<var $num>&amp;board=<var $board-\>path()>&amp;noformat=1">No Format</a> ]</if>
</td></tr>
</tbody></table></form></div><hr />

}.NORMAL_FOOT_INCLUDE);

#
# Post search
#

use constant SEARCH_TEMPLATE => compile_template(NORMAL_HEAD_INCLUDE . q{
	[<a href="<var expand_filename($board-\>option('HTML_SELF'))>"><const S_RETURN></a>]

	<div class="postarea">
	<form id="searchform" action="<var $self>" method="post" enctype="multipart/form-data">
	<input type="hidden" name="task" value="search" />
	<input type="hidden" name="board" value="<var $board-\>path()>" />

	<table>
	<tbody>

		<tr><td class="postblock"><label for="search"><const S_SEARCH><br />
		<const S_MINLENGTH></label></td>
		<td><input type="text" name="find" id="search" value="<var $find>" />
		<input value="<const S_SEARCHSUBMIT>" type="submit" />
		</td></tr>

		<tr><td class="postblock"><const S_OPTIONS></td>
		<td>
		<label><input type="checkbox" name="op"      value="1" <if $oponly>checked="checked"</if> /> <const S_SEARCHOP></label><br />
		<label><input type="checkbox" name="subject" value="1" <if $insubject>checked="checked"</if> /> <const S_SEARCHSUBJECT></label><br />
		<!--<label><input type="checkbox" name="files"   value="1" <if $filenames>checked="checked"</if> /> <const S_SEARCHFILES></label><br />-->
		<label><input type="checkbox" name="comment" value="1" <if $comment>checked="checked"</if> /> <const S_SEARCHCOMMENT></label>
		</td></tr>

	</tbody>
	</table>

	</form>
	</div>

	<if $find>
		<hr />
		<var S_SEARCHFOUND> <var $count>
		<if $count><br /><br /></if>
	</if>

	<if $find><form id="sch_form"></if>

	<loop $posts>
		} . POST_VIEW_INCLUDE . q{
	</loop>

	<if $find></form></if>

	<p style="clear: both;"></p>
	<hr />

} . NORMAL_FOOT_INCLUDE);

use constant SINGLE_POST_TEMPLATE => compile_template(q{
<loop $posts>
} . POST_VIEW_INCLUDE . q{
</loop>
});


use constant BACKUP_PANEL_TEMPLATE => compile_template(MANAGER_HEAD_INCLUDE.q{
<div class="dellist"><h2>Trash Bin</h2></div>
<if !POST_BACKUP><p align="center"><em>Wakaba is not set to backup deletions/edits at this time.</em></p></if>
<if POST_BACKUP>
	<p align="center"><em>Wakaba is currently set to purge all posts older than <var &POST_BACKUP_EXPIRE/24/3600> day(s) old.</em></p>
	<hr />

	<if !$thread>
		<table border="1" style="float:left"><tbody><tr><td>

		<if $prevpage ne 'none'><form method="get" action="<var $self>"><input type="hidden" name="task" value="postbackups" /><input type="hidden" name="board" value="<var $board-\>path()>" /><input type="hidden" name="page" value="<var $prevpage>" /><input value="<const S_PREV>" type="submit" /></form></if>
		<if $prevpage eq 'none'><const S_FIRSTPG></if>

		</td><td>

		<loop $pages>
			<if !$current>[<a href="<var $filename>"><var $page></a>]</if>
			<if $current>[<var $page>]</if>
		</loop>

		</td><td>

		<if $nextpage ne 'none'><form method="get" action="<var $self>"><input type="hidden" name="task" value="postbackups" /><input type="hidden" name="board" value="<var $board-\>path()>" /><input type="hidden" name="page" value="<var $nextpage>" /><input value="<const S_NEXT>" type="submit" /></form></if>
		<if $nextpage eq 'none'><const S_LASTPG></if>

		</td></tr></tbody></table>
	</if>
	<if $thread>
		<span style="float:left">[<a href="<var $self>?task=postbackups&amp;board=<var $board-\>path()>">Return to Panel</a>]</span>
	</if>

	<form action="<var $self>" method="post" id="delform">
	<input type="hidden" name="board" value="<var $board-\>path>" />
	<input type="hidden" name="admindelete" value="1" />
	<input type="hidden" name="task" value="restorebackups" />

	<div class="delbuttons" style="float:right">
	<input type="submit" name="handle" value="Restore" />
	<input type="submit" name="handle" value="<const S_MPDELETE>" />
	<input type="reset" value="<const S_MPRESET>" />
	</div>

	<br style="clear:both" />
	<hr />

	<loop $threads>
		<loop $posts>
			}.POST_BACKUP_INCLUDE.q{		
		</loop>
		<br style="clear:left" /><hr />
	</loop>

	<div class="delbuttons" style="float:right">
	<input type="submit" name="handle" value="Restore" />
	<input type="submit" name="handle" value="<const S_MPDELETE>" />
	<input type="reset" value="<const S_MPRESET>" />
	</div>
	</form>

	<if !$thread>
		<table border="1" style="float:left"><tbody><tr><td>

		<if $prevpage ne 'none'><form method="get" action="<var $self>"><input type="hidden" name="task" value="postbackups" /><input type="hidden" name="board" value="<var $board-\>path()>" /><input type="hidden" name="page" value="<var $prevpage>" /><input value="<const S_PREV>" type="submit" /></form></if>
		<if $prevpage eq 'none'><const S_FIRSTPG></if>

		</td><td>

		<loop $pages>
			<if !$current>[<a href="<var $filename>"><var $page></a>]</if>
			<if $current>[<var $page>]</if>
		</loop>

		</td><td>

		<if $nextpage ne 'none'><form method="get" action="<var $self>"><input type="hidden" name="task" value="postbackups" /><input type="hidden" name="board" value="<var $board-\>path()>" /><input type="hidden" name="page" value="<var $nextpage>" /><input value="<const S_NEXT>" type="submit" /></form></if>
		<if $nextpage eq 'none'><const S_LASTPG></if>

		</td></tr></tbody></table>
	</if>

	<br style="clear:both" />
</if>
}.NORMAL_FOOT_INCLUDE);

use constant ORPHANS_PANEL_TEMPLATE => compile_template(MANAGER_HEAD_INCLUDE.q{
<div class="dellist"><var $$locale{S_MANAORPH}> (<var $file_count> Files, <var $thumb_count> Thumbs)</div>

<div class="postarea">

<form action="<var $self>" method="post">

<table><tbody>
	<tr class="managehead"><const S_ORPHTABLE></tr>
	<loop $files>
		<tr class="row<var $rowtype>">
		<td><a target="_blank" href="<var expand_filename($name)>"><const S_MANASHOW></a></td>
		<td><label><input type="checkbox" name="file" value="<var $name>" checked="checked" /><var $name></label></td>
		<td><var make_date($modified, "2ch")></td>
		<td align="right"><var get_displaysize($size, DECIMAL_MARK)></td>
		</tr>
	</loop>
</tbody></table><br />

<loop $thumbs>
	<div class="file">
	<label><input type="checkbox" name="file" value="<var $name>" checked="checked" /><var $name></label><br />
	<var make_date($modified, "2ch")> (<var get_displaysize($size, DECIMAL_MARK)>)<br />
	<img src="<var expand_filename($name)>" alt="" />
	</div>
</loop>

<p style="clear: both;"></p>

<input type="hidden" name="task" value="movefiles" />
<input type="hidden" name="board" value="<var $board-\>path()>" />
<input value="<const S_MPARCHIVE>" type="submit" />
</form>

</div>
}.NORMAL_FOOT_INCLUDE);

#
# RSS
#

use constant RSS_TEMPLATE => compile_template("".q{
<?xml version="1.0"?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:dc="http://purl.org/dc/elements/1.1/">
  <channel>
    <atom:link href="<var expand_filename("board.rss", 1)>" rel="self" type="application/rss+xml" />
    <title><var $board-\>option("TITLE")> /<var $board-\>path()>/: Latest Posts</title>
    <link><var expand_filename("", 1)></link>
    <description>The latest threads and replies in <var $board-\>option("TITLE")> at <var $ENV{SERVER_NAME}>, updated in realtime.</description>
    <pubDate><var $pub_date></pubDate>
    <lastBuildDate><var $pub_date></lastBuildDate>
    <generator>Wakaba + touhouchan</generator>
    <if RSS_WEBMASTER><webMaster><const RSS_WEBMASTER></webMaster></if>
    <docs>http://validator.w3.org/feed/docs/rss2.html</docs>
    <loop $items>
      <item>
        <title>
          Post #<var $num><if !$parent> (New Topic)</if><if $subject>: <var $subject></if>
        </title>
        <if $email && $email =~ /\@/><author><var $email><if $name || $trip> (<var $name><var $trip>)</if><if !$name && !$trip> (<var $board-\>option("S_ANONAME")>)</if></author></if>
        <if !$email || $email !~ /\@/><dc:creator><if $name || $trip><var $name><var $trip></if><if !$name && !$trip><var $board-\>option("S_ANONAME")></if></dc:creator></if>
        <if $comment>
          <description><![CDATA[<var $comment>]]></description>
        </if>
        <if $image>
          <enclosure url="<var expand_filename($image, 1)>" length="<var $size>" type="<var $mime_type>" />
        </if>
        <pubDate><var make_date($timestamp, "http")></pubDate>
        <link><var get_reply_link($num, $parent, 0, 1)></link>
	<guid><var get_reply_link($num, $parent, 0, 1)></guid>
      </item>
    </loop>
  </channel>
</rss>
});

#
# Template-Specific Functions
#

sub get_filename($) { my $path=shift; $path=~m!([^/]+)$!; clean_string($1) }

sub get_flag($) {
		my $country=shift;
		return lc('/img/flags/'.$country.'.png');
}

no encoding;
1;

