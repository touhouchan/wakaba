# config
use utf8;

use constant ADMIN_PASS => 'CHANGEME';			# Admin password. For fucks's sake, change this.
use constant NUKE_PASS => 'CHANGEME';			# Password to nuke a board. Change this too, NOW!
use constant SECRET => 'setmeTOveryveriLONGrandomStringorSomething';				# Cryptographic secret. CHANGE THIS to something totally random, and long.
use constant SQL_DBI_SOURCE => 'DBI:mysql:database=CHANGEME;host=localhost'; # DBI data source string (mysql version, put server and database name in here)
use constant SQL_USERNAME => 'CHANGEME';		# MySQL login name
use constant SQL_PASSWORD => 'CHANGEME';		# MySQL password
use constant USE_TEMPFILES => 1;				# Set this to 1 under Unix and 0 under Windows! (Use tempfiles when creating pages).
#use constant SQL_COUNTERS_TABLE => 'counters'; # Table used for post counters
#use constant SQL_ADMIN_TABLE => 'admin';		# Table used for admin information
#use constant SQL_MODER_TABLE => 'moders';
#use constant SQL_BACKUP_TABLE => '__waka_backup';			# Table backup
use constant CHARSET => 'utf-8';	
use constant HOME => '/';
use constant CSS_DIR => 'css/';
use constant REPLIES_PER_STICKY => 1;   		# Replies shown per sticky thread
use constant REPLIES_PER_LOCKED => 2;    		# Replies per locked

use constant POST_BACKUP=>1;				# 1: Back up posts that are deleted or edited. 0: Do not back up.
use constant POST_BACKUP_EXPIRE => 3600*24*14;		# How long should backups last prior to purging?

use constant HIDDEN_BOARDS => qw(test rf bm);      # Boards hidden from list

use constant ENABLE_RSS => 1;  			 # RSS Feed

# Files
use constant FILEGROUPS => (
	image => 'Images',
	video => 'Videos',
	archive => 'Archive',
	audio => 'Audio',
	doc => 'Documents',
	other => 'Other',
);

use constant GROUPORDER => 'image video archive audio doc other'; # other has to be last

use constant CONVERT_COMMAND => 'convert';
use constant VIDEO_CONVERT_COMMAND => '/opt/ffmpeg/ffmpeg-10bit';

# DNSBL
# use constant ENABLE_DNSBL_CHECK => 1;
# use constant DNSBL_TIMEOUT => '0.1';
use constant DNSBL_INFOS =>[
            ['tor.dnsbl.sectoor.de', ['127.0.0.1']],
            ['torexit.dan.me.uk' ,['127.0.0.100']],
            ];

use constant BOARD_CATORDER => 'Обсуждения|Общее';

'nyan';
