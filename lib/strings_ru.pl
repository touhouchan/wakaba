use utf8;

use constant S_HOME => 'Домой';										# Forwards to home page
use constant S_ADMIN => 'Управление';									# Forwards to Management Panel
use constant S_RETURN => 'Назад';									# Returns to image board
use constant S_POSTING => 'Режим отправки: ответ';					# Prints message in red bar atop the reply screen

use constant S_NAME => 'Имя';										# Describes name field
use constant S_EMAIL => 'E-mail';									# Describes e-mail field
use constant S_SUBJECT => 'Тема';								# Describes subject field
use constant S_SUBMIT => 'Отправить';									# Describes submit button
use constant S_COMMENT => 'Текст';								# Describes comment field
use constant S_UPLOADFILE => 'Файл';								# Describes file field
use constant S_NOFILE => 'Файла нет';									# Describes file/no file checkbox
use constant S_CAPTCHA => 'Капча';							# Describes captcha field
use constant S_DELPASS => 'Пароль';								# Describes password field
use constant S_DELEXPL => '(для удаления поста и файлов)';			# Prints explanation for password box (to the right)
use constant S_SPAMTRAP => 'Оставь эти поля пустыми: ';
use constant S_NOFORMAT => 'Без разметки';
use constant S_OPTIONS => 'Опции';
use constant S_CAPCODE => 'Как админ';

use constant S_REPLY => 'Ответ';											# Prints text for reply link
use constant S_VIEW => 'Просмотр';
use constant S_ABBR => 'Пропущено постов: %d';			# Prints text to be shown when replies are hidden
use constant S_ABBRIMG => 'Пропущено постов: %d, изображений: %d';	# Prints text to be shown when replies and images are hidden
use constant S_ABBRTEXT => 'Комментарий слишком длинный. Нажмите <a href="%s">здесь</a> чтобы увидеть его полностью.';
use constant S_BANNED  => '<p class="ban">(Потребитель был запрещен для этого столба)</p>';
use constant S_ALLOWED => 'Разрешенные типы файлов (Лимит: %s)';

use constant S_ABBRTEXT1 => 'Еще 1 строка';
use constant S_ABBRTEXT2 => 'Еще %d строк';

use constant S_REPDEL => 'Удалить пост ';							# Prints text next to S_DELPICONLY (left)
use constant S_DELPICONLY => 'Только файл';							# Prints text next to checkbox for file deletion (right)
use constant S_DELKEY => 'Пароль ';								# Prints text next to password field for deletion (left)
use constant S_DELETE => 'Удалить';									# Defines deletion button's name

use constant S_PREV => 'Предыдущая';									# Defines previous button
use constant S_FIRSTPG => 'Предыдущая';								# Defines previous button
use constant S_NEXT => 'Следующая';										# Defines next button
use constant S_LASTPG => 'Следующая';									# Defines next button

use constant S_SEARCHTITLE		=> 'Поиск';
use constant S_SEARCH			=> 'Поиск';
use constant S_SEARCHCOMMENT	=> 'Поиск по комментарию';
use constant S_SEARCHSUBJECT	=> 'Поиск по теме';
use constant S_SEARCHFILES		=> 'Поиск по файлу';
use constant S_SEARCHOP			=> 'Искать только ОП-посты';
use constant S_SEARCHSUBMIT		=> 'Поиск';
use constant S_SEARCHFOUND		=> 'Найдено:';
use constant S_OPTIONS			=> 'Опции';
use constant S_MINLENGTH		=> '(мин. 3 символа)';

use constant S_WEEKDAYS => 'Вск Пон Втр Срд Чтв Птн Сбт'; # Defines abbreviated weekday names.
use constant S_MONTHS => 'Январь Февраль Март Апрель Май Июнь Июль Август Сентябрь Октябрь Ноябрь Декабрь';

use constant S_MANARET => 'Назад';										# Returns to HTML file instead of PHP--thus no log/SQLDB update occurs
use constant S_MANAMODE => 'Режим управления';								# Prints heading on top of Manager page

use constant S_ADMINPASS => 'Пароль:';							# Prints login prompt
use constant S_ADMINUSER => 'Имя:';							# Prints login prompt

use constant S_MANAPANEL => 'Панель управления';							# Defines Management Panel radio button--allows the user to view the management panel (overview of all posts)
use constant S_MANABANS => 'Баны/Белый список';							# Defines Bans Panel button
use constant S_MANABACKUPS => 'Удаленные';
use constant S_MANAMODS => 'Модераторы';							# Defines Bans Panel button
use constant S_MANAORPH => 'Потерянные файлы';									# 
use constant S_MANALOGOUT => 'Выйти';									# 
use constant S_MANASAVE => 'Запомнить меня на этой машине';				# Defines Label for the login cookie checbox
use constant S_MANASUB => 'Панеслася!';									# Defines name for submit button in Manager Mode

use constant S_MPDELETE => 'Удалить';									# Defines for deletion button in Management Panel
use constant S_MPEDIT => 'Редактировать';    # Defines for deletion button in Management Panel
use constant S_MPARCHIVE => 'Архив';
use constant S_MPRESET => 'Сброс';										# Defines name for field reset button in Management Panel
use constant S_MPDELETEALL => 'Удалить&nbsp;все';							# 
use constant S_MPBAN => 'Бан';											# Sets whether or not to delete only file, or entire post/thread
use constant S_IMGSPACEUSAGE => '[ Используется места: %d KB ]';				# Prints space used KB by the board under Management Panel


use constant S_MODTABLE => '<th>ID</th><th>Имя</th><th>Класс</th><th>Дата</th><th>Посл. вход</th><th>Посл. IP</th><th>Отключен</th><th>Действие</th>';          # Explains names for Moderators Panel

use constant S_BANTABLE => '<th>Тип</th><th>Значение</th><th>Дата</th><th>Текст</th><th>Действие</th>'; # Explains names for Ban Panel
use constant S_BANIPLABEL => 'IP';
use constant S_BANMASKLABEL => 'Маска';
use constant S_BANCOMMENTLABEL => 'Текст';
use constant S_BANWORDLABEL => 'Слово';
use constant S_BANIP => 'Бан по IP';
use constant S_BANWORD => 'Бан по слову';
use constant S_BANWHITELIST => 'Белый список';
use constant S_BANREMOVE => 'Удалить';
use constant S_BANCOMMENT => 'Текст';
use constant S_BANTRUST => 'Без каптчи';
use constant S_BANTRUSTTRIP => 'Tripcode';

use constant S_TOOBIG => 'Изображение слишком большое! Залей что-нибудь поменьше!';
use constant S_TOOBIGORNONE => 'Либо изображение слишком большое, либо его вообще не было. Ага';
use constant S_NOCAPTCHA => 'Нет этого кода подтверждения, наверное он просрочен';	# Returns error when there's no captcha in the database for this IP/key
use constant S_BADCAPTCHA => 'Код подтверждения неверен';		# Returns error when the captcha is wrong
use constant S_BADFORMAT => 'Формат файла не поддерживается';			# Returns error when the file is not in a supported format.
use constant S_STRREF => 'Не принимаю строку';							# Returns error when a string is refused
use constant S_UNJUST => 'Хреновый POST';								# Returns error on an unjust POST - prevents floodbots or ways not using POST method?
use constant S_NOPIC => 'Файл не выбран. Забыл клацнуть "Ответ"?';	# Returns error for no file selected and override unchecked
use constant S_NOTEXT => 'Текст не введён';						# Returns error for no text entered in to subject/comment
use constant S_TOOLONG => 'Слишком много символов в текстовом поле';		# Returns error for too many characters in a given field
use constant S_NOTALLOWED => 'Отправка не разрешена';					# Returns error for non-allowed post types
use constant S_UNUSUAL => 'Аномальный ответ';							# Returns error for abnormal reply? (this is a mystery!)
use constant S_BADHOST => 'Бан';							# Returns error for banned host ($badip string)
use constant S_BADHOSTPROXY => 'Прокси';	# Returns error for banned proxy ($badip string)
use constant S_RENZOKU => 'Обнаружен флуд, пост отвергнут';			# Returns error for $sec/post spam filter
use constant S_RENZOKU2 => 'Обнаружен флуд, файл отвергнут';		# Returns error for $sec/upload spam filter
use constant S_RENZOKU3 => 'Обнаружен флуд';						# Returns error for $sec/similar posts spam filter.
use constant S_RENZOKU4 => 'Попробуйте создать тред через %d минут';						# Returns error for $sec/similar posts spam filter.
use constant S_DUPE => 'Файл уже залит <a href="%s">здесь</a>';	# Returns error when an md5 checksum already exists.
use constant S_NOTHREADERR => 'Треда не существует';				# Returns error when a non-existant thread is accessed
use constant S_BADDELPASS => 'Неверный пароль для удаления';		# Returns error for wrong password (when user tries to delete file)
use constant S_BADDELIP => 'Неверный IP для удаления';		# Returns error for wrong password (when user tries to delete file)
use constant S_WRONGPASS => 'Неверный пароль на управление';		# Returns error for wrong password (when trying to access Manager modes)
use constant S_NOTWRITE => 'Не могу писать в каталог';				# Returns error when the script cannot write to the directory, the chmod (777) is wrong
use constant S_SPAM => 'Спамеры идут лесом';					# Returns error when detecting spam

use constant S_SQLCONF => 'Ошибка подключения SQL';							# Database connection failure
use constant S_SQLFAIL => 'Критическая ошибка SQL!';							# SQL Failure

use constant S_GOTO => 'Перейти к';
use constant S_GOTO_BOARD => 'доске';
use constant S_GOTO_THREAD => 'треду';
use constant S_PLEASEWAIT => 'Ожидайте &hellip;';

use constant S_THREADTITLE => 'Тред No.%d';

use constant S_STICKYTITLE => 'Прикреплен';
use constant S_LOCKEDTITLE => 'Закрыт';
use constant S_THREADLOCKED => 'Тред закрыт.';
use constant S_LOCKEDANNOUNCE => "Этот тред закрыт. Вы не можете отвечать в этот тред.";		# An announcement that appears in place of the post form in a locked thread

use constant S_INSUFF => 'Недостаточно привилегий.';

# use constant S_MAINTENANCE => 'Sex with sql... wait..';
use constant S_EDITHEAD => 'Редактирование поста <a href="%s">No.%d</a>';

use constant S_STOP_FOOLING => 'Haxx0r';
use constant S_INVALID_PAGE => 'Неверная страница';

use constant S_TOP => 'Вверх';
use constant S_BOTTOM => 'Вниз';
use constant S_DONE => 'Готово';
use constant S_UPDATETHR => 'Обновить тред';
use constant S_HIDETHR => 'Скрыть тред (&minus;)';
use constant S_STYLES => 'Стили';

use constant S_EDITMOD => 'Аккаунт';
use constant S_ORPHTABLE => '<th>Ссылка</th><th>Файл</th><th>Дата изменения</th><th>Размер</th>';


use constant S_DNSBL => 'Your IP was found in the Blacklist %s';

1;
