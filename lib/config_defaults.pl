use strict;

BEGIN {
	use constant S_NOADMIN => 'No ADMIN_PASS or NUKE_PASS defined in the configuration';	# Returns error when the config is incomplete
	use constant S_NOSECRET => 'No SECRET defined in the configuration';		# Returns error when the config is incomplete
	use constant S_NOSQL => 'No SQL settings defined in the configuration';		# Returns error when the config is incomplete

	die S_NOADMIN unless(defined &ADMIN_PASS);
	die S_NOADMIN unless(defined &NUKE_PASS);
	die S_NOSECRET unless(defined &SECRET);
	die S_NOSQL unless(defined &SQL_DBI_SOURCE);
	die S_NOSQL unless(defined &SQL_USERNAME);
	die S_NOSQL unless(defined &SQL_PASSWORD);

	eval "use constant SQL_TABLE => 'comments'" unless(defined &SQL_TABLE);
	eval "use constant SQL_ADMIN_TABLE => 'admin'" unless(defined &SQL_ADMIN_TABLE);
	eval "use constant SQL_MODER_TABLE => 'moders'" unless(defined &SQL_MODER_TABLE);
	eval "use constant SQL_COMMON_SITE_TABLE => 'board_index'" unless(defined &SQL_COMMON_SITE_TABLE);
	eval "use constant SQL_BACKUP_TABLE => '__waka_backup'" unless(defined &SQL_BACKUP_TABLE);
	eval "use constant SQL_COUNTERS_TABLE => 'counters'" unless (defined &SQL_COUNTERS_TABLE);

	eval "use constant ENABLE_CAPTCHA => 1" unless(defined &ENABLE_CAPTCHA);
	eval "use constant CAPTCHA_ALWAYS_ON => 0" unless(defined &CAPTCHA_ALWAYS_ON);
	eval "use constant SQL_CAPTCHA_TABLE => 'captcha'" unless(defined &SQL_CAPTCHA_TABLE);
	eval "use constant CAPTCHA_LIFETIME => 1440" unless(defined &CAPTCHA_LIFETIME);
	eval "use constant CAPTCHA_SCRIPT => 'captcha.pl'" unless(defined &CAPTCHA_SCRIPT);
	eval "use constant CAPTCHA_HEIGHT => 18" unless(defined &CAPTCHA_HEIGHT);
	eval "use constant CAPTCHA_SCRIBBLE => 0.2" unless(defined &CAPTCHA_SCRIBBLE);
	eval "use constant CAPTCHA_SCALING => 0.15" unless(defined &CAPTCHA_SCALING);
	eval "use constant CAPTCHA_ROTATION => 0.3" unless(defined &CAPTCHA_ROTATION);
	eval "use constant CAPTCHA_SPACING => 2.5" unless(defined &CAPTCHA_SPACING);

	eval "use constant ERRORLOG => ''" unless (defined &ERRORLOG);
	eval "use constant HOME => '/'" unless (defined &HOME);
	eval "use constant CSS_DIR => 'css/'" unless(defined &CSS_DIR);
	eval "use constant DECIMAL_MARK => ','" unless (defined &DECIMAL_MARK);

	eval "use constant CONVERT_COMMAND => ''" unless (defined &CONVERT_COMMAND);
	eval "use constant VIDEO_CONVERT_COMMAND => ''" unless (defined &VIDEO_CONVERT_COMMAND);

	eval "use constant USE_TEMPFILES => 1" unless(defined &USE_TEMPFILES);
	
	eval "use constant USE_SECURE_ADMIN => 1" unless (defined &USE_SECURE_ADMIN);
	eval "use constant CHARSET => 'utf-8'" unless (defined &CHARSET);
	eval "use constant CONVERT_CHARSETS => 1" unless (defined &CONVERT_CHARSETS);

	eval "use constant PAGE_EXT => '.html'" unless(defined &PAGE_EXT);

	eval "use constant ENABLE_POST_BACKUP => 1" unless defined (&ENABLE_POST_BACKUP);
	eval "use constant POST_BACKUP_EXPIRE => 3600*24*14" unless defined (&POST_BACKUP_EXPIRE);

	eval "use constant REPLIES_PER_STICKY => 1" unless(defined &REPLIES_PER_STICKY);
	eval "use constant REPLIES_PER_LOCKED => 2" unless(defined &REPLIES_PER_LOCKED);

	eval "use constant MAX_SEARCH_RESULTS => 200" unless (defined &MAX_SEARCH_RESULTS);

	eval "use constant MAX_FCGI_LOOPS => 250" unless (defined &MAX_FCGI_LOOPS);
	eval "use constant SERVER_CONCURRENCY => 4" unless (defined &SERVER_CONCURRENCY);

	eval "use constant HIDDEN_BOARDS => ''" unless (defined &HIDDEN_BOARDS);

	eval "use constant ENABLE_RSS => 1" unless (defined &ENABLE_RSS);
	eval "use constant RSS_LENGTH => 10" unless defined (&RSS_LENGTH);
	eval "use constant RSS_WEBMASTER => ''" unless defined (&RSS_WEBMASTER);

	eval "use constant ENABLE_DNSBL_CHECK => 1" unless(defined &ENABLE_DNSBL_CHECK);
	eval "use constant DNSBL_TIMEOUT => '0.1'" unless(defined &DNSBL_TIMEOUT);
	eval "use constant DNSBL_INFOS => [ ['tor.dnsbl.sectoor.de', ['127.0.0.1']], ['torexit.dan.me.uk', ['127.0.0.100']], ]" unless(defined &DNSBL_INFOS);

	eval "use constant BOARD_CATORDER => 'Обсуждения|Общее'" unless(defined &BOARD_CATORDER);

	eval "use constant FILEGROUPS => ( image => 'Images', video => 'Videos', archive => 'Archive', audio => 'Audio', doc => 'Documents', other => 'Other', )"
		unless(defined &FILEGROUPS);

	eval "use constant GROUPORDER => 'image video archive audio doc other'" unless(defined &GROUPORDER);

	eval "use constant WAKABA_VERSION => '3.0.8'" unless(defined &WAKABA_VERSION);
}

1;
