var postByNum = [];
var ajaxPosts = [];
var ajaxThrds = {};
var refMap = [];
var Posts = [];
var opPosts = [];
var pView, dForm;
var style_cook = window.style_cookie;

var lang = 'ru'; // change to en if needed
var consts = {
	en: {
		newPostsNotFound: "Now new posts found.",
		pNotFound: "Post not found",
		updthr: "Update thread",
		load: "Loading...",
		thrhide: "Hide thread (\u2212)",
		thrshow: "Show thread (+)",
		thread: "Thread",
		hidden: "hidden",
		replies: "Replies: "
	},
	ru: {
		newPostsNotFound: "Новые посты не найдены.",
		pNotFound: "Пост не найден",
		updthr: "Обновить тред",
		load: "\u0417агрузка...",
		thrhide: "Скрыть тред (\u2212)",
		thrshow: "Пока\u0437ать тред (+)",
		thread: "Тред",
		hidden: "скрыт",
		replies: "Ответы: "
	}
}

//==================================================================================================
// WAKABA COMMON STUFF

function get_cookie(name)
{
	with(document.cookie)
	{
		var regexp=new RegExp("(^|;\\s+)"+name+"=(.*?)(;|$)");
		var hit=regexp.exec(document.cookie);
		if(hit&&hit.length>2) return unescape(hit[2]);
		else return '';
	}
};

function set_cookie(name,value,days)
{
	if(days)
	{
		var date=new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires="; expires="+date.toGMTString();
	}
	else expires="";
	document.cookie=name+"="+value+expires+"; path=/";
}

function get_password(name)
{
	var pass=get_cookie(name);
	if(pass) return pass;

	var chars="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	var pass='';

	for(var i=0;i<8;i++)
	{
		var rnd=Math.floor(Math.random()*chars.length);
		pass+=chars.substring(rnd,rnd+1);
	}

	return(pass);
}



function insert(text)
{
	var textarea=document.forms.postform.shampoo;
	if(textarea)
	{
		if(textarea.createTextRange && textarea.caretPos) // IE
		{
			var caretPos=textarea.caretPos;
			caretPos.text=caretPos.text.charAt(caretPos.text.length-1)==" "?text+" ":text;
		}
		else if(textarea.setSelectionRange) // Firefox
		{
			var start=textarea.selectionStart;
			var end=textarea.selectionEnd;
			textarea.value=textarea.value.substr(0,start)+text+textarea.value.substr(end);
			textarea.setSelectionRange(start+text.length,start+text.length);
		}
		else
		{
			textarea.value+=text+" ";
		}
		textarea.focus();
	}
}

function highlight(post)
{
	var cells=document.getElementsByTagName("td");
	for(var i=0;i<cells.length;i++) if(cells[i].className=="reply highlight") cells[i].className="reply";

	var reply=document.getElementById("reply"+post);
	if(reply)
	{
		reply.className="reply highlight";
/*		var match=/^([^#]*)/.exec(document.location.toString());
		document.location=match[1]+"#"+post;*/
		return false;
	}

	return true;
}



function set_stylesheet_frame(styletitle,framename)
{
	set_stylesheet(styletitle);
	var list = get_frame_by_name(framename);
	if(list) set_stylesheet(styletitle,list);
}

function set_stylesheet(styletitle,target)
{
	set_cookie("wakastyle",styletitle,365);

	var links = target ? target.document.getElementsByTagName("link") : document.getElementsByTagName("link");
	var found=false;
	for(var i=0;i<links.length;i++)
	{
		var rel=links[i].getAttribute("rel");
		var title=links[i].getAttribute("title");
		if(rel.indexOf("style")!=-1&&title)
		{
			links[i].disabled=true; // IE needs this to work. IE needs to die.
			if(styletitle==title) { links[i].disabled=false; found=true; }
		}
	}
	if(!found)
	{
		if(target) set_preferred_stylesheet(target);
		else set_preferred_stylesheet();
	}
}

function set_preferred_stylesheet()
{
	var links=document.getElementsByTagName("link");
	for(var i=0;i<links.length;i++)
	{
		var rel=links[i].getAttribute("rel");
		var title=links[i].getAttribute("title");
		if(rel.indexOf("style")!=-1&&title) links[i].disabled=(rel.indexOf("alt")!=-1);
	}
}

function get_active_stylesheet()
{
	var links=document.getElementsByTagName("link");
	for(var i=0;i<links.length;i++)
	{
		var rel=links[i].getAttribute("rel");
		var title=links[i].getAttribute("title");
		if(rel.indexOf("style")!=-1&&title&&!links[i].disabled) return title;
	}
	return null;
}

function get_preferred_stylesheet()
{
	var links=document.getElementsByTagName("link");
	for(var i=0;i<links.length;i++)
	{
		var rel=links[i].getAttribute("rel");
		var title=links[i].getAttribute("title");
		if(rel.indexOf("style")!=-1&&rel.indexOf("alt")==-1&&title) return title;
	}
	return null;
}

function get_frame_by_name(name)
{
	var frames = window.parent.frames;
	for(i = 0; i < frames.length; i++)
	{
		if(name == frames[i].name) { return(frames[i]); }
	}
}

function set_inputs(id) { 
	var form=$id(id);
	var gb2 = form.gb2;
	var gb2val = get_cookie("gb2");

	if (typeof gb2 == "object" && gb2 && gb2val)
		for (var i = 0; i < gb2.length; i++) {
			gb2[i].checked = (gb2[i].value == gb2val);
		}

	with(form) {
		if((typeof akane == "object") && (!akane.value)) akane.value=get_cookie("name");
		if((typeof nabiki == "object") && (!nabiki.value)) nabiki.value=get_cookie("email");
		// if (typeof gb2 == "object")	gb2[1].checked = (get_cookie("gb2") == "thread");
		if(!password.value) password.value=get_password("password");
	}
}
function set_delpass(id) { with(document.getElementById(id)) password.value=get_cookie("password"); }

function do_ban(el)
{
	var reason=prompt("Give a reason for this ban:");
	if(reason) document.location=el.href+"&comment="+encodeURIComponent(reason);
	return false;
}


// =================================

window.onunload=function(e)
{
	if(style_cook)
	{
		var title=get_active_stylesheet();
		set_cookie(style_cook,title,365);
	}
}

var initz = function() {
	checkIn(window.board);
	scriptCSS();
	document.onkeydown = hotkeyMommy;
	th_buttons();

	// if($j('textarea[name="shampoo"]').length > 0) {
	//	$j('textarea[name="shampoo"]').click(captcha_required);
	//}

	dForm = ($id('delform') || $id('sch_form'));
	if(!dForm) return;
	$each($c('oppost'), function(post) {
		opPosts.push(post);
		var pNum = post.id.match(/\d+/);
		post.Num = pNum;
		postByNum[pNum] = post;
		
	});
	$each($c('post'), function(post) {
		Posts.push(post);
		var pNum = post.id.match(/\d+/);
		post.Num = pNum;
		postByNum[pNum] = post;
	});
	eventPostPreview();
	addRefMap();
};

function load_style()
{
	if(style_cook)
	{
		var cookie=get_cookie(style_cook);
		var title=cookie?cookie:get_preferred_stylesheet();
		set_stylesheet(title);
	}
}

load_style();

//==================================================================================================
// COMMON
function $id(id) {
	return document.getElementById(id);
}
function $n(id) {
	return document.getElementsByName(id)[0];
}
function $t(id, root) {
	return (root || document).getElementsByTagName(id);
}
function $c(id, root) {
	return (root || document).getElementsByClassName(id);
}
function $each(arr, fn) {
	for(var el, i = 0; el = arr[i++];)
		fn(el);
} 
function $html(el, htm) {
	var cln = el.cloneNode(false);
	cln.innerHTML = htm;
	el.parentNode.replaceChild(cln, el);
	return cln;
}
function $attr(el, attr) {
	for(var key in attr) {
		if(key == 'text') { el.textContent = attr[key]; continue; }
		if(key == 'value') { el.value = attr[key]; continue; }
		if(key == 'html') { el.innerHTML = attr[key]; continue; }
		el.setAttribute(key, attr[key]);
	}
	return el;
}
function $event(el, events) {
	for(var key in events)
		el.addEventListener(key, events[key], false);
}
function $before(el, nodes) {
	for(var i = 0, len = nodes.length; i < len; i++)
		if(nodes[i]) el.parentNode.insertBefore(nodes[i], el);
}
function $after(el, nodes) {
	var i = nodes.length;
	while(i--) if(nodes[i]) el.parentNode.insertBefore(nodes[i], el.nextSibling);
}
function $new(tag, attr, events) {
	var el = document.createElement(tag);
	if(attr) $attr(el, attr);
	if(events) $event(el, events);
	return el;
}
function $disp(el) {
	el.style.display = el.style.display == 'none' ? '' : 'none';
}
function $del(el) {
	if(el) el.parentNode.removeChild(el);
}
function $offset(el, xy) {
	var c = 0;
	while(el) { c += el[xy]; el = el.offsetParent; }
	return c;
}
function $close(el) {
	if(!el) return;
	var h = el.clientHeight - 18;
	el.style.height = h + 'px';
	var i = 8;
	var closing = setInterval(function() {
		if(!el || i-- < 0) { clearInterval(closing); $del(el); return; }
		var s = el.style;
		s.opacity = i/10;
		s.paddingTop = parseInt(s.paddingTop) - 1 + 'px';
		s.paddingBottom = parseInt(s.paddingBottom) - 1 + 'px';
		var hh = parseInt(s.height) - h/10;
		s.height = (hh < 0 ? 0 : hh) + 'px';
	}, 35);
}
function $show(el) {
	var i = 0;
	var showing = setInterval(function() {
		if(!el || i++ > 8) { clearInterval(showing); return; }
		var s = el.style;
		s.opacity = i/10;
		s.paddingTop = parseInt(s.paddingTop) + 1 + 'px';
		s.paddingBottom = parseInt(s.paddingBottom) + 1 + 'px';
	}, 35);
}

function $byattr(attr)
{
  return document.querySelectorAll('[' + attr + ']');
}

//==================================================================================================
// THREAD HIDING

function threadHide(id)
{
	toggleHidden(id);
	add_to_thread_cookie(id);
}

function threadShow(id)
{
	document.getElementById(id).style.display = "";
	
	var threadInfo = id + "_info";
	var parentform = document.getElementById("delform");
	var obsoleteinfo = document.getElementById(threadInfo);
	obsoleteinfo.setAttribute("id","");
	var clearedinfo = document.createElement("div");
	clearedinfo.style.cssFloat = "left";
	clearedinfo.style.styleFloat = "left"; // Gee, thanks, IE.
	parentform.replaceChild(clearedinfo,obsoleteinfo);
	clearedinfo.setAttribute("id",threadInfo);
	
	var hideThreadSpan = document.createElement("span");
	var hideThreadLink = document.createElement("a");
	hideThreadLink.setAttribute("href","javascript:threadHide('"+id+"')");
	var hideThreadLinkText = document.createTextNode(consts[lang].thrhide);
	hideThreadLink.appendChild(hideThreadLinkText);
	hideThreadSpan.appendChild(hideThreadLink);
	
	var oldSpan = document.getElementById(id+"_display");
	oldSpan.setAttribute("id","");
	parentform.replaceChild(hideThreadSpan,oldSpan);
	hideThreadLink.setAttribute("id","toggle"+id);
	hideThreadSpan.setAttribute("id",id+"_display");
	hideThreadSpan.style.cssFloat = "right";
	hideThreadSpan.style.styleFloat = "right";
	
	remove_from_thread_cookie(id);
}

function add_to_thread_cookie(id)
{
	var hiddenThreadArray = get_cookie(thread_cookie);
	if (hiddenThreadArray.indexOf(id + ",") != -1)
	{			
		return;
	}
	else
	{
		set_cookie(thread_cookie, hiddenThreadArray + id + ",", 365);
	}
}

function remove_from_thread_cookie(id)
{
	var hiddenThreadArray = get_cookie(thread_cookie);
	var myregexp = new RegExp(id + ",", 'g');
	hiddenThreadArray = hiddenThreadArray.replace(myregexp, "");
	set_cookie(thread_cookie, hiddenThreadArray, 365);
}

function toggleHidden(id)
{
	var id_split = id.split("");
	if (id_split[0] == "t")
	{
		id_split.reverse();
		var shortenedLength = id_split.length - 1;
		id_split.length = shortenedLength;
		id_split.reverse();
	}
	else
	{
		id = "t" + id; // Compatibility with an earlier mod
	}
	if (document.getElementById(id))
	{
		document.getElementById(id).style.display = "none";
	}
	var thread_name = id_split.join("");
	var threadInfo = id + "_info";
	if (document.getElementById(threadInfo))
	{
		var hiddenNotice = document.createElement("em");
		var hiddenNoticeText = document.createTextNode(consts[lang].thread + ' ' + thread_name + ' ' + consts[lang].hidden + '.');
		hiddenNotice.appendChild(hiddenNoticeText);
		
		var hiddenNoticeDivision = document.getElementById(threadInfo);
		hiddenNoticeDivision.appendChild(hiddenNotice);
	}
	var showThreadText = id + "_display";
	if (document.getElementById(showThreadText)) 
	{
		var showThreadSpan = document.createElement("span");
		var showThreadLink = document.createElement("a");
		showThreadLink.setAttribute("href","javascript:threadShow('"+id+"')");
		var showThreadLinkText = document.createTextNode(consts[lang].thrshow);
		showThreadLink.appendChild(showThreadLinkText);
		showThreadSpan.appendChild(showThreadLink);
		
		var parentform = document.getElementById("delform");
		var oldSpan = document.getElementById(id+"_display");
		oldSpan.setAttribute("id","");
		parentform.replaceChild(showThreadSpan,oldSpan);
		showThreadLink.setAttribute("id","toggle"+id);
		showThreadSpan.setAttribute("id",id+"_display");
		showThreadSpan.style.cssFloat = "right";
		showThreadSpan.style.styleFloat = "right";
	}
}

function areYouSure(el)
{
	if(confirm('Are you sure?')) document.location = el.href;

	return false;
}

function update_captcha(el2) {
	var el = $j(el2);
	var de = $j('#de-main');
	if (el.length) { 
		var src = el.attr('src');
		if (!de.length) src = src.replace(/dummy=\d*/, 'dummy=' + Math.round(Math.random()*1000000));
		if (!(src.indexOf('update') + 1)) { src = src +"&update=1"; } el.attr('src',src);
	}
}

function captcha_required()
{
	var url = "/" + window.board + "/api/captcha?thread=" + window.thread_id;
	var td = $j('#trcap');

	if(td.css('display') != 'table-row')
		$j.getJSON(url, function(captcha) {
			if (captcha.data.required > 0)
				td.css('display', 'table-row');
		});
}

//==================================================================================================
// INLINE EXPANDING

function expand_post(id) {
	//$j("#posttext_" + id).html($j("#posttext_full_" + id).html());
	var abbr = document.getElementById("posttext_" + id);
	var full = document.getElementById("posttext_full_" + id);
	abbr.innerHTML = full.innerHTML;
	return false;
}

// http://stackoverflow.com/a/7557433/5628
function isElementInViewport(el) {
	var rect = el.getBoundingClientRect();
	return (rect.top >= 0 && rect.left >= 0);
}

function expand_image(element, org_width, org_height, thumb_width, thumb_height, thumb) {
	// var img = element;
	var img = element.firstElementChild || element.children[0];
	var org = img.parentNode.href;
	var post = element.parentNode.parentNode.parentNode.parentNode.parentNode;

	if (img.src != org) {
		img.src = org;
		//img.style.maxWidth = "98%";
		var maxw = (window.innerWidth || document.documentElement.clientWidth) - 100;
		img.width = org_width < maxw ? org_width : maxw;
		img.style.height = "auto";
		//img.width = org_width;
		//img.height = org_height;
	} else {
		img.src = thumb;
		img.width = thumb_width;
		img.height = thumb_height;
		if (!isElementInViewport(post)) post.scrollIntoView();
	}
	return false;
}

function expandAllPics(){for(var b=document.getElementsByName("expandfunc"),a=0;a<b.length;a++)b[a].click()};

//==================================================================================================
// AJAX.....

function AJAX(b, id, fn) {
	var xhr;
	if(window.XMLHttpRequest) xhr = new XMLHttpRequest()
	else if (window.ActiveXObject) {
		try { xhr = new ActiveXObject("Msxml2.XMLHTTP"); }
		catch (e) {
			try{ xhr = new ActiveXObject("Microsoft.XMLHTTP"); } catch (e){}
		}
	} else return false;
	xhr.onreadystatechange = function() {
		if(xhr.readyState != 4) return;
		if(xhr.status == 200) {
			var x = xhr.responseText;
			x = x.split(/<form[^>]+del[^>]+>/)[1].split('</form>')[0];
			var thrds = x.substring(0, x.lastIndexOf(x.match(/<br[^>]+left/))).split(/<br[^>]+left[^>]*>\s*<hr[^>]*>/);
			for(var i = 0, tLen = thrds.length; i < tLen; i++) {
				var tNum = thrds[i].match(/<input[^>]+checkbox[^>]+>/i)[0].match(/(\d+)/)[0];
				var posts = thrds[i].split(/<table[^>]*>/);
				ajaxThrds[tNum] = {keys: [], pcount: posts.length};
				for(var j = 0, pLen = posts.length; j < pLen; j++) {
					var x = posts[j];
					var pNum = x.match(/<input[^>]+checkbox[^>]+>/i)[0].match(/(\d+)/)[0];
					ajaxThrds[tNum].keys.push(pNum);
					ajaxPosts[pNum] = x.substring(!/<td/.test(x) && /filesize[^>]*>/.test(x) ? x.search(/input[^>]+checkbox*>/) : x.indexOf('<label'), /<td/.test(x) ? x.lastIndexOf('</td') : (/omittedposts[^>]*>/.test(x) ? x.lastIndexOf('</span') + 7 : x.lastIndexOf('</blockquote') + 13));
					ajaxRefmap(ajaxPosts[pNum].substr(ajaxPosts[pNum].indexOf('<blockquote>') + 12), pNum);
				}
			}
			fn();
		} else fn('HTTP ' + xhr.status + ' ' + xhr.statusText);
	};
	xhr.open('GET', '/' + b + '/res/' + id + '.html', true);
	// xhr.setRequestHeader('Accept-Encoding', 'deflate, gzip, x-gzip');
	xhr.send(false);
}

//==================================================================================================
// POST PREVIEW BY >>REFLINKS

function delPostPreview(e) {
	pView = e.relatedTarget;
	while(1) {
		if(/^preview/.test(pView.id)) break;
		else { pView = pView.parentNode; if(!pView) break; }
	}
	setTimeout(function() {
		if(!pView) $each($t('div'), function(el) { if(/^preview/.test(el.id)) $del(el); });
		else while(pView.nextSibling) $del(pView.nextSibling);
	}, 800);
}

function funcPostPreview(htm) {
	if(!pView) return;
	pView.innerHTML = htm;
	$each($t('script', pView), function(el) { $del(el); });
	eventPostPreview(pView);
	var pNum = pView.id.match(/\d+/);
	if(!$c('refmap', pView)[0] && !postByNum[pNum] && refMap[pNum])
		showRefMap(pView, pNum);
}

function showPostPreview(e) {
	var link = e.target;
	var ttNum = link.href.match(/thread=\d+/);
	if(ttNum) ttNum = ttNum[0].match(/\d+/)[0];
	var tNum = ttNum ? ttNum : link.pathname.substring(link.pathname.lastIndexOf('/')).match(/\d+/);
	var pNum = link.hash.match(/\d+/) || tNum;
	var scrW = document.body.clientWidth, scrH = window.innerHeight;
	x = $offset(link, 'offsetLeft') + link.offsetWidth/2;
	y = $offset(link, 'offsetTop');
	if(e.clientY < scrH*0.75) y += link.offsetHeight;
	pView = $new('div', {
		'id': 'preview_' + pNum,
		'class': 'reply',
		'html': '<span class="icn_wait">&nbsp;</span>&nbsp;' + consts[lang].load,
		'style':
			('position:absolute; z-index:300; border:1px solid grey; '
			+ (x < scrW/2 ? 'left:' + x : 'right:' + parseInt(scrW - x + 2)) + 'px; '
			+ (e.clientY < scrH*0.75 ? 'top:' + y : 'bottom:' + parseInt(scrH - y - 4)) + 'px')}, {
		'mouseout': delPostPreview,
		'mouseover': function() { if(!pView) pView = this; }
	});
	if(postByNum[pNum]) funcPostPreview(postByNum[pNum].innerHTML);
	else if(ajaxPosts[pNum]) funcPostPreview(ajaxPosts[pNum]);
	else AJAX(board, tNum, function(err) {
		funcPostPreview(err || ajaxPosts[pNum] || consts[lang].pNotFound)
	});
	$del($id(pView.id));
	dForm.appendChild(pView);
}

function eventPostPreview(node) {
	$each($t('a', node), function(link) {
		if(link.textContent.indexOf('>>') == 0 && !link.getAttribute('onmouseover'))
			$event(link, {'mouseover': showPostPreview, 'mouseout': delPostPreview});
	});
}

//==================================================================================================
// >>REFLINKS MAP IN POSTS

function getRefMap(pNum, rNum) {
	if(!refMap[rNum]) refMap[rNum] = [];
	if((',' + refMap[rNum].toString() + ',').indexOf(',' + pNum + ',') < 0)
		refMap[rNum].push(pNum);
}

function ajaxRefmap(txt, pNum) {
	var x = txt.match(/&gt;&gt;\d+/g);
	if(x) for(var i = 0; rLen = x.length, i < rLen; i++)
		getRefMap(pNum, x[i].match(/\d+/g));
}

function showRefMap(post, pNum, isUpd) {
	if(typeof refMap[pNum] !== 'object' || !post) return;
	var txt = consts[lang].replies + refMap[pNum].toString().replace(/(\d+)/g,
		'<a href="#$1" onmouseover="showPostPreview(event)" onmouseout="delPostPreview(event)">&gt;&gt;$1</a>');
	var el = isUpd ? $id('rfmap_' + pNum) : null;
	if(!el) {
		el = $new('small', {'class': 'refmap', 'id': 'rfmap_' + pNum, 'html': txt});
		$after($t('blockquote', post)[0], [el]);
	} else $html(el, txt);
}

function addRefMap(post) {
	$each($t('a', post), function(link) {
		if(link.textContent.indexOf('>>') == 0) {
			var rNum = link.hash.match(/\d+/);
			if(postByNum[rNum]) {
				while((link = link.parentNode).tagName != 'BLOCKQUOTE');
				var pu = link.parentNode.parentNode.parentNode.parentNode.parentNode;
				getRefMap(pu.id.match(/\d+/), rNum);
			}
		}
	});
	for(var rNum in refMap)
		showRefMap(postByNum[rNum], rNum, Boolean(post));
}


//==================================================================================================
// Messages

function showMessage(text, delay) {  // spizdil s inacha
    if (delay == null) delay = 1000;
    if ($j('#message').get() == '') {
        $j('body').children().last().after('<div id="message" class="reply"></div>');
        $j('#message').css('position', 'fixed');
        $j('#message').css('border-color','#161616');
        $j('#message').css('border-radius','5px 5px 5px 5px');
		$j('#message').css('top', '50px');
        $j('#message').css('padding', '20px');
        $j('#message').css('width', '40%');
        $j('#message').css('left', '30%');
        $j('#message').css('text-align', 'center');
        $j('#message').css('-webkit-box-shadow', '#666 0px 0px 10px');
        $j('#message').hide();
    }
    $j('#message').html("<span class=\"postername\">" + text + "</span>");
    $j('#message').fadeIn(150).delay(delay).fadeOut(300);
}

//==================================================================================================
// Thread updating

function buildTablePost(id) {
	return $new('table', {
		'class': 'post',
		'id': 'post_' + id,
		'html':
			'<tbody><tr><td class="reply" id="reply' + id +
			'">' + ajaxPosts[id] + '</td></tr></tbody>'
	});
}

function updateThread() {
	var btn = $id('getnewposts');
	btn.innerHTML = '[' + consts[lang].load + ']';
	// $alert('Загрузка...', 'wait');
	var thrd = $c('thread')[0];
	var tNum = thrd.id.match(/\d+/)[0];
	var last = Posts.length + 1;
	AJAX(board, tNum, function(err) {
		if(err) { alert(err); return; }
		if(ajaxThrds[tNum].pcount - last == 0) showMessage(consts[lang].newPostsNotFound);
		for(var i = last, len = ajaxThrds[tNum].pcount; i < len; i++) {
			var pNum = ajaxThrds[tNum].keys[i];
			var post = buildTablePost(pNum);
			post.Num = pNum;
			thrd.appendChild(post);
			postByNum[pNum] = post;
			Posts.push(post);
			eventPostPreview(post);
			addRefMap(post);
		}
		setTimeout(function() {
			btn.innerHTML = '[<a href="#" onclick="javascript:updateThread(); return false;">' + consts[lang].updthr + '</a>]';
			checkIn(window.board);
		}, 1500);
	}, true);
}

//==================================================================================================
// "MOMMY IN ROOM" IMAGE HIDER

function toggleMommy() {
	Settings.set('mommy', (Settings.get('mommy') || 0) == 0 ? 1 : 0);
	scriptCSS();
}

function hotkeyMommy(e) {
	if(!e) e = window.event;
	if(e.altKey && e.keyCode == 78) toggleMommy();
}

//==================================================================================================
// "Small fonts"

function toggleSMFonts() {
	Settings.set('small', (Settings.get('small') || 0) == 0 ? 1 : 0);
	scriptCSS();
}

//==================================================================================================
// SCRIPT CSS

function scriptCSS() {
	var x = [];
	if(Settings.get('mommy') == 1)
		x.push('img[src*="thumb"] {opacity:0.04 !important} img[src*="thumb"]:hover {opacity:1 !important}');
	// if(Settings.get('small') == 1)
	// 	x.push('form {font-size: 10pt} html,body {font-family:"Trebuchet MS",Trebuchet,tahoma,serif;font-size:1em} #overlay, .adminbar {font-size:.8em} .reflink {font-size:.9em}');
	if(!$id('css'))
		$t('head')[0].appendChild($new('style', {
			'id': 'css',
			'type': 'text/css',
			'text': x.join(' ')
		}));
	else $id('css').textContent = x.join(' ');
}


//==================================================================================================
// Localstorage settings

var Settings = function() {
    if (!window.localStorage) {
        return {
            set: function(key, value) {
                return set_cookie(key, value, 365 * 24 * 60 * 60 * 1000);
            },
            get: function(key) {
                return get_cookie(key);
            },
            del: function(key) {
                return set_cookie(key, "", 1);
            }
        };
    } else
        return {
            set: function(key, value) {
                return window.localStorage.setItem(key, value);
            },
            get: function(key) {
                // legacy
                var cookie = get_cookie(key);
                var local = window.localStorage.getItem(key);
                if (cookie && !local) {
                    window.localStorage.setItem(key, cookie);
                }
                // end legacy
                return window.localStorage.getItem(key);
            },
            del: function(key) {
                return window.localStorage.removeItem(key);
            }
        };
}();

function th_buttons() {
	$j("a[href='#bottom']").click(function() {
	  $j("html, body").animate({ scrollTop: $j(document).height() }, "slow");
	});

    $j('a[href=#top]').click(function(){
        $j('html, body').animate({scrollTop:0}, 'slow');
    });

	return false;
}

function checkIn(brd) {
	var forms = (document.forms['delform'] || $byattr('de-form'));
    if (!brd || !forms)
        return;
    var json = Settings.get('visited');
    var obj = json ? JSON.parse(json) : {};
    obj[brd] = 1 + Math.round((new Date()).getTime() / 1000);
    Settings.set('visited', JSON.stringify(obj), 365);
}

function toggleNavMenu(link, mode) {
  if (mode == 0) {
   	$id("overlay").style.display = "block";
  } else {
    $id("overlay").style.display = "none";
  }
}

// ========= END =============