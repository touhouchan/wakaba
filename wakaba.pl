#!/usr/bin/perl -X

#
# Board Class
# Handles properties of a particular board
#

# Constructor:
# $board_object = Board->new("current_board_name");
# Instance Methods:
# $board_name = $board_object->path();
# $silly_anonymous_option = $board_object->option('SILLY_ANONYMOUS'); 

# Thanks to
# desuchan.net
# for parts of code
#   lel

package Board;

use strict;		# ALWAYS USE STRICT. NEVER NOT USE STRICT.

my %options; 		# Class attribute storing properties of all board configurations.

sub new()
{
	my $self = shift;
	my $board = shift;
	
	# Grab code from config file and evaluate.
	open (BOARDCONF, './'.$board.'/board_config.pl') or return 0; # Silently fail if we cannot open file.
	binmode BOARDCONF; # Needed for files using non-ASCII characters.
	
	my $board_options_code = do { local $/; <BOARDCONF> };
	
	# NOTE TO SELF: Add directory normalization
	# Add global configuration versus local configuration resolution.

	$options{$board} = eval $board_options_code; # Set up hash.

	# Default/global options. (MUCH more should be added.)
	$options{$board}{BACKUP_DIR} = "backup/";
	$options{$board}{ORPH_DIR} = "orphans/";

	# Exception for bad config.
	&::log_issue("Board configuration error: ".$@) and close BOARDCONF and return 0 if ($@);
	
	close BOARDCONF;
	
	bless \$board, $self;
}

sub option()
{
	my $board = shift;
	my $option = shift;	
	
	$options{$$board}->{$option};
}

sub path()
{
	my $self = shift;
	return $$self;
}

#
# Main Class
#

package main;

use CGI::Carp qw(fatalsToBrowser);

umask 0022;

use strict;

use CGI::Fast;
use FCGI::ProcManager qw(pm_manage pm_pre_dispatch 
						 pm_post_dispatch);
use DBI;
use Net::DNS;
use Net::IP qw(:PROC);
use IO::Socket;
use IO::Scalar;
use File::Copy;

use HTML::Entities;
use JSON::XS;
#use JSON;

use Encode qw(decode encode);

#
# Import settings
#

use lib '.';
BEGIN {
 require "lib/site_config.pl";
 require "lib/config_defaults.pl";
 require "lib/strings_ru.pl";		# edit this line to change the language
 require "lib/futaba_style.pl";	# edit this line to change the board style
 require "captcha.pl";
 require "lib/wakautils.pl";
}

if (ENABLE_RSS)
{
	use MIME::Types;
}

print "Wakaba loaded!\n";

# stop here if we're being called externally
return 1 if(caller);

pm_manage( n_processes => SERVER_CONCURRENCY, pm_title => 'fcgi-pm-toho' );

#
# Optional modules
#


#
# Global init
#
my $protocol_re=qr/(?:http|https|ftp|mailto|nntp|xmpp)/;
my $dbh;

# FCGI "init"
my ($query,$board,$ajax_mode);

my $JSON = JSON::XS->new->pretty->utf8;

# Sanity checks for constants
my $page_ext = (PAGE_EXT =~ /^\./) ? PAGE_EXT : '.'.PAGE_EXT;

#
# Database
#

sub get_conn() 
{
	return $MyPackage::dbh if ($MyPackage::dbh && $MyPackage::dbh->ping);

	# we don't have connection or the connection timed out, so make a new one
	return 0 unless $MyPackage::dbh = DBI->connect_cached(SQL_DBI_SOURCE,SQL_USERNAME,SQL_PASSWORD,{AutoCommit=>1,RaiseError=>1});
	return $MyPackage::dbh;
}

FASTCGI:
while ( $query = CGI::Fast->new )
{
	pm_pre_dispatch();

	$dbh=get_conn();

	my $conn = $dbh->prepare("SET NAMES 'utf8mb4';");
	$conn->execute() or make_error(S_SQLFAIL);
	$conn->finish();

	my $task=($query->param("task") or $query->param("action"));
	my $json=($query->param("json") or "");
	$ajax_mode    = 0; #reset

	unless (0)				# Unless working with a global task -- NOT DONE YET...
	{
		my $board_name=($query->param("board"));
		if($board_name && $board_name !~/^[a-zA-Z0-9]+$/)
		{
			print "Content-type: text/plain\n\nWrong board name: $board_name";
			next;
		}
		if ((! -e './'.$board_name) || !$board_name || $board_name eq "include")
		{
			print ("Content-type: text/plain\n\nBoard not found.");
			next;
		}
		if (! -e './'.$board_name.'/board_config.pl')
		{
			print ("Content-type: text/plain\n\nBoard configuration not found.");
			next;
		}
		
		$board = Board->new($board_name) or make_error("Board module for $board_name could not be loaded!");

		# check for moders table
		if (!table_exists(SQL_MODER_TABLE))
		{
			init_moder_database();
			add_initial_moder();
		}

		if(!table_exists(SQL_TABLE)) # check for comments table
		{
			init_database();
			make_http_forward($board->path().'/'.$board->option('HTML_SELF'));
		}

		if(!counter_exists())
		{
			init_counters($board->path);
		}
	}

	# check for admin table
	init_admin_database() if(!table_exists(SQL_ADMIN_TABLE));

	# check for common site table
	init_common_site_database() if (!table_exists(SQL_COMMON_SITE_TABLE));

	# check for backups table
	init_backup_database() if (!table_exists(SQL_BACKUP_TABLE) && ENABLE_POST_BACKUP);

	# cleanup backups table
	cleanup_backups_database();

	# JSON

	if ($json eq "threads") {
		my $page=$query->param("page");
		$page=1 if(!$page);
		output_json_threads($page);
	}
	elsif ($json eq "thread") {
		my $id = $query->param("id");
        if (defined($id) and $id =~ /^[+-]?\d+$/)
        {
			output_json_thread($id);
		}
        else { make_json_error(''); }
	}
	elsif ( $json eq "newposts" ) {
		my $id = $query->param("id");
		my $after = $query->param("after");
		if ( $id =~ /^[+-]?\d+$/ && $after =~ /^[+-]?\d+$/ ) {
			output_json_newposts($id, $after);
		}
		else { make_json_error(''); }
	}
	elsif ( $json eq "post" ) {
		my $id = $query->param("id");
		if ( defined($id) and $id =~ /^[+-]?\d+$/ ) {
			output_json_newposts($id, '', 1);
		}
		else { make_json_error(''); }
	}
	elsif ($json eq "postcount") {
		my $id=$query->param("id");
        if (defined($id) and $id =~ /^[+-]?\d+$/)
        {
			output_json_postcount($id);
		}
        else { make_json_error(''); }
	}
	elsif ($json eq "posts") {
		my $timestamp=$query->param("timestamp");
		output_json_allpostcount($timestamp);
	}
    elsif ( $json eq "search" ) {
        my $find            = $query->param("find");
        my $op_only         = $query->param("op");
        my $in_subject      = $query->param("subject");
        my $in_comment      = $query->param("comment");

        json_find_posts($find, $op_only, $in_subject, $in_comment);
    }
    elsif ( $json eq "getboards" ) {
        output_json_boardlist();
    }
	elsif ( $json eq "checkconfig" ) {
		get_boardconfig(1, undef);
	}
	elsif ($json eq "captcha") {
		get_boardconfig(1, 1);
	}

	# Tasks
	if(!$task && !$json)
	{
		my $admin = $query->cookie("wakaadmin");
		show_page(1,$admin);
	}
	elsif ( $task eq "show" ) {
		my $admin    = $query->cookie("wakaadmin");
		my $page = $query->param("page");
		my $post = $query->param("post");
		my $thread = $query->param("thread");

        # outputs a single post only
		if ( defined($post) and $post =~ /^[+-]?\d+$/ ) {
			show_post($post, $admin);
		}
		# show the requested thread
		elsif ( defined($thread) and $thread =~ /^[+-]?\d+$/ ) {
			if($thread ne 0) {
				show_thread($thread, $admin);
			}
			else {
				make_error(S_STOP_FOOLING);
			}
		}
        # show the requested page (if any)
		else {
            # fallback to page 1 if parameter was empty or incorrect
            $page = 1 unless (defined($page) and $page =~ /^[+-]?\d+$/ and $page > 0);
            show_page($page, $admin);
		}
	}
	elsif($task eq "post")
	{
		my $parent=$query->param("parent");
		my $name=$query->param("akane");
		my $email=$query->param("nabiki");
		my $subject=$query->param("kasumi");
		my $comment=$query->param("shampoo");
		my $file=$query->param("file");
		my $password=$query->param("password");
		my $nofile=$query->param("nofile");
		my $captcha=$query->param("captcha");
		my $admin=$query->cookie("wakaadmin");
		my $no_captcha=$query->param("no_captcha");
		my $no_format=$query->param("no_format");
		my $postfix=$query->param("postfix");
		my $admin_post=$query->param("adminpost");
		my $capcode=$query->param("capcode");
		my $gb2=$query->param("gb2");
		my $sticky=$query->param("sticky");
		my $lock=$query->param("lock");
		my $autosage=$query->param("autosage");
		my $ajax=$query->param("ajax");

		post_stuff(
			$parent, $name, $email, $subject,
			$comment, $file, $file, $password,
			$nofile, $captcha, $admin, $no_captcha,
			$no_format, $postfix, $admin_post, $capcode,
			$gb2, $sticky, $lock, $autosage, $ajax
		);
	}
	elsif($task eq "delete")
	{
		my $password=$query->param("password");
		my $fileonly=$query->param("fileonly");
		my $archive=$query->param("archive");
		my $ajax=$query->param("ajax");
		my $admin=$query->cookie("wakaadmin");
		my $admindel=$query->param("admindel");
		my $parent=$query->param("parent");
		my $caller = $query->param("caller");
		my @posts=$query->param("delete");

		delete_stuff($password,$fileonly,$archive,$parent,$ajax,$admin,$admindel,$caller,@posts);
	}
	elsif($task eq "loginpanel")
	{
		my $admincookie = $query->cookie("wakaadmin");
		my $savelogin=$query->param("savelogin");

		unless ($admincookie)	# Direct to login panel unless already logged in via cookie.
		{
			make_admin_login();
		}
		else
		{
			do_login('','','',$savelogin,$admincookie);
		}
		
	}
	elsif($task eq "admin")
	{
		my $password=$query->param("berra"); # lol obfuscation
		my $username=$query->param("desu"); # Fuck yes, you are the best obfuscation ever! 
		my $nexttask=$query->param("nexttask");
		my $savelogin=$query->param("savelogin");
		my $admincookie=$query->cookie("wakaadmin");

		do_login($username,$password,$nexttask,$savelogin,$admincookie);
	}
	elsif($task eq "logout")
	{
		do_logout();
	}
	elsif ( $task eq "mpanel" ) {
		my $admin = $query->cookie("wakaadmin");
		my $page  = $query->param("page");
		if ( !defined($page) ) { $page = 1; }
		show_page($page, $admin);
	}
	elsif($task eq "deleteall")
	{
		my $admin=$query->cookie("wakaadmin");
		my $ip=$query->param("ip");
		my $mask=$query->param("mask");
		delete_all($admin,parse_range($ip,$mask));
	}
	elsif($task eq "bans")
	{
		my $admin=$query->cookie("wakaadmin");
		make_admin_ban_panel($admin);
	}
	elsif($task eq "addip")
	{
		my $admin=$query->cookie("wakaadmin");
		my $type=$query->param("type");
		my $comment=$query->param("comment");
		my $ip=$query->param("ip");
		my $mask=$query->param("mask");
		add_admin_entry($admin,$type,$comment,parse_range($ip,$mask),'',0);
	}
	elsif($task eq "addstring")
	{
		my $admin=$query->cookie("wakaadmin");
		my $type=$query->param("type");
		my $string=$query->param("string");
		my $comment=$query->param("comment");
		add_admin_entry($admin,$type,$comment,0,0,$string,0);
	}
	elsif($task eq "removeban")
	{
		my $admin=$query->cookie("wakaadmin");
		my $num=$query->param("num");
		remove_admin_entry($admin,$num);
	}
	elsif($task eq "banpost")
	{
		my $admin=$query->cookie("wakaadmin");
		my $comment=$query->param("comment");
		my $num=$query->param("num");
		banpost($admin,$num,$comment);
	}
    elsif ( $task eq "orphans" ) {
        my $admin = $query->cookie("wakaadmin");
        make_admin_orphans($admin);
    }
    elsif ( $task eq "movefiles" ) {
        my $admin = $query->cookie("wakaadmin");
        my @files = $query->multi_param("file"); # CGI >= 4.08 REQUIRED
        move_files($admin, @files);
    }
	elsif($task eq "sticky")
	{
		my $admin=$query->cookie("wakaadmin");
		my $num=$query->param("num");
		thread_control($admin,$num,'sticky');
	}
	elsif($task eq "lock")
	{
		my $admin=$query->cookie("wakaadmin");
		my $num=$query->param("num");
		thread_control($admin,$num,'lock');
	}
	elsif($task eq "autosage")
	{
		my $admin=$query->cookie("wakaadmin");
		my $num=$query->param("num");
		thread_control($admin,$num,'autosage');
	}
	# Post search
	elsif ($task eq "search") {
		my $admin  			= $query->cookie("wakaadmin");
		my $find			= $query->param("find");
		my $op_only			= $query->param("op");
		my $in_subject		= $query->param("subject");
		my $in_filenames	= $query->param("files");
		my $in_comment		= $query->param("comment");

		find_posts($admin, $find, $op_only, $in_subject, $in_filenames, $in_comment);
	}
	# Moders
	elsif ($task eq "moders") {
		my $admin = $query->cookie("wakaadmin");
		make_admin_moder_panel($admin);
	}
	elsif ($task eq "addmod") {
		my $admin = $query->cookie("wakaadmin");
		my $management_password = $query->param("mpass"); # Necessary for creating in Admin class
		my $username = $query->param("usertocreate");
		my $password = $query->param("passtocreate");
		my $type = $query->param("account");
		my @boards = $query->param("boards");

		add_moder($admin,$username,$password,$type,$management_password,@boards);
	}
	elsif ($task eq "removemod") {
		my $admin = $query->cookie("wakaadmin");
		my $username = $query->param("username");
		my $mpass = $query->param("mpass");
		remove_moder($admin,$username,$mpass);
	}
	# elsif ($task eq "showmodpass") {
	#     my $admin = $query->cookie("wakaadmin");
	#     my $num = $query->param("num");
	#     show_moderpass($admin,$num);
	# }
	elsif($task eq "editmod"){
		my $admin=$query->cookie("wakaadmin");
		my $username=$query->param("username");
		edit_moder($admin,$username);
	}
	elsif($task eq "updatemod"){
		my $admin=$query->cookie("wakaadmin");
		my $user=$query->param("username");
		my $newpass=$query->param("newpass");
		my $newclass=$query->param("newclass");
		my $origpass=$query->param("origpass");
		my $disable=$query->param("disable");
		my @boards=$query->param("boards");
		update_moder_details($admin,$user,$newpass,$newclass,$origpass,$disable,@boards);
	}
	elsif($task eq "showpost")
	{
		my $post = $query->param("id");
		my $after = $query->param("after");
		my $parent = $query->param("parent");
		my $admin = $query->cookie("wakaadmin");

		show_post($post, $after, $parent, $admin);
	}
	# Post Backups (Staff Only)
	elsif ($task eq "postbackups")
	{	
		my $admin = $query->cookie("wakaadmin");
		my $page = $query->param('page');
		my $perpage = $query->param('perpage');

		make_backup_posts_panel($admin, $page, $perpage);
	}
	elsif ($task eq "restorebackups")
	{
		my $admin = $query->cookie("wakaadmin");
		my @num = $query->param('num');
		my $board_name = $query->param('board') || $board->path();

		if (lc $query->param("handle") eq "restore")
		{
			restore_stuff($admin, $board_name, @num);
		}
		else
		{
			remove_post_from_backup($admin, $board_name, @num);
		}
	}
	# post editing
	elsif($task eq "edit")
	{
		my $admin=$query->cookie("wakaadmin");
		my $post=$query->param("num");
		my $noformat=$query->param("noformat");
		make_edit_post_panel($admin,$post,$noformat);
	}
	elsif($task eq "doedit")
	{
		my $admin=$query->cookie("wakaadmin");
		my $num=$query->param("num");
		my $name=$query->param("field1");
		my $email=$query->param("field2");
		my $subject=$query->param("field3");
		my $comment=$query->param("field4");
		my $no_format=$query->param("noformat");
		my $capcode=$query->param("capcode");
		edit_post($admin,$num,$name,$email,$subject,$comment,$capcode,$no_format);
	}
	elsif($task eq "rss")
	{
		make_rss();
	}
	# Idi sasay pornuha
	else
	{
		make_error("Invalid task") unless($json);
	}

	#$dbh->disconnect();

	pm_post_dispatch();
}


#
# Dynamic pages, kek
#

# sub show_post, backported from phutaba
# shows a single post out of a thread, essential for the reloadless view of threads
# takes an integer as argument
# returns nothing
sub show_post {
	my ($id, $after, $parent, $admin) = @_;
	my ($sth, $row, $where, @thread);
	my $isAdmin = 0;
	if(defined($admin))
	{
		check_password($admin, '');
		$isAdmin = 1;
	}

	$after = 0 if($after=~/[^0-9]/);
	undef($after) unless $parent;
	# make_error($after);

	if($after) {
		$id = $after;
	}

	$where = $after ? "section=? AND num>? AND parent=$parent" : "section=? AND num=?";
	$sth = $dbh->prepare("SELECT * FROM `" . SQL_TABLE . "` WHERE $where;" ) or make_error(S_SQLFAIL);
	$sth->execute( $board->path(), $id ) or make_error(S_SQLFAIL);
	make_http_header();
	while($row = get_decoded_hashref($sth))
	{
		$$row{comment} = resolve_reflinks($$row{comment});
		push(@thread, $row);
	}
	$sth->finish();

	if ($sth->rows ne 0) {
		my $output =
			encode_string(
				SINGLE_POST_TEMPLATE->(
					thread	     => $id,
					posts        => \@thread,
					single	     => 1,
					admin        => $isAdmin,
					locked       => $thread[0]{locked},
					board        => $board,
				)
			);
		$output =~ s/^\s+//; # remove whitespace at the beginning
		$output =~ s/^\s+\n//mg; # remove empty lines
		print($output);
	}
	else {
		print encode_json( { "error_code" => 400 } );
	}
}

sub show_thread {
	my ($thread, $admin) = @_;
	my ( $sth, $row, @thread );
	my ( $filename, $tmpname );
	my ( $title );
	
	# if we try to call show_thread with admin parameter
	# the admin password will be checked and this
	# variable will be 1
	my $isAdmin = 0;
	my ($username, $type);
	if(defined($admin))
	{
		($username, $type) = check_password($admin, '');
		$isAdmin = 1;
	}

	$sth = $dbh->prepare( "SELECT * FROM `" . SQL_TABLE . "` WHERE section=? AND (num=? OR parent=?) ORDER BY num ASC;" ) or make_error(S_SQLFAIL);
	$sth->execute( $board->path(), $thread, $thread ) or make_error(S_SQLFAIL);

	while ( $row = get_decoded_hashref($sth) ) {
		$$row{comment} = resolve_reflinks($$row{comment});
		push( @thread, $row );
	}

	make_error(S_NOTHREADERR, 1) if ( !$thread[0] or $thread[0]{parent} );

	$title = $thread[0]{subject} ne '' ? $thread[0]{subject} : sprintf S_THREADTITLE,$thread[0]{num};

	make_http_header();
	my $loc = get_geolocation(get_remote_addr());
	my $output = 
		encode_string(
			PAGE_TEMPLATE->(
				thread       => $thread,
				title        => $title,
				postform 	 => ( $board->option('ALLOW_TEXT_REPLIES') or $board->option('ALLOW_IMAGE_REPLIES') ),
				image_inp    => $board->option('ALLOW_IMAGE_REPLIES'),
				textonly_inp => 0,
				dummy        => $thread[$#thread]{num},
				loc          => $loc,
				threads      => [ { posts => \@thread } ],
				admin        => $isAdmin, 
				lockedthread => $thread[0]{locked},
				board        => $board,
				stylesheets  => get_stylesheets(),
				usertype 	 => $type,
				username 	 => $username,
			)
		);
	$output =~ s/^\s+\n//mg;
	print($output);
}

sub show_page {
	my ($pageToShow, $admin) = @_;
	my $page = 1;
	my ( $sth, $row, @thread );
	my ($sth2, $row2);
	# if we try to call show_page with admin parameter
	# the admin password will be checked and this
	# variable will be 1
	my $isAdmin = 0;
	my ($username, $type);
	if(defined($admin))
	{
		($username, $type) = check_password($admin, '');
		$isAdmin = 1;
	}

	my $thread_count = count_threads();
	my $total_pages = get_page_count($thread_count);

	if ( $thread_count == 0 )    # no posts on the board!
	{
		output_page( 1, 1, $isAdmin, $username, $type, $admin, () );    # make an empty page 1
	}
	else {
		make_error(S_INVALID_PAGE, 1) if ($pageToShow > $total_pages or $pageToShow <= 0);

		my (@threads, @thread);

		# grab all threads for the current page in sticky and bump order
		$sth = $dbh->prepare(
			    "SELECT * FROM "
			  . SQL_TABLE
			  . " WHERE section=? AND parent=0 ORDER BY sticky DESC,lasthit DESC,num ASC"
			  . " LIMIT ?,?"
		) or make_error(S_SQLFAIL);
		$sth->execute( $board->path(), $board->option('IMAGES_PER_PAGE') * ($pageToShow - 1), $board->option('IMAGES_PER_PAGE') ) or make_error(S_SQLFAIL);

		while ($row = get_decoded_hashref($sth)) {
			@thread = ($row);

			# add posts to thread
			$sth2 = $dbh->prepare(
				    "SELECT * FROM "
				  . SQL_TABLE
				  . " WHERE section=? AND parent=? ORDER BY num ASC"
			) or make_error(S_SQLFAIL);
			$sth2->execute($board->path(), $$row{num}) or make_error(S_SQLFAIL);

			while ($row2 = get_decoded_hashref($sth2)) {
				push @thread, $row2;
			}

			push @threads, { posts => [@thread] };
		}

		output_page( $page, $total_pages, $isAdmin, $username, $type, $admin, @threads);
	}
}

sub output_page {
	my ( $page, $total, $isAdmin, $username, $usertype, $adminPass, @threads) = @_;
	my ( $filename, $tmpname );

	# do abbrevations and such
	foreach my $thread (@threads) {
		# split off the parent post, and count the replies and images
		my ( $parent, @replies ) = @{ $$thread{posts} };
		my $replies = @replies;

		my $images = grep { $$_{image} } @replies;

		my $curr_replies = $replies;
		my $curr_images  = $images;
		my $max_images   = $images;
		my $max_replies;

		# Abbreviation setings -- Stickied threads have their own setting
		if ( $$parent{sticky} )
		{
			$max_replies = REPLIES_PER_STICKY;
		}
		elsif ( $$parent{locked} ) {
			$max_replies = REPLIES_PER_LOCKED;
		}
		else
		{
			$max_replies = $board->option('REPLIES_PER_THREAD');
		}

		# drop replies until we have few enough replies and images
		while ( $curr_replies > $max_replies or $curr_images > $max_images ) {
			my $post = shift @replies;
			$curr_images-- if $$post{image};
			$curr_replies--;
		}
		# write the shortened list of replies back
		$$thread{posts}      = [ $parent, @replies ];
		$$thread{omit}=$replies-$curr_replies;
		$$thread{omitimages}=$images-$curr_images;
		$$thread{num}	     = ${$$thread{posts}}[0]{num};

		# abbreviate the remaining posts
		foreach my $post ( @{ $$thread{posts} } ) {
			# create ref-links
			$$post{comment} = resolve_reflinks($$post{comment});

			my $abbreviation =
			  abbreviate_html( $$post{comment}, $board->option('MAX_LINES_SHOWN'),
				$board->option('APPROX_LINE_LENGTH') );
			if ($abbreviation) {
                $$post{abbrev} = get_abbrev_message(count_lines($$post{comment}) - count_lines($abbreviation));
                $$post{comment_full} = $$post{comment};
				$$post{comment} = $abbreviation;
			}
		}
	}

	# make the list of pages
	my @pages = map +{ page => $_ }, ( 1 .. $total );

	foreach my $p (@pages) {
			if($$p{page}==0) { $$p{filename}=expand_filename($board->option('HTML_SELF')) }
			else { $$p{filename} = expand_filename( $$p{page} . $page_ext ); }
  
			if ( $$p{page} == $page ) { $$p{current} = 1 }   # current page, no link
	}

	my ( $prevpage, $nextpage, $prevpage_fn, $nextpage_fn ); ##ugly
	$prevpage_fn = $pages[ $page - 2 ]{filename} if ( $page != 1 );
	$nextpage_fn = $pages[ $page     ]{filename} if ( $page != $total );

	$prevpage = $pages[ $page - 2 ]{page} if ( $page != 1 );
	$nextpage = $pages[ $page     ]{page} if ( $page != $total );

	make_http_header();
	my $loc = get_geolocation(get_remote_addr());
	my $output =
		encode_string(
			PAGE_TEMPLATE->(
				postform => ( $board->option('ALLOW_TEXTONLY') or $board->option('ALLOW_IMAGES') ),
				image_inp    => $board->option('ALLOW_IMAGES'),
				textonly_inp => ( $board->option('ALLOW_IMAGES') and $board->option('ALLOW_TEXTONLY') ),
				prevpage     => $prevpage, nextpage => $nextpage,
				prevpage_fn  => $prevpage_fn, nextpage_fn => $nextpage_fn,
				pages        => \@pages,
				loc          => $loc,
				threads      => \@threads,
				admin        => $isAdmin,
				board 		 => $board,
				stylesheets  => get_stylesheets(),
				usertype 	 => $usertype,
				username 	 => $username,
			)
		);

	$output =~ s/^\s+\n//mg;
	print($output);
}

sub print_page($$)
{
	my ($filename,$contents)=@_;

	$contents=encode_string($contents);
#		$PerlIO::encoding::fallback=0x0200 if($has_encode);
#		binmode PAGE,':encoding('.CHARSET.')' if($has_encode);

	if(USE_TEMPFILES)
	{
		my $tmpname=$board->path().'/'.$board->option('RES_DIR').'tmp'.int(rand(1000000000));

		open (PAGE,">$tmpname") or make_error(S_NOTWRITE);
		print PAGE $contents;
		close PAGE;

		rename $tmpname,$filename;
	}
	else
	{
		open (PAGE,">$filename") or make_error(S_NOTWRITE);
		print PAGE $contents;
		close PAGE;
	}
}


#
# Posting
#

sub post_stuff
{
	my (
		$parent, $name, $email, $subject,
		$comment, $file, $uploadname, $password,
		$nofile, $captcha, $admin, $no_captcha,
		$no_format, $postfix, $admin_post_mode, $capcode,
		$gb2, $sticky, $lock, $autosage, $ajax
	)=@_;

	# initialize variables
	my $admin_post = 0;
	my $max_sticky = (get_max_sticked() + 1);
	$ajax_mode = 1 if $ajax;

	# get a timestamp for future use
	my $time=time();

	#my $removemelater = 1;
	#make_error("Mysql sex, will be back soon") if $removemelater;

	# check that the request came in as a POST, or from the command line
	make_error(S_UNJUST) if($ENV{REQUEST_METHOD} and $ENV{REQUEST_METHOD} ne "POST");

	if($admin_post_mode) # check admin password - allow both encrypted and non-encrypted
	{
		check_password($admin,'');
		$admin_post = 1;
	}
	else
	{
		# forbid admin-only features
		make_error(S_WRONGPASS) if($no_captcha or $no_format or $postfix or $sticky or $lock or $autosage);

		# check what kind of posting is allowed
		if($parent)
		{
			make_error(S_NOTALLOWED) if( $file and !($board->option('ALLOW_IMAGE_REPLIES')) );
			make_error(S_NOTALLOWED) if( !$file and !($board->option('ALLOW_TEXT_REPLIES')) );
		}
		else
		{
			make_error(S_NOTALLOWED) if( $file and !($board->option('ALLOW_IMAGES')) );
			make_error(S_NOTALLOWED) if( !$file and $nofile and !($board->option('ALLOW_TEXTONLY')) );
		}
	}

	# puu
	my $stickycheck;
    if($parent and $admin_post)
    {
        my $selectsticky = $dbh->prepare(
              "SELECT sticky, locked, autosage FROM "
              . SQL_TABLE
              . " WHERE section=? AND num=? and parent=0 LIMIT 1;"
            ) or make_error(S_SQLFAIL);
        $selectsticky->execute($board->path(), $parent) or make_error(S_SQLFAIL);
        $stickycheck = $selectsticky->fetchrow_hashref;
        $selectsticky->finish();
    }

	if($sticky)
	{
		$sticky = $max_sticky;
		if($parent)
		{
			my $stickyupdate=$dbh->prepare("UPDATE `".SQL_TABLE."` SET sticky=? WHERE section=? AND (num=? OR parent=?);") or make_error(S_SQLFAIL);
			$sticky = $$stickycheck{sticky} ? 0 : $max_sticky;
			$stickyupdate->execute($sticky, $board->path(), $parent, $parent) or make_error(S_SQLFAIL);
			$stickyupdate->finish();
		}
	}
	else { $sticky = 0; }
	
	if ($lock)
	{
		$lock = 1;
		if ($parent)
		{
			my $lockupdate=$dbh->prepare("UPDATE `".SQL_TABLE."` SET locked=? WHERE section=? AND (num=? OR parent=?);") or make_error(S_SQLFAIL);
			$lock = $$stickycheck{locked} ? 0 : 1;
			$lockupdate->execute($lock, $board->path(), $parent, $parent) or make_error(S_SQLFAIL);
			$lockupdate->finish();
		}
	}
	else { $lock = 0; }

	if ($autosage)
	{
		$autosage=1;
		if ($parent)
		{
			my $asupdate=$dbh->prepare("UPDATE `".SQL_TABLE."` SET autosage=? WHERE section=? AND (num=? OR parent=?);") or make_error(S_SQLFAIL);
			$autosage = $$stickycheck{autosage} ? 0 : 1;
			$asupdate->execute($lock, $board->path(), $parent, $parent) or make_error(S_SQLFAIL);
			$asupdate->finish();
		}
	}
	else { $autosage = 0; }

	# check for weird characters
	make_error(S_UNUSUAL) if($parent=~/[^0-9]/);
	make_error(S_UNUSUAL) if(length($parent)>10);
	make_error(S_UNUSUAL) if($name=~/[\n\r]/);
	make_error(S_UNUSUAL) if($email=~/[\n\r]/);
	make_error(S_UNUSUAL) if($subject=~/[\n\r]/);

	# check for excessive amounts of text
	make_error(S_TOOLONG) if(length( decode_string($comment, CHARSET) ) > ($board->option('MAX_COMMENT_LENGTH')) );

	# check to make sure the user selected a file, or clicked the checkbox
	make_error(S_NOPIC) if(!$parent and !$file and !$nofile and !$admin_post);

	# check for empty reply or empty text-only post
	make_error(S_NOTEXT) if($comment=~/^\s*$/ and !$file);

	# get file size, and check for limitations.
	my $size=get_file_size($file) if($file);

	# find IP
	my $ip=get_remote_addr();

	# ssl
	my $ssl = $ENV{HTTP_X_ALUHUT};
	undef($ssl) unless $ssl;

	#$host = gethostbyaddr($ip);
	my $numip=dot_to_dec($ip);

	# set up cookies
	my $c_name=$name;
	my $c_email=$email;
	my $c_password=$password;
	my $c_gb2=$gb2;

	# check if IP is whitelisted
	my $whitelisted=is_whitelisted($numip);
	dnsbl_check($ip) if ( !$whitelisted and ENABLE_DNSBL_CHECK );

	# process the tripcode - maybe the string should be decoded later
	my $trip;
	($name,$trip)=process_tripcode($name,($board->option('TRIPKEY')),SECRET,CHARSET);

	# check for bans
	ban_check($numip,$c_name,$subject,$comment) unless $whitelisted;
	($comment) = word_check($comment) unless $whitelisted;

	# check for excessive amounts of text
	make_error(S_TOOLONG) if(length($name)>($board->option('MAX_FIELD_LENGTH')));
	make_error(S_TOOLONG) if(length($email)>($board->option('MAX_FIELD_LENGTH')));
	make_error(S_TOOLONG) if(length($subject)>($board->option('MAX_FIELD_LENGTH')));

	# spam check
	my $isspam;
	$isspam = spam_engine(
		query=>$query,
		trap_fields=>$board->option('SPAM_TRAP')?["name","link"]:[],
	) unless $whitelisted or $admin_post or is_trusted($trip);

	if($isspam)
	{
		make_error(S_SPAM);
	}

	# get location
	my $loc = get_geolocation($ip);

	# check captcha
	check_captcha( $dbh, $captcha, $ip, $parent, $board->path() )
	  if ( (use_captcha($board->option('ENABLE_CAPTCHA'), $loc) and !$admin_post) or ($board->option('ENABLE_CAPTCHA') and !$admin_post and !$no_captcha and !is_trusted($trip)) );

	# check if thread exists, and get lasthit value
	my ($parent_res,$lasthit);
	if($parent)
	{
		$parent_res=get_parent_post($parent) or make_error(S_NOTHREADERR, 1);
		$lasthit=$$parent_res{lasthit};
		$sticky=$$parent_res{sticky};
		$lock=$$parent_res{locked};
		$autosage=$$parent_res{autosage};

		# Forbid posting into locked thread
		if ($$parent_res{locked} && !$admin_post_mode)
		{
			make_error(S_THREADLOCKED);
		}
	}
	else
	{
		$lasthit=$time;
	}


	# kill the name if anonymous posting is being enforced
	if($board->option('FORCED_ANON'))
	{
		$name='';
		$trip='';
		if($email=~/sage/i) { $email='sage'; }
		else { $email=''; }
	}

	# clean up the inputs
	$email=clean_string(decode_string($email,CHARSET));
	$subject=clean_string(decode_string($subject,CHARSET));

	# fix up the email/link
	$email="mailto:$email" if $email and $email!~/^$protocol_re:/;

	# format comment
	$comment=format_comment(clean_string(decode_string($comment,CHARSET))) unless $no_format;
	$comment.=$postfix;

	# insert default values for empty fields
	$parent=0 unless $parent;
	$name=make_anonymous($ip,$time) unless $name or $trip;
	$subject=$board->option('S_ANOTITLE') unless $subject;
	$comment=$board->option('S_ANOTEXT') unless $comment;

	# flood protection - must happen after inputs have been cleaned up
	flood_check($numip,$time,$comment,$file,$parent);

	# Manager and deletion stuff - duuuuuh?

	# make admin name
	$capcode = 1 if($admin_post && $capcode);

	# replace name if sage
	$name=$board->option('S_SAGENAME') if($board->option('REPLACE_SAGE_NAMES') && $email=~/sage/i);

	# generate ID code if enabled
	my $id;
	$id='ID:'.make_id_code($ip,$time,$email) if($board->option('DISPLAY_ID'));

	# copy file, do checksums, make thumbnail, etc
	my ($filename,$md5,$width,$height,$thumbnail,$tn_width,$tn_height,$origname,$info)=process_file($file,$uploadname,$time,$parent) if($file);

	# finally, write to the database
	my $num = 0;
	eval {
		$dbh->begin_work();

		my $sth=$dbh->prepare("UPDATE `".SQL_COUNTERS_TABLE."` SET counter = counter + 1 WHERE section = ?;");
		$sth->execute( $board->path() );

		$sth=$dbh->prepare("SELECT counter FROM `".SQL_COUNTERS_TABLE."` WHERE section = ?;");
		$sth->execute( $board->path() );
		$num=($sth->fetchrow_array())[0];

		$sth=$dbh->prepare("INSERT INTO `".SQL_TABLE."` VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);") or make_error(S_SQLFAIL);
		$sth->execute(
			$num,$parent,$time,$lasthit,$numip,
			$id,$name,$trip,$email,$subject,$password,$comment,
			$filename,$size,$md5,$width,$height,$thumbnail,$tn_width,$tn_height,$origname,$info,
			$admin_post,$capcode,$sticky,$lock,$autosage,$loc,$ssl,$board->path()
		);
		$sth->finish();

		$dbh->commit();
	};
	if ($@) {
		eval { $dbh->rollback(); };
		make_error(S_SQLFAIL);
	}
	
	my $sth;
	if($parent) # bumping
	{
        # if parent has autosage set, the sage_count SQL query does not need to be executed
        my $bumplimit = ($autosage or $board->option('MAX_RES') and sage_count($parent_res) > $board->option('MAX_RES'));

        # check for sage, or too many replies
        unless ( $email =~ /sage/i or $bumplimit ) {
            $sth =
              $dbh->prepare(
                "UPDATE " . SQL_TABLE . " SET lasthit=? WHERE section=? AND (num=? OR parent=?);"
              );
            $sth->execute( $time, $board->path(), $parent, $parent ) or make_sql_error();
        }
        if ( $bumplimit and !$autosage ) {
            $sth =
              $dbh->prepare(
                  "UPDATE " . SQL_TABLE . " SET autosage=1 WHERE section=? AND (num=? OR parent=?);"
              );
            $sth->execute( $board->path(), $parent, $parent ) or make_sql_error();
        }
	}

	# remove old threads from the database
	trim_database();

	# find out what our new thread number is
	if($filename)
	{
		$sth=$dbh->prepare("SELECT num FROM `".SQL_TABLE."` WHERE timestamp=? AND image=? AND section=?;") or make_error(S_SQLFAIL);
		$sth->execute($time,$filename,$board->path) or make_error(S_SQLFAIL);
	}
	else
	{
		$sth=$dbh->prepare("SELECT num FROM `".SQL_TABLE."` WHERE timestamp=? AND comment=? AND section=?;") or make_error(S_SQLFAIL);
		$sth->execute($time,$comment,$board->path) or make_error(S_SQLFAIL);
	}
	my $num=($sth->fetchrow_array())[0];

	$sth->finish();

	# set the name, email and password cookies
	make_cookies(name=>$c_name,email=>$c_email,password=>$c_password,gb2=>$c_gb2,
	-charset=>CHARSET,-autopath=>$board->option('COOKIE_PATH')); # yum!

	# redirect to the appropriate page
	if(!$ajax)
	{
		if($c_gb2 =~/thread/i)
		{
			if($parent) { make_http_forward($board->path().'/'.$board->option('RES_DIR').$parent.$page_ext.($num?"#$num":"")); }
			elsif($num)	{ make_http_forward($board->path().'/'.$board->option('RES_DIR').$num.$page_ext); }
			else { make_http_forward($board->path().'/'.$board->option('HTML_SELF')); } # shouldn't happen
		}
		else
		{
			make_http_forward($board->path().'/'.$board->option('HTML_SELF'));
		}
	}
	else
	{
		my %redirect = ( 
			parent => $parent,
			num => $num
		);
		my %error = (
			error_msg => 'Success',
			error_code => 200
		);

		make_json_header();
		print $JSON->encode({ data => \%redirect, status => \%error });
	}
}

sub is_whitelisted($)
{
	my ($numip)=@_;
	my ($sth);

	$sth=$dbh->prepare("SELECT count(*) FROM `".SQL_ADMIN_TABLE."` WHERE type='whitelist' AND ? & ival2 = ival1 & ival2;") or make_error(S_SQLFAIL);
	$sth->execute($numip) or make_error(S_SQLFAIL);
	my $ip_is_whitelisted = ($sth->fetchrow_array())[0];
	$sth->finish();

	return 1 if($ip_is_whitelisted);

	return 0;
}

sub is_trusted($)
{
	my ($trip)=@_;
	my ($sth);
		$sth=$dbh->prepare("SELECT count(*) FROM `".SQL_ADMIN_TABLE."` WHERE type='trust' AND sval1 = ?;") or make_error(S_SQLFAIL);
		$sth->execute($trip) or make_error(S_SQLFAIL);
	my $tripfag_is_trusted = ($sth->fetchrow_array())[0];
	$sth->finish();

		return 1 if($tripfag_is_trusted);

	return 0;
}

sub ban_check {
	my ( $numip, $name, $subject, $comment ) = @_;
	my ($sth,$sth2,$row);
	my (@bans);
    my $ip = dec_to_dot($numip);

	$sth=$dbh->prepare( "SELECT count(*) FROM `".SQL_ADMIN_TABLE."` WHERE type='ipban' AND ? & ival2 = ival1 & ival2;" ) or make_error(S_SQLFAIL);
	$sth->execute($numip) or make_error(S_SQLFAIL);

	$sth2=$dbh->prepare("SELECT comment,ival2,sval1 FROM `".SQL_ADMIN_TABLE."` WHERE type='ipban' AND ? & ival2 = ival1 & ival2;");
	$sth2->execute($numip) or make_error(S_SQLFAIL);
	while ($row = get_decoded_hashref($sth2))
	{
		my $ban;
		$$ban{ip}       = $ip;
		$$ban{network}  = dec_to_dot($numip & $$row{ival2});
		$$ban{setbits}  = unpack("%32b*", pack('N', $$row{ival2}));
		$$ban{showmask} = $$ban{setbits} < 32 ? 1 : 0;
		$$ban{reason}   = $$row{comment};
		$$ban{expires}  = 0; #later
		push @bans, $ban;
	}
	make_ban( S_BADHOST, @bans ) if (@bans);

# fucking mysql...
#	$sth=$dbh->prepare("SELECT count(*) FROM ".SQL_ADMIN_TABLE." WHERE type='wordban' AND ? LIKE '%' || sval1 || '%';") or make_error(S_SQLFAIL);
#	$sth->execute($comment) or make_error(S_SQLFAIL);
#
#	make_error(S_STRREF) if(($sth->fetchrow_array())[0]);

	$sth = $dbh->prepare( "SELECT sval1,comment FROM `".SQL_ADMIN_TABLE."` WHERE type='wordban';") or make_error(S_SQLFAIL);
	$sth->execute() or make_error(S_SQLFAIL);

	while ( $row = $sth->fetchrow_arrayref() ) {
		my $regexp = quotemeta $$row[0];

		#		make_error(S_STRREF) if($comment=~/$regexp/);
		if ( $comment =~ /$regexp/ ) {
			$comment = $$row[1];

			# make_error($$row[1]);
		}
		make_error(S_STRREF) if ( $name    =~ /$regexp/ );
		make_error(S_STRREF) if ( $subject =~ /$regexp/ );
	}

	# etc etc etc

	return (0);
}

sub word_check
{
	my ($comment)=@_;
	my ($sth,$row);

	$sth = $dbh->prepare( "SELECT sval1,comment FROM `".SQL_ADMIN_TABLE."` WHERE type='wordban';") or make_error(S_SQLFAIL);
	$sth->execute() or make_error(S_SQLFAIL);

	while ( $row = $sth->fetchrow_arrayref() ) {
		my $regexp = quotemeta $$row[0];

		if ( $comment =~ /$regexp/ ) {
			$comment =~ s/$1/\[wordban\]/g;
		}
	}
	return ($comment);
}

sub dnsbl_check {
	my ($ip) = @_;
	my @errors;

	foreach my $dnsbl_info ( @{&DNSBL_INFOS} ) {
		my $dnsbl_host   = @$dnsbl_info[0];
		my $dnsbl_answers = @$dnsbl_info[1];
		my $result;
		my $resolver;
		my $reverse_ip    = join( '.', reverse split /\./, $ip );
		my $dnsbl_request = join( '.', $reverse_ip,        $dnsbl_host );

		$resolver = Net::DNS::Resolver->new;
		my $bgsock = $resolver->bgsend($dnsbl_request);
		my $sel    = IO::Select->new($bgsock);

		my @ready = $sel->can_read(DNSBL_TIMEOUT);
		if (@ready) {
			foreach my $sock (@ready) {
				if ( $sock == $bgsock ) {
					my $packet = $resolver->bgread($bgsock);
					if ($packet) {
						foreach my $rr ( $packet->answer ) {
							next unless $rr->type eq "A";
							$result = $rr->address;
							last;
						}
					}
					$bgsock = undef;
				}
				$sel->remove($sock);
				$sock = undef;
			}
		}

        foreach (@{$dnsbl_answers}) {
            if ( $result eq $_ ) {
                push @errors, sprintf($ajax_mode ? "IP Found in %s blacklist" : S_DNSBL, $dnsbl_host);
            }
        }
	}
	make_ban( S_BADHOSTPROXY, { ip => $ip, showmask => 0, reason => shift(@errors), expires => 0 } ) if @errors;
}

sub flood_check($$$$;$)
{
	my ($ip,$time,$comment,$file,$parent)=@_;
	my ($sth,$maxtime);
	my $board_path = $board->path();

	if(!$parent)
	{
		# check for repeated threads
		$maxtime=$time-($board->option('RENZOKU4'));
		$sth=$dbh->prepare("SELECT count(*) FROM `".SQL_TABLE."` WHERE parent=0 AND ip=? AND section=? AND timestamp>$maxtime;") or make_error(S_SQLFAIL);
		$sth->execute($ip,$board_path) or make_error(S_SQLFAIL);
		make_error(sprintf(S_RENZOKU4, ($board->option('RENZOKU4')/60))) if(($sth->fetchrow_array())[0]);
	}
	else
	{
		if($file)
		{
			# check for to quick file posts
			$maxtime=$time-($board->option('RENZOKU2'));
			$sth=$dbh->prepare("SELECT count(*) FROM `".SQL_TABLE."` WHERE ip=? AND section=? AND timestamp>$maxtime;") or make_error(S_SQLFAIL);
			$sth->execute($ip,$board_path) or make_error(S_SQLFAIL);
			make_error(S_RENZOKU2) if(($sth->fetchrow_array())[0]);
		}
		else
		{
			# check for too quick replies or text-only posts
			$maxtime=$time-($board->option('RENZOKU'));
			$sth=$dbh->prepare("SELECT count(*) FROM `".SQL_TABLE."` WHERE ip=? AND section=? AND timestamp>$maxtime;") or make_error(S_SQLFAIL);
			$sth->execute($ip,$board_path) or make_error(S_SQLFAIL);
			make_error(S_RENZOKU) if(($sth->fetchrow_array())[0]);

			# check for repeated messages
			$maxtime=$time-($board->option('RENZOKU3'));
			$sth=$dbh->prepare("SELECT count(*) FROM `".SQL_TABLE."` WHERE ip=? AND comment=? AND section=? AND timestamp>$maxtime;") or make_error(S_SQLFAIL);
			$sth->execute($ip,$comment,$board_path) or make_error(S_SQLFAIL);
			make_error(S_RENZOKU3) if(($sth->fetchrow_array())[0]);
		}
	}
	$sth->finish();
}

#
# Proxy Checking
# removed

#
# String Formatting
#

sub format_comment($)
{
	my ($comment)=@_;

	# hide >>1 references from the quoting code
	$comment=~s/&gt;&gt;([0-9\-]+)/&gtgt;$1/g;

	my $handler=sub # fix up >>1 references
	{
		my $line=shift;

		$line =~ s/&gtgt;([0-9]+)/<!--reflink-->&gt;&gt;$1/g;

		return $line;
	};

	# if($board->option('ENABLE_WAKABAMARK')) { $comment=do_wakabamark($comment,$handler) }
	# else { $comment="<p>".simple_format($comment,$handler)."</p>" }

	if ($board->option('ENABLE_WAKABAMARK')) {
		$comment = do_wakabamark($comment, $handler);
	} elsif ($board->option('ENABLE_BBCODE')) {
		# do_bbcode() will always try to apply (at least some) wakabamark
		$comment = do_bbcode($comment, $handler);
	} else {
		$comment = "<p>" . simple_format($comment, $handler) . "</p>";
	}

	# fix <blockquote> styles for old stylesheets
	$comment=~s/<blockquote>/<blockquote class="unkfunc">/g;

	# restore >>1 references hidden in code locks
	$comment=~s/&gtgt;/&gt;&gt;/g;

	return $comment;
}

sub simple_format($@)
{
	my ($comment,$handler)=@_;
	return join "<br />",map
	{
		my $line=$_;

		# make URLs into links
		$line=~s{(https?://[^\s<>"]*?)((?:\s|<|>|"|\.|\)|\]|!|\?|,|&#44;|&quot;)*(?:[\s<>"]|$))}{\<a href="$1"\>$1\</a\>$2}sgi;

		# colour quoted sections if working in old-style mode.
		$line=~s!^(&gt;.*)$!\<span class="unkfunc"\>$1\</span\>!g unless($board->option('ENABLE_WAKABAMARK'));

		$line=$handler->($line) if($handler);

		$line;
	} split /\n/,$comment;
}

sub encode_string($)
{
	my ($str)=@_;

	# return $str unless($has_encode);
	return encode(CHARSET,$str,0x0400);
}

sub make_anonymous($$)
{
	my ($ip,$time)=@_;

	return $board->option('S_ANONAME') unless($board->option('SILLY_ANONYMOUS'));

	my $string=$ip;
	$string.=",".int($time/86400) if($board->option('SILLY_ANONYMOUS')=~/day/i);
	$string.=",".$ENV{SCRIPT_NAME} if($board->option('SILLY_ANONYMOUS')=~/board/i);

	srand unpack "N",hide_data($string,4,"silly",SECRET);

	return cfg_expand("%G% %W%",
		W => ["%B%%V%%M%%I%%V%%F%","%B%%V%%M%%E%","%O%%E%","%B%%V%%M%%I%%V%%F%","%B%%V%%M%%E%","%O%%E%","%B%%V%%M%%I%%V%%F%","%B%%V%%M%%E%"],
		B => ["B","B","C","D","D","F","F","G","G","H","H","M","N","P","P","S","S","W","Ch","Br","Cr","Dr","Bl","Cl","S"],
		I => ["b","d","f","h","k","l","m","n","p","s","t","w","ch","st"],
		V => ["a","e","i","o","u"],
		M => ["ving","zzle","ndle","ddle","ller","rring","tting","nning","ssle","mmer","bber","bble","nger","nner","sh","ffing","nder","pper","mmle","lly","bling","nkin","dge","ckle","ggle","mble","ckle","rry"],
		F => ["t","ck","tch","d","g","n","t","t","ck","tch","dge","re","rk","dge","re","ne","dging"],
		O => ["Small","Snod","Bard","Billing","Black","Shake","Tilling","Good","Worthing","Blythe","Green","Duck","Pitt","Grand","Brook","Blather","Bun","Buzz","Clay","Fan","Dart","Grim","Honey","Light","Murd","Nickle","Pick","Pock","Trot","Toot","Turvey"],
		E => ["shaw","man","stone","son","ham","gold","banks","foot","worth","way","hall","dock","ford","well","bury","stock","field","lock","dale","water","hood","ridge","ville","spear","forth","will"],
		G => ["Albert","Alice","Angus","Archie","Augustus","Barnaby","Basil","Beatrice","Betsy","Caroline","Cedric","Charles","Charlotte","Clara","Cornelius","Cyril","David","Doris","Ebenezer","Edward","Edwin","Eliza","Emma","Ernest","Esther","Eugene","Fanny","Frederick","George","Graham","Hamilton","Hannah","Hedda","Henry","Hugh","Ian","Isabella","Jack","James","Jarvis","Jenny","John","Lillian","Lydia","Martha","Martin","Matilda","Molly","Nathaniel","Nell","Nicholas","Nigel","Oliver","Phineas","Phoebe","Phyllis","Polly","Priscilla","Rebecca","Reuben","Samuel","Sidney","Simon","Sophie","Thomas","Walter","Wesley","William"],
	);
}

sub make_id_code($$$)
{
	my ($ip,$time,$link)=@_;

	return $board->option('EMAIL_ID') if($link and $board->option('DISPLAY_ID')=~/link/i);
	return $board->option('EMAIL_ID') if($link=~/sage/i and $board->option('DISPLAY_ID')=~/sage/i);

	return resolve_host(get_remote_addr()) if($board->option('DISPLAY_ID')=~/host/i);
	return get_remote_addr() if($board->option('DISPLAY_ID')=~/ip/i);

	my $string="";
	$string.=",".int($time/86400) if($board->option('DISPLAY_ID')=~/day/i);
	$string.=",".$ENV{SCRIPT_NAME} if($board->option('DISPLAY_ID')=~/board/i);

	return mask_ip(get_remote_addr(),make_key("mask",SECRET,32).$string) if($board->option('DISPLAY_ID')=~/mask/i);

	return hide_data($ip.$string,6,"id",SECRET,1);
}

sub get_post($)
{
	my ($thread)=@_;
	my ($sth);

	$sth=$dbh->prepare("SELECT * FROM `".SQL_TABLE."` WHERE num=? AND section=?;") or make_error(S_SQLFAIL);
	$sth->execute( $thread,$board->path() ) or make_error(S_SQLFAIL);

	return $sth->fetchrow_hashref();
}

sub get_parent_post($)
{
	my ($thread)=@_;
	my ($sth);

	$sth=$dbh->prepare("SELECT * FROM `".SQL_TABLE."` WHERE num=? AND parent=0 AND section=?;") or make_error(S_SQLFAIL);
	$sth->execute( $thread,$board->path() ) or make_error(S_SQLFAIL);
	my $return = $sth->fetchrow_hashref();
	$sth->finish();

	return ($return);
}

sub sage_count($)
{
	my ($parent)=@_;
	my ($sth);

	$sth=$dbh->prepare("SELECT count(*) FROM `".SQL_TABLE."` WHERE parent=? AND NOT ( timestamp<? AND ip=? );") or make_error(S_SQLFAIL);
	$sth->execute($$parent{num},$$parent{timestamp}+($board->option('NOSAGE_WINDOW')),$$parent{ip}) or make_error(S_SQLFAIL);
	my $return = ($sth->fetchrow_array())[0];
	$sth->finish();

	return $return;
}

sub get_file_size($)
{
	my ($file) = @_;
	my (@filestats, $max_size);
	my ($size) = 0;
	my ($ext) = $file =~ /\.([^\.]+)$/;
	my $sizehash = $board->option('FILESIZES');

	@filestats = stat($file);
	$size = $filestats[7];
	$max_size = $board->option('MAX_KB');
	$max_size = $$sizehash{$ext} if ($$sizehash{$ext});
	# or round using: int($size / 1024 + 0.5)

	make_error(S_TOOBIG) if ($size > $max_size * 1024);
	make_error(S_TOOBIGORNONE) if ($size == 0);  # check for small files, too?

	return ($size);
}

sub process_file($$$;$)
{
	my ($file,$uploadname,$time,$parent)=@_;
	my $filetypes=$board->option('FILETYPES');

	# make sure to read file in binary mode on platforms that care about such things
	binmode $file;

	# analyze file and check that it's in a supported format
	my ($ext,$width,$height)=analyze_image($file,$uploadname);

	my $known=($width or $$filetypes{$ext});

	make_error(S_BADFORMAT) unless($board->option('ALLOW_UNKNOWN') or $known);
	make_error(S_BADFORMAT) if(grep { $_ eq $ext } $board->option('FORBIDDEN_EXTENSIONS'));
	make_error(S_TOOBIG) if($board->option('MAX_IMAGE_WIDTH') and $width>($board->option('MAX_IMAGE_WIDTH')));
	make_error(S_TOOBIG) if($board->option('MAX_IMAGE_HEIGHT') and $height>($board->option('MAX_IMAGE_HEIGHT')));
	make_error(S_TOOBIG) if($board->option('MAX_IMAGE_PIXELS') and $width*$height>($board->option('MAX_IMAGE_PIXELS')));

	# jpeg -> jpg
	$uploadname =~ s/\.jpeg$/\.jpg/i;

	# generate random filename - fudges the microseconds
	my $filebase=$time.sprintf("%03d",int(rand(1000)));
	my $filename=$board->path.'/'.$board->option('IMG_DIR').$filebase.'.'.$ext;
	my $thumbnail=$board->path.'/'.$board->option('THUMB_DIR').$filebase;
	$filename.=$board->option('MUNGE_UNKNOWN') unless($known);

	if ( $ext eq "png" or $ext eq "svg" )
	{
		$thumbnail .= "s.png";
	}
	elsif ( $ext eq "gif" )
	{
		$thumbnail .= "s.gif";
	}
	else
	{
		$thumbnail .= "s.jpg";
	}

	# do copying and MD5 checksum
	my ($md5,$md5ctx,$buffer);

	# prepare MD5 checksum if the Digest::MD5 module is available
	eval 'use Digest::MD5 qw(md5_hex)';
	$md5ctx=Digest::MD5->new unless($@);

	# copy file
	open (OUTFILE,">>$filename") or make_error(S_NOTWRITE);
	binmode OUTFILE;
	while (read($file,$buffer,1024)) # should the buffer be larger?
	{
		print OUTFILE $buffer;
		$md5ctx->add($buffer) if($md5ctx);
	}
	close $file;
	close OUTFILE;

	if($md5ctx) # if we have Digest::MD5, get the checksum
	{
		$md5=$md5ctx->hexdigest();
	}
	else # otherwise, try using the md5sum command
	{
		my $md5sum=`md5sum $filename`; # filename is always the timestamp name, and thus safe
		($md5)=$md5sum=~/^([0-9a-f]+)/ unless($?);
	}

	if($md5 && (($parent && $board->option('DUPLICATE_DETECTION') eq 'thread') || $board->option('DUPLICATE_DETECTION') eq 'board')) # if we managed to generate an md5 checksum, check for duplicate files
	{
		my $sth;
		
		if ($board->option('DUPLICATE_DETECTION') eq 'thread') # Check dupes in same thread
		{
			$sth=$dbh->prepare("SELECT * FROM `".SQL_TABLE."` WHERE md5=? AND (parent=? OR num=?) AND section=?;") or make_error(S_SQLFAIL);
			$sth->execute( $md5, $parent, $parent, $board->path() ) or make_error(S_SQLFAIL);
		}
		else # Check dupes throughout board
		{
			$sth=$dbh->prepare("SELECT * FROM `".SQL_TABLE."` WHERE md5=? AND section=?;") or make_error(S_SQLFAIL);
			$sth->execute( $md5, $board->path() ) or make_error(S_SQLFAIL);
		}
		
		if(my $match=$sth->fetchrow_hashref())
		{
			unlink $filename; # make sure to remove the file
			make_error(sprintf(S_DUPE,get_reply_link($$match{num},$parent)));
		}
		
		$sth->finish();
	}

	# do thumbnail
	my ($tn_width,$tn_height,$tn_ext);

	if(!$width or !$filename =~ /\.svg$/) # unsupported file
	{
		$thumbnail = undef;
	}
	elsif($width>($board->option('MAX_W'))
		or $height>($board->option('MAX_H'))
		or $board->option('THUMBNAIL_SMALL')
		or $filename =~ /\.svg$/ # why not check $ext?
		or $ext eq 'pdf'
		or $ext eq 'webm'
		or $ext eq 'mp4')
	{
		if($width<=($board->option('MAX_W')) and $height<=($board->option('MAX_H')))
		{
			$tn_width=$width;
			$tn_height=$height;
		}
		else
		{
			$tn_width=$board->option('MAX_W');
			$tn_height=int(($height*($board->option('MAX_W')))/$width);

			if($tn_height>($board->option('MAX_H')))
			{
				$tn_width=int(($width*($board->option('MAX_H')))/$height);
				$tn_height=$board->option('MAX_H');
			}
		}

		if ($ext eq 'pdf' or $ext eq 'svg') { # cannot determine dimensions for these files
			undef($width);
			undef($height);
			$tn_width=$board->option('MAX_W');
			$tn_height=$board->option('MAX_H');
		}

		if($board->option('STUPID_THUMBNAILING')) { $thumbnail=$filename }
		else
		{
			if ($ext eq 'webm' or $ext eq 'mp4')
			{
				undef($thumbnail)
				  unless(make_video_thumbnail($filename,$thumbnail,$tn_width,$tn_height,$board->option('MAX_W'),$board->option('MAX_H'),VIDEO_CONVERT_COMMAND));
			}
			else
			{
				undef($thumbnail) unless(make_thumbnail($filename,$thumbnail,$tn_width,$tn_height,$board->option('THUMBNAIL_QUALITY'),CONVERT_COMMAND));
			}

			# get the thumbnail size created by external program
			if ($thumbnail and ($ext eq 'pdf' or $ext eq 'svg'))
			{
				open THUMBNAIL,$thumbnail;
				binmode THUMBNAIL;
				($tn_ext, $tn_width, $tn_height) = analyze_image(\*THUMBNAIL, $thumbnail);
				close THUMBNAIL;
			}
		}
	}
	else
	{
		$tn_width=$width;
		$tn_height=$height;
		$thumbnail=$filename;
	}

    my ($info) = get_meta_markup($filename, &CHARSET);

	my $origname=$uploadname;
	$origname=~s!^.*[\\/]!!; # cut off any directory in filename
	$origname=~tr/\0//d; # fix for dangerous 0-day

	if($board->option('ENABLE_LOAD'))
	{
		# only called if files to be distributed across web     
		$ENV{SCRIPT_NAME}=~m!^(.*/)[^/]+$!;
		my $root=$1;
		system($board->option('LOAD_SENDER_SCRIPT')." $filename $root $md5 &");
	}
	
	chmod 0644, $filename; # Make file world-readable
	chmod 0644, $thumbnail if defined($thumbnail); # Make thumbnail (if any) world-readable
	
	my $board_path = $board->path(); # Clear out the board path name.
	$filename =~ s/^${board_path}\///;
	$thumbnail =~ s/^${board_path}\///;

	return ($filename,$md5,$width,$height,$thumbnail,$tn_width,$tn_height,$origname,$info);
}


#
# Deleting
#

sub delete_stuff($$$$$$$$@)
{
	my ($password,$fileonly,$archive,$parent,$ajax,$admin,$admin_deletion_mode,$caller,@posts)=@_;
	my ($post,$ip);
	my $deletebyip = 0;
	my $noko = 1; # try to stay in thread after deletion by default	
	$ajax_mode = 1 if $ajax;

	if($admin_deletion_mode)
	{
		check_password($admin,'');
	}

    if ( !$password and !$admin_deletion_mode ) { $deletebyip = 1; }

    make_error(S_BADDELPASS)
      unless (
        ( !$password and $deletebyip )
        or ( $password and !$deletebyip )
        or $admin_deletion_mode
      );    # allow deletion by ip with empty password
            # no password means delete always

	# no password means delete always
	$password="" if($admin_deletion_mode); 

	my (@errors, @AoE);

	foreach $post (@posts)
	{
		$ip = delete_post($post,$password,$fileonly,$deletebyip);
		if ($ip !~ /\d+\.\d+\.\d+\.\d+/) # Function returned with error string
		{
			push (@errors,{post=>$post, error=>$ip});
			next;
		}
		$noko = 0 if ( $parent and $post eq $parent ); # the thread is deleted and cannot be redirected to		
	}

	return if ($caller eq 'internal' || $caller eq "internal_delete_all()"); # If not called directly, return to the calling function

	if (!@errors)
	{
		my $redir;
		if($admin_deletion_mode)
		{ $redir = get_secure_script_name()."?task=mpanel&board=".$board->path(); }
		elsif($noko == 1 and $parent)
		{ $redir = $board->path().'/'.$board->option('RES_DIR').$parent.$page_ext; }
		else
		{ $redir = $board->path.'/'.$board->option('HTML_SELF'); }

		if($ajax) {
			make_json_header();
            print $JSON->encode({redir => $redir});
        } else {
        	make_http_forward($redir);
        }
	}
	else
	{
		if(!$ajax)
		{
			foreach (@errors)
			{
				my $string = sprintf("Post %d: %s", $$_{post}, $$_{error});
				push (@AoE, $string);
			}
			make_error(join("<br />", @AoE));
		}
		else
		{
			make_error(\@errors);
		}
	}
}

sub delete_post($$$$)
{
	my ($post,$password,$fileonly,$deletebyip)=@_;
	my ($sth,$row,$res,$reply);

	my $thumb=$board->option('THUMB_DIR');
	my $archive=$board->option('ARCHIVE_DIR');
	my $src=$board->option('IMG_DIR');
	my $postinfo;
    my $numip = dot_to_dec(get_remote_addr()); # do not use $ENV{REMOTE_ADDR}

	$sth=$dbh->prepare("SELECT * FROM `".SQL_TABLE."` WHERE num=? AND section=?;") or return S_SQLFAIL;
	$sth->execute( $post, $board->path() ) or return S_SQLFAIL;

	if($row=$sth->fetchrow_hashref())
	{
		return S_BADDELPASS if($password and $$row{password} ne $password);
        return S_BADDELIP if ( $deletebyip and ( $numip and $$row{ip} ne $numip ) );

		return "This was posted by a moderator or admin and cannot be deleted this way." if ($password and $$row{adminpost});

		backup_stuff($row) if (ENABLE_POST_BACKUP);

		unless($fileonly)
		{
			# remove files from comment and possible replies
			$sth=$dbh->prepare("SELECT image,thumbnail FROM `".SQL_TABLE."` WHERE (num=? OR parent=?) AND section=?") or return S_SQLFAIL;
			$sth->execute($post,$post,$board->path) or return S_SQLFAIL;

			while($res=$sth->fetchrow_hashref())
			{
				system($board->path.'/'.$board->option('LOAD_SENDER_SCRIPT')." $$res{image} &") if($board->option('ENABLE_LOAD'));
				
				# delete images if they exist
				unlink $board->path.'/'.$$res{image};
				unlink $board->path.'/'.$$res{thumbnail} if($$res{thumbnail}=~/^$thumb/);
			}

			# remove post and possible replies
			$sth=$dbh->prepare("DELETE FROM `".SQL_TABLE."` WHERE (num=? OR parent=?) AND section=?;") or make_error(S_SQLFAIL);
			$sth->execute($post,$post,$board->path()) or make_error(S_SQLFAIL);

			# prevent GHOST BUMPING by hanging a thread where it belongs: at the time of the last non sage post
			if ($board->option('PREVENT_GHOST_BUMPING')) {
				# first find the parent of the post
				my $parent = $$row{parent};
				if ( $parent != 0 ) {
					# its actually a post in a thread, not a thread itself
					# find the thread to check for autosage
					$sth = $dbh->prepare("SELECT * FROM `".SQL_TABLE."` WHERE num=? AND section=?") or return S_SQLFAIL;
					$sth->execute( $parent, $board->path() );
					my $threadRow = $sth->fetchrow_hashref();
					if ( $threadRow and !$$threadRow{autosage} )
					{
						my $sth2;
						$sth2 = $dbh->prepare( "SELECT * FROM `".SQL_TABLE."` WHERE section=? AND (num=? OR parent=?) ORDER BY timestamp DESC") or return S_SQLFAIL;
						$sth2->execute( $board->path(), $parent, $parent );
						my $postRow;
						my $foundLastNonSage = 0;
						while ( ( $postRow = $sth2->fetchrow_hashref() )
							and $foundLastNonSage == 0 )
						{
							# takes into account, that threads can have SAGE and are of course counted as
							# normal post! this is a special case where we accept sage and the timestamp as valid
							if (  !( $$postRow{email} =~ /sage/i )
								or ( $$postRow{parent} == 0 ) )
							{
								$foundLastNonSage = $$postRow{timestamp};
							}
						}
						if ($foundLastNonSage)
						{
							# var now contains the timestamp we have to update lasthit to
							my $upd;
							$upd = $dbh->prepare( "UPDATE `".SQL_TABLE."` SET lasthit=? WHERE parent=? OR num=? AND section=?;" ) or return S_SQLFAIL;
							$upd->execute( $foundLastNonSage, $parent, $parent, $board->path() ) or make_error( S_SQLFAIL . " " . $dbh->errstr() );
							$upd->finish();
						}
						$sth2->finish();
					}
				}
			} ######
		}
		else # remove just the image and update the database
		{
			if($$row{image})
			{
				system($board->path.'/'.$board->option('LOAD_SENDER_SCRIPT')." $$row{image} &") if($board->option('ENABLE_LOAD'));

				# remove images
				unlink $board->path.'/'.$$row{image};
				unlink $board->path.'/'.$$row{thumbnail} if($$row{thumbnail}=~/^$thumb/);

				$sth=$dbh->prepare("UPDATE `".SQL_TABLE."` SET size=0,md5=null,thumbnail=null WHERE num=? AND section=?;") or return S_SQLFAIL;
				$sth->execute($post,$board->path()) or make_error(S_SQLFAIL);
			}
		}

		$postinfo = dec_to_dot($$row{ip});
	}

	$sth->finish();

	return $postinfo;
}

#
# Editing/Deletion Undoing
#

# Present backup posts to admin

sub make_backup_posts_panel($$$)
{
	my ($admin, $page)=@_;
	my ($sth,$row,@threads);

	my ($username, $type) = check_password($admin, '');
	
	# Grab board posts
	if ($page =~ /^t\d+$/)
	{
		$page =~ s/^t//g;
		$sth=$dbh->prepare("SELECT * FROM `".SQL_BACKUP_TABLE."` WHERE (postnum=? OR parent=?) AND board_name = ? ORDER BY postnum ASC;") or make_error(S_SQLFAIL);
		$sth->execute($page,$page,$board->path()) or make_error(S_SQLFAIL);
		
		$row = get_decoded_hashref($sth);
		make_error("Thread not found in backup.") if !$row;
		my @thread;
		push @thread, $row;
		while ($row=get_decoded_hashref($sth))
		{
			my $base_filename= $$row{image};
			$base_filename=~s!^.*[\\/]!!; 			# cut off any directory in filename
			
			$$row{image} = '/'.$board->path.'/'.$board->option('ARCHIVE_DIR').$board->option('BACKUP_DIR').$base_filename;
			undef($$row{image}) unless($base_filename);

			my $thumb_dir = $board->option('THUMB_DIR');
			if ($$row{thumbnail} =~ /^$thumb_dir/)
			{
				my $base_thumbnail = $$row{thumbnail};
				$base_thumbnail =~ s!^.*[\\/]!!;		# Do the same for the thumbnail.
				$$row{thumbnail} = '/'.$board->path.'/'.$board->option('ARCHIVE_DIR').$board->option('BACKUP_DIR').$base_thumbnail;
			}

			push @thread, $row;
		}
		push @threads,{posts=>[@thread]};
		
		make_http_header();
		print encode_string(BACKUP_PANEL_TEMPLATE->(
			admin=>$admin,
			board=>$board,
			postform=>($board->option('ALLOW_TEXTONLY') or $board->option('ALLOW_IMAGES')),
			image_inp=>$board->option('ALLOW_IMAGES'),
			textonly_inp=>0,
			threads=>\@threads,
			thread=>$page,
			lockedthread=>$thread[0]{locked},
			usertype=>$type,
			username=>$username,
			parent=>$page,
			stylesheets=>get_stylesheets()));
	}
	else
	{
		# Grab count of threads and orphaned posts.
		$sth=$dbh->prepare("SELECT COUNT(1) FROM `".SQL_BACKUP_TABLE."` A  WHERE (parent=0 OR (parent>0 AND NOT EXISTS (SELECT * FROM `".SQL_BACKUP_TABLE."` B WHERE A.parent = B.postnum LIMIT 1))) AND board_name=?;") or make_error(S_SQLFAIL);
		$sth->execute($board->path()) or make_error(S_SQLFAIL);

		my $threadcount = ($sth->fetchrow_array)[0];
		$sth->finish();

		# Handle page variable
		my $total = get_page_count_real($threadcount); 
		$page = $total if (($page - 1) * $board->option('IMAGES_PER_PAGE') > $threadcount);
		$page = 1 if ($page !~ /^\d+$/);
		$page = 1 if ($page <= 0);
		my $thread_offset = ($page - 1) * ($board->option('IMAGES_PER_PAGE'));
		
		# Grab the parent posts and posts without parent threads
		$sth=$dbh->prepare("SELECT * FROM `".SQL_BACKUP_TABLE."` A  WHERE (parent=0 OR (parent>0 AND NOT EXISTS (SELECT * FROM `".SQL_BACKUP_TABLE."` B WHERE A.parent = B.postnum LIMIT 1))) AND board_name=? ORDER BY timestampofarchival DESC, num ASC LIMIT ".$board->option('IMAGES_PER_PAGE')." OFFSET $thread_offset;") or make_error(S_SQLFAIL);
		$sth->execute($board->path()) or make_error(S_SQLFAIL);
		
		# Grab the thread posts in each thread
		while ($row=get_decoded_hashref($sth))
		{
			my $base_filename= $$row{image};
			$base_filename=~s!^.*[\\/]!!; 			# cut off any directory in filename

			$$row{image} = '/'.$board->path.'/'.$board->option('ARCHIVE_DIR').$board->option('BACKUP_DIR').$base_filename;
			undef($$row{image}) unless($base_filename);

			my $thumb_dir = $board->option('THUMB_DIR');
			if ($$row{thumbnail} =~ /^$thumb_dir/)
			{
				my $base_thumbnail = $$row{thumbnail};
				$base_thumbnail =~ s!^.*[\\/]!!;		# Do the same for the thumbnail.
				$$row{thumbnail} = '/'.$board->path.'/'.$board->option('ARCHIVE_DIR').$board->option('BACKUP_DIR').$base_thumbnail;
			}

			my @thread = ($row);
			my $threadnumber = $$row{postnum};
			my $imgcount = 0;
			my $postcount = 0;
			my $limit = $board->option('REPLIES_PER_THREAD');
			my $shownimages = 0;
			$$row{standalone} = 1;	# Indicates extracted post is orphaned in the database (i.e., without a parent thread)

			unless ($$row{parent}) 		# We are grabbing both threads and orphaned posts, here.
			{
				$$row{standalone} = 0;

				# Grab thread replies
				my $postcountquery=$dbh->prepare("SELECT COUNT(*) AS count, COUNT(image) AS imgcount FROM `".SQL_BACKUP_TABLE."` WHERE parent=?") or make_error(S_SQLFAIL);
				$postcountquery->execute($threadnumber) or make_error(S_SQLFAIL);
				my $postcountrow = $postcountquery->fetchrow_hashref();
				my $postcount = $$postcountrow{count};
				my $imgcount = $$postcountrow{imgcount};
				$postcountquery->finish();
			
				# Grab limits for SQL query
				my $offset = ($postcount > $board->option('REPLIES_PER_THREAD')) ? $postcount - ($board->option('REPLIES_PER_THREAD')) : 0;
			
				my $threadquery=$dbh->prepare("SELECT * FROM `".SQL_BACKUP_TABLE."` WHERE parent=? ORDER BY timestampofarchival DESC, num ASC LIMIT $limit OFFSET $offset;") or make_error(S_SQLFAIL);
				$threadquery->execute($threadnumber) or make_error(S_SQLFAIL);
				while (my $inner_row=get_decoded_hashref($threadquery))
				{
					my $base_filename= $$inner_row{image};
					$base_filename=~s!^.*[\\/]!!; 			# cut off any directory in filename

					$$inner_row{image} = '/'.$board->path.'/'.$board->option('ARCHIVE_DIR').$board->option('BACKUP_DIR').$base_filename;
					undef($$inner_row{image}) unless($base_filename);

					my $thumb_dir = $board->option('THUMB_DIR');
					if ($$inner_row{thumbnail} =~ /^$thumb_dir/)
					{
						my $base_thumbnail = $$inner_row{thumbnail};
						$base_thumbnail =~ s!^.*[\\/]!!;		# Do the same for the thumbnail.
						$$inner_row{thumbnail} = '/'.$board->path.'/'.$board->option('ARCHIVE_DIR').
							$board->option('BACKUP_DIR').$base_thumbnail;
					}

					push @thread, $inner_row;
					++$shownimages if $$inner_row{image};
				}
				$threadquery->finish();
			}
	
			push @threads,{posts=>[@thread],omit=>($postcount > $limit) ? $postcount-$limit : 0,omitimages=>($imgcount > $shownimages) ? $imgcount-$shownimages : 0};
		}
		
		$sth->finish();
		
		# make the list of pages
		my @pages=map +{ page=>$_ },(1..$total);
		foreach my $p (@pages)
		{
			if($$p{page}==0) { $$p{filename}=get_secure_script_name().'?task=postbackups&amp;board='.$board->path() } # first page
			else { $$p{filename}=get_secure_script_name().'?task=postbackups&amp;board='.$board->path().'&amp;page='.$$p{page} }
			if($$p{page}==$page) { $$p{current}=1 } # current page, no link
		}
		
		my ($prevpage,$nextpage) = ('none','none');
		$prevpage=$page-1 if($page!=1);
		$nextpage=$page+1 if($page!=$total);
	
		make_http_header();
		print encode_string(BACKUP_PANEL_TEMPLATE->(
			admin=>$admin,
			board=>$board,
			postform=>($board->option('ALLOW_TEXTONLY') or $board->option('ALLOW_IMAGES')),
			image_inp=>$board->option('ALLOW_IMAGES'),
			textonly_inp=>($board->option('ALLOW_IMAGES') and $board->option('ALLOW_TEXTONLY')),
			nextpage=>$nextpage,
			prevpage=>$prevpage,
			threads=>\@threads,
			usertype=>$type,
			username=>$username,
			pages=>\@pages,
			stylesheets=>get_stylesheets())
		);
	}
}

# Backup post to backup table.
sub backup_stuff($;$) # Delete post or thread.
{
	my $row = $_[0]; 				# First argument is post drug from database.
	my $affected_board = $_[1] || $board->path(); 	# Second (optional) argument is name of board.
	
	my $timestamp = time();

	backup_post($row,$affected_board, $timestamp);
	
	if (!$$row{parent})
	{
		# Bring up comment table for requested board.
		my $this_board;
		if ($affected_board ne $board->path())
		{
			$this_board = Board->new($affected_board)
				or make_error("Post ".$affected_board.",".$$row{num}." backed up, but board resolution failed.");
		}
		else
		{
			$this_board = $board;
		}
		
		# Grab all responses.
		my $sth = $dbh->prepare("SELECT * FROM `".SQL_TABLE."` WHERE parent=? AND section=? ORDER BY num ASC;") 
			or make_error(S_SQLFAIL);
		$sth->execute($$row{num},$this_board->path()) or make_error(S_SQLFAIL);

		my $res;
		while ($res=$sth->fetchrow_hashref())
		{
			backup_post($res,$this_board->path(),$timestamp);
		}
	}
}

sub backup_post($;$$) # Delete single post.
{
	my $row = $_[0]; 					# First argument is post drug from database.
	my $affected_board = $_[1] || $board->path(); 		# Second (optional) argument is name of board.
	my $timestamp = $_[2] || time();			# Time the post was archived, for interface sorting and thread deletion.

	my $sth;						# Database variable.
	
	# If post has already been backed up, delete any extraneous copies.
	$sth=$dbh->prepare("DELETE FROM `".SQL_BACKUP_TABLE."` WHERE board_name=? AND postnum=?;") or make_error(S_SQLFAIL);
	$sth->execute($affected_board,$$row{num}) or make_error(S_SQLFAIL);

	# Backup post.

	# finally, write to the database
	my $num = 0;
	eval {
		$dbh->begin_work();

		my $sth=$dbh->prepare("UPDATE `".SQL_COUNTERS_TABLE."` SET counter = counter + 1 WHERE section = ?;");
		$sth->execute('backups');

		$sth=$dbh->prepare("SELECT counter FROM `".SQL_COUNTERS_TABLE."` WHERE section = ?;");
		$sth->execute('backups');
		$num=($sth->fetchrow_array())[0];

		$sth=$dbh->prepare("INSERT INTO `".SQL_BACKUP_TABLE."` VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);");
		$sth->execute(
				$num,$affected_board,$$row{num},$$row{parent},$$row{timestamp},
				$$row{lasthit},$$row{ip},$$row{id},$$row{name},$$row{trip},
				$$row{email},$$row{subject},$$row{password},$$row{comment},$$row{image},
				$$row{size},$$row{md5},$$row{width},$$row{height},$$row{thumbnail},
				$$row{tn_width},$$row{tn_height},$$row{origname},$$row{meta},$$row{adminpost},
				$$row{admin_post},$$row{sticky},$$row{locked},$$row{autosage},$$row{location},
				$$row{aluhut},$timestamp
			);
		$sth->finish();

		$dbh->commit();
	};
	if ($@) {
		eval { $dbh->rollback(); };
		make_error(S_SQLFAIL);
	}
	
	if ($$row{image})
	{
		my $this_board;
		if ($affected_board ne $board->path())
		{
			$this_board = Board->new($affected_board)
				or make_error("Post ".$affected_board.",".$$row{num}." backed up, but board resolution failed.");
		}
		else
		{
			$this_board = $board;
		}
		
		my $base_filename= $$row{image};
		$base_filename=~s!^.*[\\/]!!; 			# cut off any directory in filename

		my $base_thumbnail = $$row{thumbnail};
		$base_thumbnail =~ s!^.*[\\/]!!;	# Do the same for the thumbnail.
		
		# Move image.
		copy($this_board->path().'/'.$$row{image}, $this_board->path().'/'.$this_board->option('ARCHIVE_DIR').$this_board->option('BACKUP_DIR').
			$base_filename) 
			and chmod 0644, $this_board->path().'/'.$this_board->option('ARCHIVE_DIR').$this_board->option('BACKUP_DIR').$base_filename
			if ( -e $this_board->path().'/'.$$row{image} );

		# Move thumbnail.
		my $thumb_dir = $this_board->option('THUMB_DIR');
		if (  $$row{thumbnail} =~ /^$thumb_dir/ )
		{
			   copy($this_board->path().'/'.$$row{thumbnail},$this_board->path().'/'.$this_board->option('ARCHIVE_DIR').
			$this_board->option('BACKUP_DIR').$base_thumbnail) and chmod 0644, $this_board->option('BACKUP_DIR').$base_thumbnail
			if ( -e $this_board->path().'/'.$$row{thumbnail} );
		}
	}
	
	$sth->finish();
}

sub restore_stuff($$@)
{
	my ($admin, $board_name, @num) = @_;

	# Confirm administrative rights.
	check_password($admin,'');
	
	foreach my $id (@num)
	{
		restore_post_or_thread($id, $board_name);

		# add_log_entry($username,'backup_restore',$board->path().','.$id,make_date(time()+TIME_OFFSET,DATE_STYLE),
				# dot_to_dec($ENV{REMOTE_ADDR}),0,time());
	}

	make_http_forward($ENV{HTTP_REFERER});
}

sub restore_post_or_thread($$;$)
{
	my ($num,$board_name,$recursive_instance) = @_;

	my $sth;              		# Database handler.
	my $updated_lasthit = 0;  	# Update for the lasthit field when recovering replies to an OP.
	my $stickied_thread = 0;  	# Update for stickied field based on whether thread is stickied.
	my $this_board;	      		# Object for affected board.
	my $locked_thread;    		# Update for locked field based on whether thread is locked.


	# Resolve board name and set to current board
	if ($board_name ne $board->path())
	{
		$this_board=Board->new($board_name) or make_error("Failed to resolve board $board_name.");
	}
	else
	{
		$this_board=$board;
	}
	
	# Grab post contents
	$sth=$dbh->prepare("SELECT * FROM `".SQL_BACKUP_TABLE."` WHERE board_name=? AND postnum=?;") or make_error(S_SQLFAIL);
	$sth->execute($board_name,$num) or make_error(S_SQLFAIL);
	my $row = $sth->fetchrow_hashref();
	$sth->finish();

	my @erray;

	if(!$row)
	{
		push @erray,"Post restoration failed: Backup of post in $board_name with original ID $num not found.";
	}

	# make_error("Post restoration failed: Backup of post in $board_name with original ID $num not found.") if (!$row);

	# Delete original post, if applicable.
	$sth=$dbh->prepare("DELETE FROM `".SQL_TABLE."` WHERE num=? AND section=?;") or make_error(S_SQLFAIL);
	$sth->execute($num,$this_board->path()) or make_error(S_SQLFAIL);
	$sth->finish();

	# Check if post belongs to thread and thread is deleted. We cannot bring back a single post from a neutered thread.
	if ($$row{parent})
	{
		my $parent_row = get_post($$row{parent});

		($updated_lasthit, $stickied_thread, $locked_thread) = ($$parent_row{lasthit}, $$parent_row{sticky}, $$parent_row{locked});

		$sth->finish();

		if (!$updated_lasthit)
		{
			push @erray,"Post restoration failed: Post $board_name,$num is a member of a deleted thread.";
		}
		
		# ...Otherwise, use the updated lasthit field, as it must be in common with the rest of the thread for proper display.
		# Also match the stickied field.
	}

	if(@erray)
	{
		my $errstr = join("<br />", @erray);
		make_error("$errstr");
	}
	
	# Restore post.
	$sth=$dbh->prepare("INSERT INTO `".SQL_TABLE."` VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);") or make_error(S_SQLFAIL);
	$sth->execute(
			$$row{postnum},$$row{parent},$$row{timestamp},$updated_lasthit || time(),$$row{ip},
			$$row{id},$$row{name},$$row{trip},$$row{email},$$row{subject},
			$$row{password},$$row{comment},$$row{image},$$row{size},$$row{md5},
			$$row{width},$$row{height},$$row{thumbnail},$$row{tn_width},$$row{tn_height},
			$$row{origname},$$row{meta},$$row{adminpost},$$row{admin_post},$stickied_thread,$locked_thread,$$row{autosage},
			$$row{location},$$row{aluhut},$this_board->path()
		) or make_error(S_SQLFAIL);
	$sth->finish();
	
	# Delete backup.
	$sth=$dbh->prepare("DELETE FROM `".SQL_BACKUP_TABLE."` WHERE postnum=? AND board_name=?");
	$sth->execute($$row{postnum},$board_name);
	$sth->finish();

	# Restore image.
	if ( $$row{image} )
	{
		my $base_filename= $$row{image};
		$base_filename=~s!^.*[\\/]!!; # cut off any directory in filename
		rename ($this_board->path().'/'.$this_board->option('ARCHIVE_DIR').$this_board->option('BACKUP_DIR').$base_filename,
			  $this_board->path().'/'.$$row{image}) and chmod 0644, $this_board->path().'/'.$$row{image}
			if ( -e $this_board->path().'/'.$this_board->option('ARCHIVE_DIR').$this_board->option('BACKUP_DIR').$base_filename );

			# Move thumbnail, if applicable.
		my $thumb_dir = $this_board->option('THUMB_DIR');
		if ($$row{thumbnail} =~ /^$thumb_dir/)
				{
						$base_filename = $$row{thumbnail};
					$base_filename =~ s!^.*[\\/]!!; # cut off any directory in filename
						rename ($this_board->path().'/'.$this_board->option('ARCHIVE_DIR').$this_board->option('BACKUP_DIR').$base_filename,
				 $this_board->path().'/'.$$row{thumbnail}) and chmod 0644, $this_board->path().'/'.$$row{thumbnail}
				 if ( -e $this_board->path().'/'.$this_board->option('ARCHIVE_DIR').$this_board->option('BACKUP_DIR').$base_filename );
				}
	}

	# If thread is being restored, make sure that all posts backed up *prior* to thread backup is restored. We can base this decision on lasthit. The thread is to be restored as it was prior to deletion.
	if (!$$row{parent})
	{
		$sth=$dbh->prepare("SELECT * FROM `".SQL_BACKUP_TABLE."` WHERE num>? AND parent=? ORDER BY num ASC;");
		$sth->execute($$row{num},$$row{postnum});
		
		# Add recursive instance code variable to prevent excessive caching/redirects.
		while (my $res=$sth->fetchrow_hashref())
		{
			restore_post_or_thread($$res{postnum}, $board->path(), 1);
		}
		
		$sth->finish();
	}

	# Rebuild relevant caches.
	if (!$recursive_instance)
	{
		 repair_database();
	}
}

sub cleanup_backups_database()
{
	my $sth = $dbh->prepare("SELECT postnum, board_name FROM `".SQL_BACKUP_TABLE."` WHERE timestampofarchival < ?;");
	$sth->execute(time - POST_BACKUP_EXPIRE);

	while (my $expired_backup_row = $sth->fetchrow_hashref())
	{
		remove_backup( $$expired_backup_row{postnum}, $$expired_backup_row{board_name} );
	}
}

sub remove_post_from_backup($$@)
{
	my ($admin, $board_name, @id) = @_;
	check_password( $admin, '' );

	foreach my $post (@id)
	{
		remove_backup( $post, $board_name );

		# add_log_entry($username,'backup_remove',$board->path().','.$post,
		# 		make_date(time()+TIME_OFFSET,DATE_STYLE),dot_to_dec($ENV{REMOTE_ADDR}),0,time());
	}

	make_http_forward( $ENV{HTTP_REFERER} );
}

sub remove_backup($$)
{
	my ($id, $board_name) = @_;

	# Delete relevant files.
	my $sth = $dbh->prepare("SELECT image,thumbnail FROM `".SQL_BACKUP_TABLE."` WHERE postnum=? AND board_name=?;");
	$sth->execute($id, $board_name);

	my ($image_filename, $thumbnail_filename) = ($sth->fetchrow_array());
	my $image_filename =~ s!^.*[\\/]!!;
	my $thumbnail_filename =~ s!^.*[\\/]!!;

	if ( my $this_board = Board->new($board_name) )
	{
		unlink $this_board.'/'.$this_board->option('ARCHIVE_DIR').$this_board->option('BACKUP_DIR').$image_filename if $image_filename;
		unlink $this_board.'/'.$this_board->option('ARCHIVE_DIR').$this_board->option('BACKUP_DIR').$thumbnail_filename if $thumbnail_filename;
	}

	$sth->finish();

	# Delete post data.
	$sth = $dbh->prepare("DELETE FROM `".SQL_BACKUP_TABLE."` WHERE postnum=? AND board_name=?;");
	$sth->execute($id,$board_name);
}


#
# Admin interface
#

sub get_abbrev_message {
    my ($lines) = @_;
    return S_ABBRTEXT1 if ($lines == 1);
    return sprintf(S_ABBRTEXT2, $lines);
}

sub resolve_reflinks($) {
	my ($comment) = @_;

	$comment =~ s|<!--reflink-->&gt;&gt;([0-9]+)|
		my $res = get_post($1);
		if ($res) { '<a href="'.get_reply_link1($$res{num},$$res{parent}).'" onclick="highlight('.$1.')">&gt;&gt;'.$1.'</a>' }
		else { '<del>&gt;&gt;'.$1.'</del>'; }
	|ge;

	return $comment;
}

sub make_admin_login(;$)
{
	my ($login_task) = @_;

	make_http_header();
	print encode_string(ADMIN_LOGIN_TEMPLATE->(login_task=>$login_task,board=>$board,stylesheets=>get_stylesheets()));
}

sub make_admin_ban_panel($)
{
	my ($admin)=@_;
	my ($sth,$row,@bans,$prevtype);

	my ($username, $type) = check_password($admin,'');

	$sth=$dbh->prepare("SELECT * FROM `".SQL_ADMIN_TABLE."` WHERE type='ipban' OR type='wordban' OR type='whitelist' OR type='trust' ORDER BY type ASC,num ASC;") or make_error(S_SQLFAIL);
	$sth->execute() or make_error(S_SQLFAIL);
	while($row=get_decoded_hashref($sth))
	{
		$$row{divider}=1 if($prevtype ne $$row{type});
		$prevtype=$$row{type};
		$$row{rowtype}=@bans%2+1;
		push @bans,$row;
	}

	make_http_header();
	print encode_string(BAN_PANEL_TEMPLATE->(admin=>$admin,bans=>\@bans,board=>$board,username=>$username,usertype=>$type,stylesheets=>get_stylesheets()));
}

sub make_edit_post_panel($$;$)
{
	my ($admin,$num,$noformat)=@_;
	my ($mod, $no_format);

	my ($username, $type) = check_password($admin,'');

	my $post=get_post($num) or make_error(sprintf "Not exist post %d",$num);
 
	make_http_header();
	print encode_string(EDIT_POST_PANEL_TEMPLATE->(
		admin=>$admin,
		num=>$num,
		parent=>$$post{parent},
		name=>decode_string($$post{name},CHARSET,1),
		email=>decode_string($$post{email},CHARSET,1),
		subject=>decode_string($$post{subject},CHARSET,1),
		comment=>decode_string($$post{comment},CHARSET,1),
		stylesheets=>get_stylesheets(),
		mod=>$$post{admin_post},
		board=>$board,
		usertype=>$type,
		username=>$username,
		noformat=>$noformat,
	));
}

sub make_admin_orphans {
    my ($admin) = @_;
    my ($sth, $row, @results, @dbfiles, @dbthumbs);

	my ($username, $type) = check_password($admin, '');

    my $img_dir = $board->path() . '/' . $board->option('IMG_DIR');
    my $thumb_dir = $board->path() . '/' . $board->option('THUMB_DIR');

    # gather all files/thumbs on disk
    my @files = glob $img_dir . '*';
    my @thumbs = glob $thumb_dir . '*';

    my $board_path = $board->path(); # Clear out the board path name.
    $_ =~ s/^${board_path}\/// for @files;
    $_ =~ s/^${board_path}\/// for @thumbs;

    # gather all files/thumbs from database
    $sth = $dbh->prepare("SELECT image, thumbnail FROM " . SQL_TABLE . " WHERE size > 0 ORDER by num ASC;")
      or make_sql_error();
    $sth->execute() or make_sql_error();

    while ($row = get_decoded_arrayref($sth)) {
        push(@dbfiles, $$row[0]);
        push(@dbthumbs, $$row[1]) if $$row[1];
    }
    $sth->finish();

    # copy all entries from the disk arrays that are not found in the database arrays to new arrays
    my %dbfiles_hash = map { $_ => 1 } @dbfiles;
    my %dbthumbs_hash = map { $_ => 1 } @dbthumbs;
    my @orph_files = grep { !$dbfiles_hash{$_} } @files;
    my @orph_thumbs = grep { !$dbthumbs_hash{$_} } @thumbs;

    my $file_count = @orph_files;
    my $thumb_count = @orph_thumbs;
    my @f_orph;
    my @t_orph;

    foreach my $file (@orph_files) {
        my @result = stat($board->path().'/'.$file);
        my $entry = {};
        $$entry{rowtype} = @f_orph % 2 + 1;
        $$entry{name} = $file;
        $$entry{modified} = $result[9];
        $$entry{size} = $result[7];
        push(@f_orph, $entry);
    }

    foreach my $thumb (@orph_thumbs) {
        my @result = stat($board->path().'/'.$thumb);
        my $entry = {};
        $$entry{name} = $thumb;
        $$entry{modified} = $result[9];
        $$entry{size} = $result[7];
        push(@t_orph, $entry);
    }

    make_http_header();
    print encode_string(ORPHANS_PANEL_TEMPLATE->(
        admin => $admin,
        board => $board,
        username => $username,
        usertype => $type,
        files => \@f_orph,
        thumbs => \@t_orph,
        file_count => $file_count,
        thumb_count => $thumb_count,
        stylesheets => get_stylesheets(),
    ));
}

sub move_files {
    my ($admin, @files) = @_;

    my @session = check_password($admin,'');

    my $orph_dir = $board->path() . '/' . $board->option('ARCHIVE_DIR') . $board->option('ORPH_DIR');
    my @errors;

    foreach my $file (@files) {
        $file = clean_string($file);
        if ($file =~ m!^[a-zA-Z0-9]+/[a-zA-Z0-9-]+\.[a-zA-Z0-9]+$!) {
            rename($board->path().'/'.$file, $orph_dir . $file)
              or push(@errors, S_NOTWRITE . ' (' . decode_string($orph_dir . $file, CHARSET) . ')');
        }
    }
    unless (@errors) {
        make_http_forward(get_script_name() . "?task=orphans&board=" . $board->path());
    }
    else{
        make_error(join("<br />", @errors));
    }
}


sub do_login($$$$$)
{
	my ($username,$password,$nexttask,$savelogin,$admincookie)=@_;
	my $crypt;
	my @adminarray = split (/,/, $admincookie) if $admincookie;
	
	my $sth=$dbh->prepare("SELECT pass, class, username FROM `".SQL_MODER_TABLE."` WHERE username=?;") or make_error(S_SQLFAIL);
	$sth->execute(($username || !$admincookie) ? $username : $adminarray[0]) or make_error(S_SQLFAIL);
	my $row=$sth->fetchrow_hashref();
	$sth->finish();
	
	if ($username && $username eq $$row{username}) # We must check the username field to ensure case-sensitivity
	{
		$crypt = $username.','.crypt_password($$row{pass}) if ($row && hide_critical_data($password,SECRET) eq $$row{pass} && !$$row{disabled});
		$nexttask||="mpanel";
	}
	elsif($admincookie && $adminarray[0] eq $$row{username})
	{
		$crypt=$admincookie if ($row && $adminarray[1] eq crypt_password($$row{pass}));
		$nexttask||="mpanel";
	}
	
	if($crypt)
	{
		# remove_password_prompt_session_from_database('admin');

		# Cookie containing encrypted login info
		make_cookies(wakaadmin=>$crypt,
		-charset=>CHARSET,-autopath=>$board->option('COOKIE_PATH'),-expires=>(($savelogin) ? time+365*24*3600 : time+1800));
		
		# Cookie signaling to script that the cookie is long-term.
		make_cookies(wakaadminsave=>1,
		-charset=>CHARSET,-autopath=>$board->option('COOKIE_PATH'),-expires=> time+365*24*3600) if ($savelogin && $query->cookie("wakaadmin"));
		
		make_http_forward(get_secure_script_name()."?task=$nexttask&board=".$board->path());
	}
	else 
	{ 
		make_admin_login($nexttask);
	}
}

sub do_logout()
{
	make_cookies(wakaadmin=>"",-expires=>1);
	make_cookies(wakaadminsave=>"",-expires=>1);
	make_http_forward(get_secure_script_name()."?task=admin&board=".$board->path());
}

sub add_admin_entry($$$$$$;$)
{
	my ($admin,$type,$comment,$ival1,$ival2,$sval1,$from)=@_;
	my ($sth,$time);
	$time = time();

	# ......
	($ival1,$ival2) = parse_range(2130706433,'') unless $ival1;  

	check_password($admin,'');

	$comment=clean_string(decode_string($comment,CHARSET));

	check_admin_entry($ival1) if($ival1);

	my $num = 0;
	eval {
		$dbh->begin_work();

		$sth=$dbh->prepare("UPDATE `".SQL_COUNTERS_TABLE."` SET counter = counter + 1 WHERE section = ?;");
		$sth->execute('admin');

		$sth=$dbh->prepare("SELECT counter FROM `".SQL_COUNTERS_TABLE."` WHERE section = ?;");
		$sth->execute('admin');
		$num=($sth->fetchrow_array())[0];

		$sth=$dbh->prepare("INSERT INTO `".SQL_ADMIN_TABLE."` VALUES(?,?,?,?,?,?,?);");
		$sth->execute($num,$type,$comment,$ival1,$ival2,$sval1,$time);
		$sth->finish();

		$dbh->commit();
	};
	if ($@) {
		eval { $dbh->rollback(); };
		make_error(S_SQLFAIL);
	}

	make_http_forward(get_secure_script_name()."?task=bans&board=".$board->path()) unless $from;
}

sub banpost($$$)
{
	my ($admin,$num,$reason)=@_;
	my ($sth,$sth2,$row,$ip,$oldcomment,$newcomment,$threadid);
	my ($blame) = S_BANNED;

	check_password($admin,'');

	$sth=$dbh->prepare("SELECT ip FROM `".SQL_TABLE."` WHERE num=? AND section=?") or make_error(S_SQLFAIL);
	$sth->execute($num,$board->path) or make_error(S_SQLFAIL);
	$ip=($sth->fetchrow_array())[0];
	$sth->finish();

	$reason=clean_string($reason);

	my @range = parse_range($ip,'');
	add_admin_entry($admin,'ipban',($reason) ? $reason : "no reason",@range[0],@range[1],'',1);
	make_http_forward(get_secure_script_name()."?task=mpanel&board=".$board->path());
}


sub check_admin_entry {
	my ($ival1) = @_;
	my ($sth, $results);

	if (!$ival1) {
		make_error("invalid parameter");
	}
	else {
		$sth = $dbh->prepare("SELECT COUNT(*) AS count FROM `".SQL_ADMIN_TABLE."` WHERE ival1=?;");
		$sth->execute($ival1);
		$results = get_decoded_hashref($sth);

		make_error("Duplicate ip") if($$results{count});
		$sth->finish();
	}
}

sub remove_admin_entry($$)
{
	my ($admin,$num)=@_;
	my ($sth);

	check_password($admin,'');

	$sth=$dbh->prepare("DELETE FROM `".SQL_ADMIN_TABLE."` WHERE num=?;") or make_error(S_SQLFAIL);
	$sth->execute($num) or make_error(S_SQLFAIL);

	make_http_forward(get_secure_script_name()."?task=bans&board=".$board->path());
}

sub delete_all($$$)
{
	my ($admin,$unparsedip,$unparsedmask,$caller)=@_;
	my ($sth,$row,@posts);

	make_error("Missing IP address.") if (!$unparsedip);
	my ($ip, $mask) = parse_range($unparsedip,$unparsedmask);

	check_password($admin,'');

	# Issue SQL query to extract posts
	$sth=$dbh->prepare("SELECT num FROM `".SQL_TABLE."` WHERE section=? AND (ip & ? = ? & ?);") or make_error(S_SQLFAIL);
	$sth->execute($board->path(),$mask,$ip,$mask) or make_error(S_SQLFAIL);
	while($row=$sth->fetchrow_hashref()) { push(@posts,$$row{num}); }
	$sth->finish();

	delete_stuff('',0,0,0,0,$admin,1,'internal_delete_all()',@posts);

	make_http_forward($ENV{HTTP_REFERER}) unless $caller eq 'internal';
}

#
# Editing
#

sub tag_killa { # subroutine for stripping HTML tags and supplanting them with corresponding wakabamark
	my $tag_killa = $_[0];
	study $tag_killa; # Prepare string for some extensive regexp.

	$tag_killa =~ s/<p(?: class="oekinfo">|>\s*<small>)\s*<strong>(?:Oekaki post|Edited in Oekaki)<\/strong>\s*\(Time\:.*?<\/p>//i; # Strip Oekaki postfix.
	$tag_killa =~ s/<br\s?\/?>/\n/g;
	$tag_killa =~ s/<\/p>$//;
	$tag_killa =~ s/<\/p>/\n\n/g;
	$tag_killa =~ s/<\/blockquote>$//;
	$tag_killa =~ s/<\/blockquote>/\n\n/g;
	while ($tag_killa =~ m/<ul>(.*?)<\/ul>/s)
	{
		my $replace = $1 || $2;
		my $replace2 = $replace;
		$replace2 =~ s/<li>/\* /g;
		$replace2 =~ s/<\/li>/\n/g;
		$tag_killa =~ s/<ul>.*?<\/ul>/${replace2}\n/s;
	}
	while ($tag_killa =~ m/<ol>(.*?)<\/ol>/s)
	{
		my $replace = $1;
		my $replace2 = $replace;
		my @strings = split (/<\/li>/, $replace2);
		my @new_strings;
		my $count = 1;
		foreach my $entry (@strings)
		{
			$entry =~ s/<li>/$count\. /;
			push (@new_strings, $entry);
			++$count;
		}
		$replace2 = join ("\n", @new_strings);
		$tag_killa =~ s/<ol>.*?<\/ol>/${replace2}\n\n/s;
	}   


	# bbcode, etc
	$tag_killa =~ s/<span class="spoiler">(.*?)<\/span>/\[spoiler\]$1\[\/spoiler\]/g;
	$tag_killa =~ s/<em>(.*?)<\/em>/\[i\]$1\[\/i\]/g;
	$tag_killa =~ s/<u>(.*?)<\/u>/\[u\]$1\[\/u\]/g;
	$tag_killa =~ s/<strong>(.*?)<\/strong>/\[b\]$1\[\/b\]/g;
	$tag_killa =~ s/<sup>(.*?)<\/sup>/\[sup\]$1\[\/sup\]/g;
	$tag_killa =~ s/<sub>(.*?)<\/sub>/\[sub\]$1\[\/sub\]/g;
	$tag_killa =~ s/<small>(.*?)<\/small>/\[small\]$1\[\/small\]/g;
	$tag_killa =~ s/<span class="red">(.*?)<\/span>/\[notred\]$1\[\/notred\]/g;

	$tag_killa =~ s/<pre><code>([^\n]*?)<\/code><\/pre>/<code>$1<\/code>/g;
	$tag_killa =~ s/<code>/\[code\]/g;      # worst shit
	$tag_killa =~ s/<\/code>/\[\/code\]/g;  # worst shit

	$tag_killa =~ s/<.*?>//g;

	$tag_killa;
}

sub edit_post($$$$$$;$$)
{
	my ($admin,$num,$name,$email,$subject,$comment,$capcode,$no_format)=@_;
	my ($sth,$ip,$time);
	$ip = get_remote_addr();
	$time = time();

	check_password($admin,'');

	my $post=get_post($num) or make_error(sprintf "Not exist post %d",$num);

	make_error(S_UNUSUAL) if($name=~/[\n\r]/);
	make_error(S_UNUSUAL) if($email=~/[\n\r]/);
	make_error(S_UNUSUAL) if($subject=~/[\n\r]/);
	make_error(S_TOOLONG) if(length($name)>($board->option('MAX_FIELD_LENGTH')));
	make_error(S_TOOLONG) if(length($email)>($board->option('MAX_FIELD_LENGTH')));
	make_error(S_TOOLONG) if(length($subject)>($board->option('MAX_FIELD_LENGTH')));
	make_error(S_TOOLONG) if(length($comment)>($board->option('MAX_COMMENT_LENGTH')));

	# clean inputs
	$name=clean_string(decode_string($name,CHARSET));
	$email=clean_string(decode_string($email,CHARSET));
	$subject=clean_string(decode_string($subject,CHARSET));


	if($email=~/sage/i) { $email='sage'; }
	else { $email=''; }

	# fix names
	if($board->option('FORCED_ANON') && $name) # a nehooy
	{
		$name = $$post{name};
		# $trip=$$post{trip};
	}

	$name = $$post{name} unless $name;
	$name = $board->option('S_SAGENAME') if($email && $board->option('REPLACE_SAGE_NAMES'));

	# make admin name
	$capcode = 1 if($admin && $capcode);

	# fix up the email/link
	$email = "mailto:$email" if $email and $email !~ /^$protocol_re:/;

	# fix comment
	my $postfix=''; #tag_killa($$post{comment});
	$comment=format_comment(clean_string(decode_string($comment,CHARSET))) unless $no_format;
	$comment.=$postfix;

	# finally, update
	$sth=$dbh->prepare("UPDATE `".SQL_TABLE."` SET name=?,email=?,subject=?,comment=?,admin_post=? WHERE num=? AND section=?;") or make_error(S_SQLFAIL);
	$sth->execute($name,$email,$subject,$comment,$capcode,$num,$board->path()) or make_error(S_SQLFAIL);

	# Go to thread
	if($$post{parent}) { make_http_forward($board->path().'/'.$board->option('RES_DIR').$$post{parent}.$page_ext.($num?"#$num":"")); }
	elsif($num)	{ make_http_forward($board->path().'/'.$board->option('RES_DIR').$num.$page_ext); }
	else { make_http_forward($board->path().'/'.$board->option('HTML_SELF')); } # shouldn't happen
}

sub check_password($$;$)
{
	my ($admin,$task_redirect,$editing)=@_;
	my $ip = get_remote_addr();
	my $time = time();

	my @adminarray = split (/,/, $admin); # <user>,rc6(<password+hostname>)
	
	my $sth=$dbh->prepare("SELECT pass, username, class, disabled, boards FROM `".SQL_MODER_TABLE."` WHERE username=?;") or make_error(S_SQLFAIL);
	$sth->execute($adminarray[0]) or make_error(S_SQLFAIL);

	my $row=$sth->fetchrow_hashref();

	# Access check
	my $path = $board->path(); # lol
	make_error("Sorry, you do not have access rights to this board.<br />(Accessible: ".$$row{boards}.")<br /><a href=\"".get_script_name()."?task=logout&amp;board=".$board->path()."\">Logout</a>") if ($$row{class} eq 'mod' && $$row{boards} !~ /\b$path\b/); 
	make_error("This account is disabled.") if ($$row{disabled});
	if ($$row{username} ne $adminarray[0] || !$adminarray[0]) # This is necessary, in fact, to ensure case-sensitivity for the username
	{
		make_error(S_WRONGPASS,$editing);
	}
	my $encrypted_pass = crypt_password($$row{pass});
	$adminarray[1] =~ s/ /+/g; # Undoing encoding done in cookies. (+ is part of the base64 set)
	
	if ($adminarray[1] eq $encrypted_pass && !$$row{disabled}) # Return username,type if correct
	{
		make_cookies(wakaadmin=>$admin,
		-charset=>CHARSET,-autopath=>$board->option('COOKIE_PATH'),-expires=>time+1800) if (!($query->cookie('wakaadminsave')));

		my $upd=$dbh->prepare("UPDATE `".SQL_MODER_TABLE."` SET lasthit=?,lastip=? WHERE username=?;");
		$upd->execute($time,dot_to_dec($ip),$$row{username});
		$upd->finish();

		my $account = $$row{class};
		$sth->finish();

		return ($adminarray[0],$account);
	}
	
	$sth->finish();
	
	make_error(S_WRONGPASS); # Otherwise, throw an error.
}

sub crypt_password($)
{
	my $crypt=hide_data((shift).get_remote_addr(),9,"admin",SECRET,1);
	$crypt=~tr/+/./; # for web shit
	return $crypt;
}

#
# Boards
#

sub get_boards_list()
{
	my @boards = get_boards();
	my $last = @boards[(scalar @boards)-1];
	$$last{last} = 1;
	return \@boards;
}

sub get_reign($)
{
	my $username=$_[0];
	my @return_AoH; # Array of Hashes whose reference to return for <loop>ing in futaba_style.pl
	
	my $sth=$dbh->prepare("SELECT reign FROM `".SQL_MODER_TABLE."` WHERE username=?;") or make_error(S_SQLFAIL);
	$sth->execute($username) or make_error(S_SQLFAIL);
	my $reign_string = ($sth->fetchrow_array)[0];
	$sth->finish();
	
	if ($reign_string)
	{
		my @boards = split (/ /, $reign_string);
		
		foreach my $board_entry (@boards)
		{
			my $row = {board_entry=>$board_entry};
			push  @return_AoH, $row;
		}
	}
	else
	{
		@return_AoH = get_boards();
	}
	
	return \@return_AoH;
}

sub get_boards() # Get array of referenced hashes of all boards
{
	my @boards; # Board list
	my $board_is_present = 0; # Is the current board present?
	
	my $sth = $dbh->prepare(
		"SELECT board AS board_entry, name AS board_name, type AS category, hidden AS is_hidden FROM `"
		.SQL_COMMON_SITE_TABLE."` ORDER BY board;"
		) or make_error(S_SQLFAIL);
	$sth->execute() or make_error(S_SQLFAIL);
	
	while (my $row=get_decoded_hashref($sth))
	{
		push @boards,$row;
		$board_is_present = 1 if $$row{board_entry} eq $board->path();
	}
	
	$sth->finish();
	
	unless ($board_is_present)
	{
		add_board_to_index();
		my $row = {board_entry=>$board->path()};
		push @boards, $row;
	}
	
	@boards;
}

sub add_board_to_index()
{
	my $brdname = $board->option('BOARD_NAME');
	my($is_hidden,$board_path,@hiddens);

	@hiddens = HIDDEN_BOARDS;
	$board_path = $board->path();
	if ( grep( /^$board_path$/, @hiddens ) ) {
	  $is_hidden = 1;
	} else {
	  $is_hidden = 0;
	}

	my $fix = $dbh->prepare("INSERT INTO `".SQL_COMMON_SITE_TABLE."` VALUES(?,?,?,?);") or make_error(S_SQLFAIL);
	$fix->execute($board->path(),"",$brdname,$is_hidden) or return 0;
	$fix->finish();
}

sub remove_board_from_index()
{
	my $fix = $dbh->prepare("DELETE FROM `".SQL_COMMON_SITE_TABLE."` WHERE board = ?;") or make_error(S_SQLFAIL);
	$fix->execute($board->path()) or return 0;
	$fix->finish();
}

#
# Moders
#

sub make_admin_moder_panel($)
{
	my ($admin) = @_;
	my ($sth,$row,@mods,$prevtype);

	my ($username, $type) = check_password($admin,'');
	make_error(S_INSUFF) if ($type ne 'admin');

	$sth = $dbh->prepare("SELECT * FROM `".SQL_MODER_TABLE."` ORDER BY timestamp ASC;") or make_error(S_SQLFAIL);
	$sth->execute() or make_error(S_SQLFAIL);
	while ($row = get_decoded_hashref($sth)) {
		$$row{divider} = 1 if ($prevtype ne $$row{type});
		$prevtype      = $$row{type};
		$$row{rowtype} = @mods % 2 + 1;
		push @mods, $row;
	}
	$sth->finish();

	my @boards = get_boards();

	make_http_header();
	print encode_string(MODER_PANEL_TEMPLATE->(admin => $admin, mods => \@mods, board => $board, boards => \@boards, usertype => $type, username => $username, stylesheets => get_stylesheets() ));
}

sub add_initial_moder {
	my ($user,$pass,$type,$mana_pass);
	my ($sth);
	my $time=time();
	$mana_pass=(ADMIN_PASS);
	($user, $pass, $type) = ("admin", "admin", "admin");

	my $encrypted_password = hide_critical_data($pass, SECRET);

	my $num = 0;
	eval {
		$dbh->begin_work();

		$sth=$dbh->prepare("UPDATE `".SQL_COUNTERS_TABLE."` SET counter = counter + 1 WHERE section = ?;");
		$sth->execute('moders');

		$sth=$dbh->prepare("SELECT counter FROM `".SQL_COUNTERS_TABLE."` WHERE section = ?;");
		$sth->execute('moders');
		$num=($sth->fetchrow_array())[0];
		$sth->finish();

		insert_user_account_entry($num,$user,$encrypted_password,'',$type,$time,0);

		$dbh->commit();
	};
	if ($@) {
		eval { $dbh->rollback(); };
		make_error(S_SQLFAIL);
	}
}

sub add_moder {
	my ($admin,$user_to_create,$password,$account_type,$management_password,@boards)=@_;
	my ($time);

	my ($username, $type) = check_password($admin, '');

	# Sanity checks
	make_error(S_INSUFF) if ($type ne 'admin');
	make_error("A username is necessary.") if (!$user_to_create);
	make_error("A password is necessary.") if (!$password);
	make_error("Invalid Class") if($account_type && $account_type !~m/(admin|mod|globmod)/);
	make_error("Please input only Latin letters (a-z), numbers (0-9), spaces, and some punctuation marks (_,^,.) for the password.") if ($password !~ /^[\w\^\.]+$/);
	make_error("Please input only Latin letters (a-z), numbers (0-9), spaces, and some punctuation marks (_,^,.) for the username.") if ($user_to_create !~ /^[\w\^\.\s]+$/);
	make_error("Please limit the username to thirty characters maximum.") if (length $user_to_create > 30);
	make_error("Please have a username of at least four characters.") if (length $user_to_create < 4);
	make_error("Please limit the password to thirty characters maximum.") if (length $password > 30);
	make_error("Passwords should be at least eight characters!") if (length $password < 8);
	make_error("No boards specified for local moderator.") if (!@boards && $account_type eq 'mod');

	my $ip = get_remote_addr();
	$time=time();

	my $sth=$dbh->prepare("SELECT * FROM `".SQL_MODER_TABLE."` WHERE username=?;") or make_error(S_SQLFAIL);
	$sth->execute($user_to_create) or make_error(S_SQLFAIL);
	my $row = $sth->fetchrow_hashref();
	
	make_error("Username exists.") if ($row);
	make_error("Password for management incorrect.") if ($account_type eq 'admin' && $management_password ne ADMIN_PASS);
	
	my $reignstring = '';
	if ($account_type eq 'mod') # Handle list of boards under jurisdiction if user is to be a local moderator.
	{
		$reignstring = join (" ", @boards);
	}

	$sth->finish();
	
	my $encrypted_password = hide_critical_data($password, SECRET);

	my $num = 0;
	eval {
		$dbh->begin_work();

		$sth=$dbh->prepare("UPDATE `".SQL_COUNTERS_TABLE."` SET counter = counter + 1 WHERE section = ?;");
		$sth->execute('moders');

		$sth=$dbh->prepare("SELECT counter FROM `".SQL_COUNTERS_TABLE."` WHERE section = ?;");
		$sth->execute('moders');
		$num=($sth->fetchrow_array())[0];
		$sth->finish();

		insert_user_account_entry($num,$user_to_create,$encrypted_password,$reignstring,$account_type,$time,dot_to_dec($ip));

		$dbh->commit();
	};
	if ($@) {
		eval { $dbh->rollback(); };
		make_error(S_SQLFAIL);
	}
	
	make_http_forward(get_secure_script_name()."?task=moders&board=".$board->path());
}

sub insert_user_account_entry($$$$$$)
{
	my ($num,$username,$encrypted_password,$reignstring,$type,$time,$ip) = @_;
	my $sth=$dbh->prepare("INSERT INTO `".SQL_MODER_TABLE."` VALUES(?,?,?,?,?,?,?,?,?);") or make_error(S_SQLFAIL);
	$sth->execute($num,$username,$encrypted_password,$type,$reignstring,$time,$ip,$time,0) or make_error(S_SQLFAIL);
	$sth->finish();
}

sub remove_moder($$$)
{
	my ($admin,$user_to_delete,$admin_pass)=@_;

	my ($username, $type) = check_password($admin, '');
	make_error(S_INSUFF) if ($type ne 'admin');
	make_error("No username specified.") if (!$user_to_delete); 
	make_error("An Hero Mode not available.") if ($user_to_delete eq $username);
	
	my $sth=$dbh->prepare("SELECT class FROM `".SQL_MODER_TABLE."` WHERE username=?;") or make_error(S_SQLFAIL);
	$sth->execute($user_to_delete) or make_error(S_SQLFAIL);
	
	my $row = $sth->fetchrow_hashref();
	
	make_error("Management password incorrect.") if ($$row{class} eq 'admin' && $admin_pass ne ADMIN_PASS);

	$sth->finish();

	my $deletion=$dbh->prepare("DELETE FROM `".SQL_MODER_TABLE."` WHERE username=?;") or make_error(S_SQLFAIL);
	$deletion->execute($user_to_delete) or make_error(S_SQLFAIL);
	$deletion->finish();

	make_http_forward(get_secure_script_name()."?task=moders&board=".$board->path());
}

sub edit_moder($$)
{
	my ($admin,$user_to_edit)=@_;

	my ($username, $type) = check_password($admin, '');
	# make_error(S_INSUFF) if ($type ne 'admin');

	my $sth=$dbh->prepare("SELECT * FROM `".SQL_MODER_TABLE."` WHERE username=?;") or make_error(S_SQLFAIL);
	$sth->execute($user_to_edit) or make_error(S_SQLFAIL);
	my $dengus=get_decoded_hashref($sth);
	$sth->finish();

	my @boards = get_boards();
	my @reign = sort (split (/ /, $$dengus{boards})); # Sort the list of boards so we can do quicker trickery with shift() 
	
	while (@reign)
	{
		my $board_under_power = shift (@reign);
		foreach my $row (@boards)
		{
			if ($$row{board_entry} eq $board_under_power)
			{
				$$row{underpower} = 1; # Mark as ruled with an iron fist.
				last; 		       # ...And go to the next entry of reign (containing loop).
			}
		}
	}

	make_http_header();
	print encode_string(EDIT_USER_TEMPLATE->(admin=>$admin,userinfo=>$dengus,num=>$$dengus{num},board=>$board,username=>$username,usertype=>$type,boards=>\@boards,stylesheets=>get_stylesheets()));
}

sub update_moder_details {
	my ($admin,$user_to_edit,$newpassword,$newclass,$origpass,$disable,@boards)=@_;
	my ($sth,$update,$row);
	my ($username, $type) = check_password($admin, '');

	# Sanity check
	make_error(S_INSUFF) if ($user_to_edit ne $username && $type ne 'admin');
	make_error("No user specified.") if (!$user_to_edit);
	make_error("Please input only Latin letters, numbers, and underscores for the password.") if ($newpassword && $newpassword !~ /^[\w\d_]+$/);
	make_error("Please limit the password to thirty characters maximum.") if ($newpassword && length $newpassword > 30);
	make_error("Passwords should be at least eight characters!") if ($newpassword && length $newpassword < 8);
	make_error("Invalid Class") if($newclass && $newclass !~m/(admin|mod|globmod)/);

	$sth=$dbh->prepare("SELECT * FROM `".SQL_MODER_TABLE."` WHERE username=?;") or make_error(S_SQLFAIL);
	$sth->execute($user_to_edit) or make_error(S_SQLFAIL);

	$row = $sth->fetchrow_hashref();

	make_error("Cannot alter your own account class.") if ($newclass && $user_to_edit eq $username && $newclass ne $$row{class});
	make_error("Cannot change your own reign.") if (@boards && join (" ", @boards) ne $$row{boards} && $user_to_edit eq $username);
	make_error("Disable yourself?") if($user_to_edit eq $username && $disable);

	# Users can change their own password, but not others' if they are without administrative rights.
	make_error("Password incorrect.") if ($user_to_edit eq $username && hide_critical_data($origpass,SECRET) ne $$row{pass});

	# Clear out unneeded changes
	$newclass = '' if ($newclass eq $$row{class});
	@boards = split (/ /, $$row{boards}) if (!@boards);
	@boards = () if ($newclass && $newclass ne 'mod');

	if(!$newclass)
	{
		$newclass = $$row{class};
	}

	if($newpassword) { $newpassword=hide_critical_data($newpassword,SECRET); }
	else { $newpassword=$$row{pass}; }

	my $disabled = $disable ? 1 : 0;

	# wtf
	# my $reign=join(' ', @boards );

	my $reignstring = '';
	if ($$row{class} eq 'mod') # If user was a moderator (whether user still is or is being promoted), then update reign string 
	{
		$reignstring = join (" ", @boards);
	}

	$update=$dbh->prepare("UPDATE `".SQL_MODER_TABLE."` SET class=?,pass=?,boards=?,disabled=? WHERE username=?") or make_error(S_SQLFAIL);
	$update->execute($newclass,$newpassword,$reignstring,$disabled,$user_to_edit) or make_error(S_SQLFAIL);
	$update->finish();
	
	$sth->finish();

	make_http_forward(get_secure_script_name()."?task=admin&board=".$board->path()) if($username eq $user_to_edit);
	make_http_forward(get_secure_script_name()."?task=moders&board=".$board->path()) if($username ne $user_to_edit);
}


#
# Page creation utils
#

sub make_rss_header()
{
	print "Content-Type: application/rss+xml; charset=".(CHARSET)."\n";
	print "\n";
}

sub make_json_header() {
	print "Cache-Control: no-cache, no-store, must-revalidate\n";
	print "Expires: Mon, 12 Apr 1997 05:00:00 GMT\n";
	print "Content-Type: application/json; charset=utf-8\n";
	print "Access-Control-Allow-Origin: *\n";
	print "\n";
}

sub make_http_header()
{
	print "Content-Type: ".get_xhtml_content_type(CHARSET,0)."\n";
	print "\n";
}

sub log_issue($)
{
	if(ERRORLOG) # could print even more data, really.
	{
		if (ERRORLOG eq 'stderr')
		{
			warn "Wakaba: ".$ENV{HTTP_USER_AGENT}.", ".get_remote_addr().", ".$board->path().": ".shift(@_)."\n";
		}
		else
		{
			open ERRORFILE,'>>'.ERRORLOG;
			print ERRORFILE "Wakaba: ".$ENV{HTTP_USER_AGENT}.", ".get_remote_addr().", ".$board->path().": ".shift(@_)."\n";
			close ERRORFILE;
		}
	}
}


sub make_error($;$)
{
	my ($error,$not_found)=@_;

	if($ajax_mode)
	{
		make_json_header();
		print $JSON->encode({
			error_msg => $error,
			error_code => ($not_found ? 404 : 200)
		});
	}
	else
	{
		make_http_header();
		print encode_string(ERROR_TEMPLATE->(error=>$error,board=>$board,stylesheets=>get_stylesheets()));
	}

	log_issue($error);

	# delete temp files
	stop_script();
}

sub make_json_error {
	my $hax = shift;
	make_json_header();
	print $JSON->encode({
		error_msg => (defined $hax ? 'Hax0r' : 'Unknown json parameter.'),
		error_code => 500
	});
	stop_script();
}

sub make_ban {
	my ($title,@bans) = @_;
	# my ($ip,$subnet,$reason,$size,$mode) = @_;

    if ( $ajax_mode ) {
        make_json_header();
        print $JSON->encode({
            banned => 1,
            bans => \@bans,
            error_msg => $title,
            error_code => 403
        });
    }
	else {
		make_http_header();
		print encode_string(
			ERROR_TEMPLATE->(
                bans           => \@bans,
                error_page     => $title,
                error_title    => $title,
                banned         => 1,
				board 		   => $board,
				stylesheets    => get_stylesheets()
			)
		);
	}

	# delete temp files
	stop_script();
}

sub stop_script() {
	eval { next FASTCGI; };
	if ($@) {
		exit(0);
	}
}

sub get_script_name()
{
	return $ENV{SCRIPT_NAME};
}

sub get_secure_script_name()
{
	return 'https://'.$ENV{SERVER_NAME}.$ENV{SCRIPT_NAME} if(USE_SECURE_ADMIN);
	return $ENV{SCRIPT_NAME};
}

#
# Other
#

sub get_stylesheets()
{
	my $found=0;
	my @stylesheets=map
	{
		my %sheet;

		$sheet{filename}=$_;

		($sheet{title})=m!([^/]+)\.css$!i;
		$sheet{title}=ucfirst $sheet{title};
		$sheet{title}=~s/_/ /g;
		$sheet{title}=~s/ ([a-z])/ \u$1/g;
		$sheet{title}=~s/([a-z])([A-Z])/$1 $2/g;

		if($sheet{title} eq $board->option('DEFAULT_STYLE')) { $sheet{default}=1; $found=1; }
		else { $sheet{default}=0; }

		\%sheet;
	} glob(CSS_DIR."*.css");

	$stylesheets[0]{default}=1 if(@stylesheets and !$found);

	return \@stylesheets;
}

sub expand_filename($;$)
{
	my ($filename,$force_http)=@_;
	return $filename if($filename=~m!^/!);
	return $filename if($filename=~m!^\w+:!);

	my ($self_path)=$ENV{SCRIPT_NAME}=~m!^(.*/)[^/]+$!;
	$self_path = 'https://'.$ENV{SERVER_NAME}.$self_path if ($force_http);
	my $board_path=$board->path().'/';
	return $self_path.$board_path.$filename;
}

sub root_path_to_filename($)
{
	my ($filename) = @_;
	return $filename if($filename=~m!^/!);
	return $filename if($filename=~m!^\w+:!);

	my ($self_path)=$ENV{SCRIPT_NAME}=~m!^(.*/)[^/]+$!;
	return $self_path.$filename;
}

sub expand_image_filename($)
{
	my $filename=shift;

	return expand_filename(clean_path($filename)) unless $board->option('ENABLE_LOAD');

	my ($self_path)=$ENV{SCRIPT_NAME}=~m!^(.*/)[^/]+$!;
	my $src=$board->option('IMG_DIR');
	$filename=~/$src(.*)/;
	return $self_path.$board.'/'.$board->option('REDIR_DIR').clean_path($1).'.html';
}

sub get_reply_link($$;$$)
{
	my ($reply,$parent,$admin,$force_http)=@_;

	return expand_filename($board->option('RES_DIR').$parent.$page_ext,$force_http).'#'.$reply if($parent);
	return expand_filename($board->option('RES_DIR').$reply.$page_ext,$force_http);
}

sub get_reply_link1($$)
{
	my ($reply,$parent)=@_;

	return expand_filename($board->option('RES_DIR').$parent.$page_ext).'#'.$reply if($parent);
	return expand_filename($board->option('RES_DIR').$reply.$page_ext).'#'.$reply;
}

sub get_page_count(;$)
{
	my ($total) = @_;
	if ( $total > ($board->option('MAX_SHOWN_THREADS')) ) {
		$total = $board->option('MAX_SHOWN_THREADS');
	}
	return int(($total+$board->option('IMAGES_PER_PAGE')-1)/$board->option('IMAGES_PER_PAGE'));
}

sub get_page_count_real(;$)
{
	my ($total) = @_;
	return int(($total+$board->option('IMAGES_PER_PAGE')-1)/$board->option('IMAGES_PER_PAGE'));
}

sub get_filetypes_hash()
{
	my $filetypes = $board->option('FILETYPES');
	$$filetypes{gif} = $$filetypes{jpg} = $$filetypes{jpeg} = $$filetypes{png} = $$filetypes{svg} = 'image';
	$$filetypes{pdf} = 'doc';
	$$filetypes{webm} = $$filetypes{mp4} = 'video';
	return $filetypes;
}

sub get_filetypes()
{
	my $filetypes = get_filetypes_hash();
	return join ", ", map { uc } sort keys %$filetypes;
}

sub get_filetypes_table()
{
	my $filetypes = get_filetypes_hash();
	my %filegroups = FILEGROUPS;
	my $filesizes = $board->option('FILESIZES');
	my @groups = split(' ', GROUPORDER);
	my @rows;
	my $blocks = 0;
	my $output = '<table style="margin:0px;border-collapse:collapse;display:inline-table;">' . "\n<tr>\n\t" . '<td colspan="4">'
		. sprintf(S_ALLOWED, get_displaysize($board->option('MAX_KB')*1024, DECIMAL_MARK, 0)) . "</td>\n</tr><tr>\n";
	delete $$filetypes{'jpeg'}; # show only jpg

	foreach my $group (@groups) {
		my @extensions;
		foreach my $ext (keys %$filetypes) {
			if ($$filetypes{$ext} eq $group or $group eq 'other') {
				my $ext_desc = uc($ext);
				$ext_desc .= ' (' . get_displaysize($$filesizes{$ext}*1024, DECIMAL_MARK, 0) . ')' if ($$filesizes{$ext});
				push(@extensions, $ext_desc);
				delete $$filetypes{$ext};
			}
		}
		if (@extensions) {
			$output .= "\t<td><strong>" . $filegroups{$group} . ":</strong>&nbsp;</td>\n\t<td>"
				. join(", ", sort(@extensions)) . "&nbsp;&nbsp;</td>\n";
			$blocks++;
			if (!($blocks % 2)) {
				push(@rows, $output);
				$output = '';
				$blocks = 0;
			}
		}
	}
	push(@rows, $output) if ($output);
	return join("</tr><tr>\n", @rows) . "</tr>\n</table>";
}

sub parse_range($$)
{
	my ($ip,$mask)=@_;

	$ip=dot_to_dec($ip) if($ip=~/^\d+\.\d+\.\d+\.\d+$/);

	if($mask=~/^\d+\.\d+\.\d+\.\d+$/) { $mask=dot_to_dec($mask); }
	elsif($mask=~/(\d+)/) { $mask=(~((1<<$1)-1)); }
	else { $mask=0xffffffff; }

	return ($ip,$mask);
}

sub get_remote_addr() # untested with x-real-ip
{
	return($ENV{HTTP_CF_CONNECTING_IP} || $ENV{HTTP_X_REAL_IP} || $ENV{REMOTE_ADDR});
}


#
# Database utils
#

sub init_counters($)
{
	my ($brd) = @_;
	my ($sth);

	eval {
		$dbh->begin_work();

		$sth=$dbh->prepare("INSERT INTO `".SQL_COUNTERS_TABLE."` VALUES (?,?);");
		$sth->execute($brd,0);
		$sth->finish();

		$dbh->commit();
	};
	if ($@) {
		$dbh->rollback();
		make_error(S_SQLFAIL);
	}
}

sub init_database()
{
	my ($sth);

	eval { $dbh->do("DROP TABLE `".SQL_TABLE."`;") } or do {};
	eval { $dbh->do("DROP TABLE `".SQL_COUNTERS_TABLE."`;") } or do {};

	eval {
		$dbh->begin_work();
		
		$sth=$dbh->prepare("CREATE TABLE `".SQL_TABLE."` (".
		"num INTEGER,".				# Post number, retreive it from counters
		"parent INTEGER,".			# Parent post for replies in threads. For original posts, must be set to 0 (and not null)
		"timestamp INTEGER,".		# Timestamp in seconds for when the post was created
		"lasthit INTEGER,".			# Last activity in thread. Must be set to the same value for BOTH the original post and all replies!
		"ip ".get_sql_ip().",".					# IP number of poster, in integer form!

		"id TEXT,". 				# Unique id
		"name TEXT,".				# Name of the poster
		"trip TEXT,".				# Tripcode (encoded)
		"email TEXT,".				# Email address
		"subject TEXT,".			# Subject
		"password TEXT,".			# Deletion password (in plaintext) 
		"comment TEXT,".			# Comment text, HTML encoded.

		"image TEXT,".				# Image filename with path and extension (IE, src/1081231233721.jpg)
		"size INTEGER,".			# File size in bytes
		"md5 TEXT,".				# md5 sum in hex
		"width INTEGER,".			# Width of image in pixels
		"height INTEGER,".			# Height of image in pixels
		"thumbnail TEXT,".			# Thumbnail filename with path and extension
		"tn_width TEXT,".			# Thumbnail width in pixels
		"tn_height TEXT,".			# Thumbnail height in pixels
		"origname TEXT,".
		"meta TEXT,".

		"adminpost TEXT,".
		"admin_post INTEGER,".
		"sticky INTEGER,".
		"locked TEXT,".
		"autosage TEXT,".
		"location TEXT,".
		"aluhut TEXT,".
		"section CHAR(64)".		# Board section

		");");
		$sth->execute();

		$sth=$dbh->prepare("CREATE TABLE `".SQL_COUNTERS_TABLE."` (".
		"section CHAR(64),".			# Board section
		"counter INTEGER".			# Messages counter
		");");
		$sth->execute();
		$sth->finish();

		$dbh->commit();
	};
	if ($@) {
		$dbh->rollback();
		make_error(S_SQLFAIL);
	}
}

sub init_admin_database()
{
	my ($sth);

	eval { $dbh->do("DROP TABLE `".SQL_ADMIN_TABLE."`;") } or do {};

	eval {
		$dbh->begin_work();

		$sth=$dbh->prepare("CREATE TABLE `".SQL_ADMIN_TABLE."` (".

		"num INTEGER,".				# Post number, retreive it from counters
		"type TEXT,".				# Type of entry (ipban, wordban, etc)
		"comment TEXT,".			# Comment for the entry
		"ival1 ".get_sql_ip().",".			# Integer value 1 (usually IP)
		"ival2 ".get_sql_ip().",".			# Integer value 2 (usually netmask)
		"sval1 TEXT,".				# String value 1
		"date INTEGER".				# String value 1

		");");
		$sth->execute();

		$sth=$dbh->prepare("INSERT INTO `".SQL_COUNTERS_TABLE."` VALUES (?,?);");
		$sth->execute('admin',0);
		$sth->finish();

		$dbh->commit();
	};
	if ($@) {
		$dbh->rollback();
		make_error(S_SQLFAIL);
	}
}

sub init_moder_database {
	my ($sth);

	eval { $dbh->do("DROP TABLE `".SQL_MODER_TABLE."`;") } or do {};

	eval {
		$dbh->begin_work();

		$sth = $dbh->prepare("CREATE TABLE `".SQL_MODER_TABLE."` (".
		"num INTEGER,".# Entry number, from counters table
		"username TEXT,". 	#
		"pass TEXT," .       # 
		"class TEXT,". 	#
		"boards TEXT," .       # 
		"timestamp INTEGER," .        # Human-readable form of date          
		"lastip ".get_sql_ip().",".                         # A "serialized" array of the last IPs to log in
		"lasthit INTEGER,".                 # The last time the user logged on. If 3 months pass without another logon, the pass will be deactivated
		"disabled INTEGER".    #

	   ");");
		$sth->execute();

		$sth=$dbh->prepare("INSERT INTO `".SQL_COUNTERS_TABLE."` VALUES (?,?);");
		$sth->execute('moders',0);
		$sth->finish();

		$dbh->commit();
	};
	if ($@) {
		$dbh->rollback();
		make_error(S_SQLFAIL);
	}
}

sub init_common_site_database() # Index of all the boards sharing the same imageboard site.
{
	my ($sth);
	
	$sth=$dbh->do("DROP TABLE `".SQL_COMMON_SITE_TABLE."`;") if table_exists(SQL_COMMON_SITE_TABLE);
	$sth=$dbh->prepare("CREATE TABLE `".SQL_COMMON_SITE_TABLE."` (".
	"board VARCHAR(25) PRIMARY KEY NOT NULL UNIQUE,".	# Name of comment table
	"type TEXT,".						# Corresponding board type? (Later use)
	"name TEXT,".						# Board name
	"hidden TEXT".						# Hidden board?
	");") or make_error(S_SQLFAIL);				# And that's it. Hopefully this is a more efficient solution than handling it all in code or a text file.
	
	$sth->execute() or make_error(S_SQLFAIL);
	$sth->finish();
}

sub init_backup_database()
{
	my ($sth);
	
	eval { $dbh->do("DROP TABLE `".SQL_BACKUP_TABLE."`;") } or do {};

	eval {
		$dbh->begin_work();

		$sth=$dbh->prepare("CREATE TABLE `".SQL_BACKUP_TABLE."` (".
		"num INTEGER,".	# Primary key, auto-increments
		"board_name VARCHAR(25) NOT NULL,".	# Board name
		"postnum INTEGER,".			# Post number
		"parent INTEGER,".			# Parent post for replies in threads. For original posts, must be set to 0 (and not null)
		"timestamp INTEGER,".			# Timestamp in seconds for when the post was created
		"lasthit INTEGER,".			# Last activity in thread. Must be set to the same value for BOTH the original post and all replies!
		"ip ".get_sql_ip().",".				# IP number of poster, in integer form!

		"id TEXT,". 				# Unique id
		"name TEXT,".				# Name of the poster
		"trip TEXT,".				# Tripcode (encoded)
		"email TEXT,".				# Email address
		"subject TEXT,".			# Subject
		"password TEXT,".			# Deletion password (in plaintext) 
		"comment TEXT,".			# Comment text, HTML encoded.

		"image TEXT,".				# Image filename with path and extension (IE, src/1081231233721.jpg)
		"size INTEGER,".			# File size in bytes
		"md5 TEXT,".				# md5 sum in hex
		"width INTEGER,".			# Width of image in pixels
		"height INTEGER,".			# Height of image in pixels
		"thumbnail TEXT,".			# Thumbnail filename with path and extension
		"tn_width TEXT,".			# Thumbnail width in pixels
		"tn_height TEXT,".			# Thumbnail height in pixels
		"origname TEXT,".

		"adminpost TEXT,".			# ADDED - Admin post?
		"admin_post INTEGER,".
		"sticky INTEGER,".			# ADDED - Stickied?
		"locked TEXT,".				# ADDED - Locked?
		"autosage TEXT,".
		"location TEXT".
		"aluhut TEXT,".
		"timestampofarchival INTEGER".		# When was this backed up?
		");");	

		$sth->execute();

		$sth=$dbh->prepare("INSERT INTO `".SQL_COUNTERS_TABLE."` VALUES (?,?);");
		$sth->execute('backups',0);
		$sth->finish();

		$dbh->commit();
	};
	if ($@) {
		$dbh->rollback();
		make_error(S_SQLFAIL);
	}

}

sub repair_database()
{
	my ($sth,$row,@threads,$thread);

	$sth=$dbh->prepare("SELECT * FROM `".SQL_TABLE."` WHERE parent=0 AND section=?;") or make_error(S_SQLFAIL);
	$sth->execute($board->path) or make_error(S_SQLFAIL);

	while($row=$sth->fetchrow_hashref()) { push(@threads,$row); }

	foreach $thread (@threads)
	{
		# fix lasthit
		my ($upd);

		$upd=$dbh->prepare("UPDATE `".SQL_TABLE."` SET lasthit=? WHERE parent=? AND section=?;") or make_error(S_SQLFAIL);
		$upd->execute($$thread{lasthit},$$thread{num},$board->path()) or make_error(S_SQLFAIL." ".$dbh->errstr());
	}
	$sth->finish();

	# my $sth2=$dbh->prepare("ALTER TABLE `".SQL_TABLE."` ADD COLUMN id TEXT AFTER ip;");
	# $sth2->execute();
	# $sth2->finish();
}

sub get_sql_autoincrement()
{
	# no pg support.....
	# return 'SERIAL PRIMARY KEY' if(SQL_DBI_SOURCE=~/^DBI:Pg:/i);
	return 'INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT' if(SQL_DBI_SOURCE=~/^DBI:mysql:/i);
	return 'INTEGER PRIMARY KEY' if(SQL_DBI_SOURCE=~/^DBI:SQLite:/i);
	return 'INTEGER PRIMARY KEY' if(SQL_DBI_SOURCE=~/^DBI:SQLite2:/i);

	make_error(S_SQLCONF); # maybe there should be a sane default case instead?
}

sub get_sql_ip()
{
	# no pg support...
	# return 'bigint' if(SQL_DBI_SOURCE=~/^DBI:Pg:/i);
	return 'TEXT' if(SQL_DBI_SOURCE=~/^DBI:mysql:/i);
	return 'TEXT' if(SQL_DBI_SOURCE=~/^DBI:SQLite:/i);
	return 'TEXT' if(SQL_DBI_SOURCE=~/^DBI:SQLite2:/i);

	make_error(S_SQLCONF); # maybe there should be a sane default case instead?
}

sub trim_database()
{
	my ($sth,$row,$order);

	if($board->option('TRIM_METHOD')==0) { $order='num ASC'; }
	else { $order='lasthit ASC'; }

	if($board->option('MAX_AGE') > 0) # needs testing
	{
		my $mintime=time()-($board->option('MAX_AGE'))*3600;

		$sth=$dbh->prepare("SELECT * FROM `".SQL_TABLE."` WHERE parent=0 AND timestamp<=$mintime AND sticky=0 AND section=?;") or make_error(S_SQLFAIL);
		$sth->execute($board->path()) or make_error(S_SQLFAIL);

		while($row=$sth->fetchrow_hashref())
		{
			delete_post($$row{num},"",0,0);
		}
	}

	my $threads=count_threads();
	my ($posts,$size)=count_posts();
	my $max_threads=($board->option('MAX_THREADS') > 0) ? ($board->option('MAX_THREADS')) : $threads;
	my $max_posts=($board->option('MAX_POSTS') > 0) ? ($board->option('MAX_POSTS')) : $posts;
	my $max_size=($board->option('MAX_MEGABYTES') > 0) ? ($board->option('MAX_MEGABYTES')*1024*1024) : $size;

	while($threads>$max_threads or $posts>$max_posts or $size>$max_size)
	{
		$sth=$dbh->prepare("SELECT * FROM `".SQL_TABLE."` WHERE parent=0 AND sticky=0 AND section=? ORDER BY $order LIMIT 1;") or make_error(S_SQLFAIL);
		$sth->execute($board->path()) or make_error(S_SQLFAIL);

		if($row=$sth->fetchrow_hashref())
		{
			my ($threadposts,$threadsize)=count_posts($$row{num});

			delete_post($$row{num},"",0,0);

			$threads--;
			$posts-=$threadposts;
			$size-=$threadsize;
		}
		else { last; } # shouldn't happen
	}
	
	if ($sth) {$sth->finish();}
}

sub table_exists($)
{
	my ($table)=@_;
	eval { $dbh->do("SELECT * FROM `".$table."` LIMIT 1;") } or do { return 0 };
	return 1
}

sub counter_exists()
{
	my ($sth,$brd);
	my $ret = 0;
	$brd = $board->path();
   
	$sth = $dbh->prepare( "SELECT * FROM `".SQL_COUNTERS_TABLE."` WHERE section=?;" );
	$sth->execute($brd);

	my $exists = ($sth->fetchrow_array())[0];
	$ret = 1 if($exists);
	$sth->finish();

	return $ret;
}

sub count_all_posts {
	my ( $sth, $where );

	$sth = $dbh->prepare("SELECT COUNT(`num`) FROM `" . SQL_TABLE . "` WHERE section=?;") or make_error(S_SQLFAIL);
	$sth->execute($board->path()) or make_error(S_SQLFAIL);
	my $return = ($sth->fetchrow_array())[0];
	$sth->finish();

	return $return;
}

sub count_threads()
{
	my ($sth);

	$sth=$dbh->prepare("SELECT count(*) FROM `".SQL_TABLE."` WHERE parent=0 AND section=?;") or make_error(S_SQLFAIL);
	$sth->execute( $board->path() ) or make_error(S_SQLFAIL);
	my $return = ($sth->fetchrow_array())[0];
	$sth->finish();

	return $return;
}

sub count_posts(;$)
{
	my ($parent)=@_;
	my ($sth,$where);
	my $brd=$board->path();

	$where="WHERE section=$brd AND (parent=$parent or num=$parent)" if($parent);
	$sth=$dbh->prepare("SELECT count(*),sum(size) FROM `".SQL_TABLE."` $where;") or make_error(S_SQLFAIL);
	$sth->execute() or make_error(S_SQLFAIL);
	my @return = ($sth->fetchrow_array());
	$sth->finish();

	return @return;
}

sub thread_exists($)
{
	my ($thread)=@_;
	my ($sth);

	$sth=$dbh->prepare("SELECT count(*) FROM `".SQL_TABLE."` WHERE section=? AND num=? AND parent=0;") or make_error(S_SQLFAIL);
	$sth->execute($board->path,$thread) or make_error(S_SQLFAIL);
	my $return = ($sth->fetchrow_array())[0];
	$sth->finish();

	return $return;
}

sub get_decoded_hashref($)
{
	my ($sth)=@_;

	my $row=$sth->fetchrow_hashref();

	if($row)
	{
		for my $k (keys %$row) # don't blame me for this shit, I got this from perlunicode.
		{ defined && /[^\000-\177]/ && Encode::_utf8_on($_) for $row->{$k}; }
	}

	return $row;
}

sub get_decoded_arrayref($)
{
	my ($sth)=@_;

	my $row=$sth->fetchrow_arrayref();

	if($row)
	{
		# don't blame me for this shit, I got this from perlunicode.
		defined && /[^\000-\177]/ && Encode::_utf8_on($_) for @$row;
	}

	return $row;
}

#
# Pedal additions
#

sub find_posts($$$$$) {
	my ($admin, $find, $op_only, $in_subject, $in_filenames, $in_comment) = @_;

	$find = clean_string(decode_string($find, CHARSET));
	$find =~ s/^\s+|\s+$//g; # trim
	$in_comment = 1 unless $find; # make the box checked for the first call.	

	my ($sth, $row);
	my ($search, $subject);
	my $lfind = lc($find); # ignore case
	my $count = 0;
	my $threads = 0;
	my @results = ();

	if (length($lfind) >= 3) {
		# grab all posts, in thread order (ugh, ugly kludge)
		$sth = $dbh->prepare("SELECT * FROM `" . SQL_TABLE . "` WHERE section=? ORDER BY sticky DESC,lasthit DESC,CASE parent WHEN 0 THEN num ELSE parent END ASC,num ASC") or make_error(S_SQLFAIL);
		$sth->execute($board->path()) or make_error(S_SQLFAIL);

		while (($row = get_decoded_hashref($sth)) and ($count < MAX_SEARCH_RESULTS) and ($threads <= $board->option('MAX_SHOWN_THREADS'))) {
			$threads++ if !$$row{parent};
			$search = $$row{comment};
			$search =~ s/<.+?>//mg; # must not search inside html-tags. remove them.
			$search = lc($search);
			$subject = lc($$row{subject});

			if (($in_comment and (index($search, $lfind) > -1)) or ($in_subject and (index($subject, $lfind) > -1))) {
				$$row{comment} = resolve_reflinks($$row{comment});
				$$row{search} = 1;

				if (!$$row{parent}) { # OP post
					# $$row{sticky_isnull} = 1; # hack, until this field is removed.
					push @results, $row;
				} else { # reply post
					push @results, $row unless ($op_only);
				}

				$count = @results;
			}
		}
		$sth->finish(); # Clean up the record set
	}

	make_http_header();
	my $output =
		encode_string(
			SEARCH_TEMPLATE->(
				title		=> S_SEARCHTITLE,
				posts		=> \@results,
				find		=> $find,
				oponly		=> $op_only,
				insubject	=> $in_subject,
				filenames	=> $in_filenames,
				comment		=> $in_comment,
				count		=> $count,
				admin		=> 0,
				board 		=> $board,
				stylesheets => get_stylesheets()
			)
		);

	$output =~ s/^\s+\n//mg;
	print($output);
}

sub get_max_sticked() 
{
	my $max = 0;
	
	# grab all posts from DB
	my $sth=$dbh->prepare("SELECT * FROM `".SQL_TABLE."` WHERE section=? ORDER BY sticky DESC, lasthit DESC, CASE parent WHEN 0 THEN num ELSE parent END ASC, num ASC") or make_error(S_SQLFAIL);
	$sth->execute($board->path()) or make_error(S_SQLFAIL);	
	
	my $row = get_decoded_hashref($sth);
	if (!$row) { return 0; }
	
	# Calculate maximum `sticky` value
	while ($row = get_decoded_hashref($sth)) 
	{
		$max = $$row{sticky} if ($$row{sticky} > $max);
	}
	$sth->finish();
	
	return $max;
}

sub thread_control($$$)
{
	my ($admin,$num,$action)=@_;
	my ($sth,$row);
	my $max_sticky = (get_max_sticked() + 1);

	check_password($admin,'');

	$sth = $dbh->prepare("SELECT * FROM `" . SQL_TABLE . "` WHERE num=? AND section=?;") or make_error(S_SQLFAIL);
	$sth->execute( $num, $board->path() ) or make_error(S_SQLFAIL);

	if ($row = $sth->fetchrow_hashref()) {
		my ($sth2,$check);
		if($action eq 'sticky') {
			$check = $$row{sticky} ? 0 : $max_sticky;
			$sth2 = $dbh->prepare("UPDATE `" . SQL_TABLE . "` SET sticky=? WHERE section=? AND (num=? OR parent=?);") or make_error(S_SQLFAIL);
		}
		elsif($action eq 'lock') {
			$check = $$row{locked} eq 1 ? 0 : 1;
			$sth2 = $dbh->prepare("UPDATE `" . SQL_TABLE . "` SET locked=? WHERE section=? AND (num=? OR parent=?);") or make_error(S_SQLFAIL);
		}
		elsif($action eq 'autosage') {
			$check = $$row{autosage} eq 1 ? 0 : 1;
			$sth2 = $dbh->prepare("UPDATE `" . SQL_TABLE . "` SET autosage=? WHERE section=? AND (num=? OR parent=?);") or make_error(S_SQLFAIL);
		}
		$sth2->execute($check,$board->path(),$num,$num) or make_error(S_SQLFAIL);
		$sth2->finish();
	}

	$sth->finish();

	make_http_forward(get_secure_script_name()."?task=mpanel&board=".$board->path());
}

#
# JSON Functions
#

sub hide_row_els($) {
	my ($row) = @_;

	$$row{'comment'} = resolve_reflinks($$row{'comment'});
	$$row{'location'} = "" if(!$board->option('ENABLE_COUNTRY_FLAGS'));
	delete @$row {'adminpost', 'password', 'ip'};
}

sub get_boardspeed()
{
	my $ret;

	my $sth = $dbh->prepare(
		"SELECT COUNT(`num`) AS cnt FROM "
		. SQL_TABLE
		. " WHERE timestamp >= UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 1 HOUR));"
	) or make_sql_error();
	$sth->execute() or make_sql_error();
	$ret = ($sth->fetchrow_array())[0];

	return $ret if $ret;
	return 0;
}

sub get_boardconfig(;$$)
{
	my ($standalone, $captcha_only) = @_;
	my $loc = get_geolocation(get_remote_addr());
	my %result;

	my %boardinfo = (
		board => $board->path(),
		board_name => $board->option('BOARD_NAME'),
		board_desc => $board->option('BOARD_DESC'),
		board_cat => $board->option('BOARD_CAT'),
		board_speed => get_boardspeed(),
		config => {
			names_allowed => !$board->option('FORCED_ANON'),
			posting_allowed => ( $board->option('ALLOW_TEXT_REPLIES') or $board->option('ALLOW_IMAGE_REPLIES') ),
			image_replies => $board->option('ALLOW_IMAGE_REPLIES'),
			image_op => $board->option('ALLOW_IMAGES'),
			max_files => 1, #later
			max_res => $board->option('MAX_RES'),
			max_field_length => $board->option('MAX_FIELD_LENGTH'),
			max_comment_length => $board->option('MAX_COMMENT_LENGTH'),
			default_name => $board->option('S_ANONAME'),
			captcha => use_captcha($board->option('ENABLE_CAPTCHA'), $loc),
			geoip_enabled => $board->option('ENABLE_COUNTRY_FLAGS'),
		}
	);
	return \%boardinfo unless $standalone;

	make_json_header();
	if(defined $captcha_only) {
		%result = ( captcha => $boardinfo{'config'}->{captcha} );
	} else {
		%result = ( %boardinfo );
	}
	print $JSON->canonical(1)->encode(\%result);
}


sub output_json_threads {
	my ($pageToShow) = @_;
	my $page = 1;
	my ($sth, $row, $error, $code, $status, $boardinfo, @threads, %json);
	my ($sth2, $row2);
	my $code = 200;

	my $thread_count = count_threads();
	my $total_pages = get_page_count($thread_count);

	$error = encode_entities(decode('utf8', $dbh->errstr));
	# $row = get_decoded_hashref($sth);

	hide_row_els($row);

	$status = {
		"error_code" => $code,
		"error_msg" => $error,
	};

	if($thread_count > 0)
	{
		$sth = $dbh->prepare(
			    "SELECT * FROM "
			  . SQL_TABLE
			  . " WHERE section=? AND parent=0 ORDER BY sticky DESC,lasthit DESC,num ASC"
			  . " LIMIT ?,?"
		) or make_error(S_SQLFAIL);
		$sth->execute( $board->path(), $board->option('IMAGES_PER_PAGE') * ($pageToShow - 1), $board->option('IMAGES_PER_PAGE') ) or make_error(S_SQLFAIL);

		# make_error(S_INVALID_PAGE, 1) if ($pageToShow > $total_pages or $pageToShow <= 0);
		my (@threads, @thread);

		while ($row = get_decoded_hashref($sth)) {
			@thread = ($row);

			# add posts to thread
			$sth2 = $dbh->prepare(
				    "SELECT * FROM "
				  . SQL_TABLE
				  . " WHERE section=? AND parent=? ORDER BY num ASC"
			);
			$sth2->execute($board->path(), $$row{num});

			while ($row2 = get_decoded_hashref($sth2)) {
				push @thread, $row2;
			}

			push @threads, { posts => [@thread] };
		}

		$sth->finish();

		output_json_page($page, $total_pages, $status, @threads);
	}
	else
	{
		$$status{error_code} = 404;
		# $error = 'Element not found.';
		output_json_page( 1, 1, $status, () );
	}

	# make_json_header();
	# print $JSON->encode(\%json);
}

sub output_json_page {
	my ($page, $total, $status, @threads) = @_;
	my (%json);

	# do abbrevations and such
	foreach my $thread (@threads) {
		# split off the parent post, and count the replies and images
		my ( $parent, @replies ) = @{ $$thread{posts} };
		my $replies = @replies;

		my $images = grep { $$_{image} } @replies;

		my $curr_replies = $replies;
		my $curr_images  = $images;
		my $max_images   = $images;
		my $max_replies;
		# Abbreviation setings -- Stickied threads have their own setting
		if ($$parent{sticky})
		{
			$max_replies=REPLIES_PER_STICKY;
		}
		else
		{
			$max_replies=$board->option('REPLIES_PER_THREAD');
		}

		# drop replies until we have few enough replies and images
		while ( $curr_replies > $max_replies or $curr_images > $max_images ) {
			my $post = shift @replies;
			$curr_images-- if $$post{image};
			$curr_replies--;
		}

		# write the shortened list of replies back
		$$thread{posts}      = [ $parent, @replies ];
		$$thread{omit}=$replies-$curr_replies;
		$$thread{omitimages}=$images-$curr_images;
		$$thread{num}	     = ${$$thread{posts}}[0]{num};

		# abbreviate the remaining posts
		foreach ( @{ $$thread{posts} } ) {
			# create ref-links
			$$_{comment} = resolve_reflinks($$_{comment});

			my $abbreviation =
			  abbreviate_html( $$_{comment}, $board->option('MAX_LINES_SHOWN'),
				$board->option('APPROX_LINE_LENGTH') );
			if ($abbreviation) {
                $$_{comment_full} = $$_{comment};
				$$_{comment} = $abbreviation;
			}
		}
	}

	# make the list of pages
	my @pages = map +{ page => $_ }, ( 1 .. $total );

	foreach my $p (@pages) {
		$$p{filename} = expand_filename( $$p{page} . $page_ext );
		if ( $$p{page} == $page ) { $$p{current} = 1 }   # current page, no link
	}

	my ( $prevpage, $nextpage );
	$prevpage = $pages[ $page - 2 ]{filename} if ( $page != 1 );
	$nextpage = $pages[ $page     ]{filename} if ( $page != $total );

	%json = (
		"data" => \@threads,
		"pages" => \@pages,
		"status" => \%$status,
		"boardinfo" => get_boardconfig(),
	);

	make_json_header();
	print $JSON->canonical(1)->encode(\%json);
}

sub output_json_thread {
	my ($id) = @_;
	my ($sth, $row, $error, $code, %status, @thread, %json);

	$sth = $dbh->prepare("SELECT * FROM `" . SQL_TABLE . "` WHERE section=? AND (num=? OR parent=?) ORDER BY num ASC;");
	$sth->execute($board->path(), $id, $id);
	$error = encode_entities(decode('utf8', $sth->errstr));
	# $row = $sth->fetchrow_hashref();

	while($row=get_decoded_hashref($sth)) {
		hide_row_els($row);
		push(@thread, $row);
	}

	if ( !$thread[0] or $thread[0]{parent} ) {
		$code = 404;
		$error = 'Element not found.';
	}
	elsif (@thread) {
		$code = 200;
	}
	else {
		$code = 500;
	}
	$sth->finish();


	%status = (
		"error_code" => $code,
		"error_msg" => $error,
	);

	%json = (
		"data" => ( $code == 200 ? \@thread : [] ),
		"status" => \%status,
		"boardinfo" => get_boardconfig(),
	);

	make_json_header();
	print $JSON->encode(\%json);
}

sub output_json_newposts {
	my ($id, $after, $single) = @_;
	my ($sth, $row, $error, $code, %status, @posts, %json);

	if(!$single) {
		$sth = $dbh->prepare("SELECT * FROM `" . SQL_TABLE . "` WHERE section=? AND (parent=? and num>?) ORDER BY num ASC;");
		$sth->execute($board->path(), $id, $after);
	} else {
		$sth = $dbh->prepare("SELECT * FROM `" . SQL_TABLE . "` WHERE section=? AND num=? ORDER BY num ASC;");
		$sth->execute($board->path(), $id);
	}
	$error = encode_entities(decode('utf8', $sth->errstr));

	while($row=get_decoded_hashref($sth)) {
		hide_row_els($row);
		push(@posts, $row);
	}

	if(@posts) {
		$code = 200;
	}
	elsif(@posts < 1) {
		$code = 404;
		$error = 'Element not found.';
	} else {
		$code = 500;
	}
	$sth->finish();


	%status = (
		"error_code" => $code,
		"error_msg" => $error,
	);

	%json = (
		"data" => \@posts,
		"status" => \%status,
		"boardinfo" => get_boardconfig(),
	);

	make_json_header();
	print $JSON->encode(\%json);
}

sub json_find_posts($$$$) {
	my ($find, $op_only, $in_subject, $in_comment) = @_;
	my ($error, $code, %status, %json);

	$find = clean_string(decode_string($find, CHARSET));
	$find =~ s/^\s+|\s+$//g; # trim
	$in_comment = 1 unless $find; # make the box checked for the first call.	

	my ($sth, $row);
	my ($search, $subject);
	my $lfind = lc($find); # ignore case
	my $count = 0;
	my $threads = 0;
	my @results = ();

	if (length($lfind) >= 3) {
		# grab all posts, in thread order (ugh, ugly kludge)
		$sth = $dbh->prepare("SELECT * FROM `" . SQL_TABLE . "` WHERE section=? ORDER BY sticky DESC,lasthit DESC,CASE parent WHEN 0 THEN num ELSE parent END ASC,num ASC") or make_error(S_SQLFAIL);
		$sth->execute($board->path()) or make_error(S_SQLFAIL);

		while (($row = get_decoded_hashref($sth)) and ($count < MAX_SEARCH_RESULTS) and ($threads <= $board->option('MAX_SHOWN_THREADS'))) {
			$threads++ if !$$row{parent};
			$search = $$row{comment};
			$search =~ s/<.+?>//mg; # must not search inside html-tags. remove them.
			$search = lc($search);
			$subject = lc($$row{subject});

			if (($in_comment and (index($search, $lfind) > -1)) or ($in_subject and (index($subject, $lfind) > -1))) {
				$$row{comment} = resolve_reflinks($$row{comment});
				$$row{search} = 1;

				if (!$$row{parent}) { # OP post
					# $$row{sticky_isnull} = 1; # hack, until this field is removed.
					push @results, $row;
				} else { # reply post
					push @results, $row unless ($op_only);
				}

				$count = @results;
			}
		}
		$sth->finish(); # Clean up the record set
	}
	else {
		$error = "Search query is too short";
		$code = 404;
	}

	if(@results) {
		$code = 200;
	}
	elsif(@results < 1) {
		$code = 404;
		$error = 'Element not found.';
	} else {
		$code = 500;
	}

	%status = (
			"error_code" => $code,
			"error_msg" => $error,
	);
	%json = (
			"data" => \@results,
			"found" => $count,
			"status" => \%status,
	);

	make_json_header();
	print $JSON->encode(\%json);
}

sub output_json_postcount {
	my ($id) = @_;
	my ($row, $error, $code, %status, %json);

	my $sth = $dbh->prepare("SELECT COUNT(`num`) as postcount FROM `" . SQL_TABLE . "` WHERE section=? AND (parent=? OR num=?) ORDER BY num ASC;");
	$sth->execute( $board->path(), $id, $id );
	$error = encode_entities(decode('utf8', $sth->errstr));
	$row=$sth->fetchrow_hashref();

	if($row) {
		$code = 200;
	}
	elsif($sth->rows eq 0) {
		$code = 404;
		$error = 'Element not found.';
	} else {
		$code = 500;
	}
	%status = (
			"error_code" => $code,
			"error_msg" => $error,
	);
	%json = (
			"data" => $row,
			"status" => \%status,
	);
	$sth->finish();

	make_json_header();
	print $JSON->encode(\%json);
}

sub output_json_allpostcount {
	my ($time) = @_;
	my ($row, $error, $code, %status, %data, %json);
	$time = time() unless($time or $time=~ /[^0-9]/);

	my $sth = $dbh->prepare("SELECT COUNT(1) FROM `" . SQL_TABLE . "` WHERE timestamp>? AND section=?;");
	$sth->execute( $time, $board->path() );
	$error = encode_entities(decode('utf8', $sth->errstr));
	$row=$sth->fetch();

	my $all = count_all_posts();
	$data{'posts'} = $all;
	$data{'new'} = $$row[0];

	if($row ne undef) {
		$code = 200;
	}
	elsif($sth->rows eq 0) {
		$code = 404;
		$error = 'Element not found.';
	} else {
		$code = 500;
	}
	%status = (
			"error_code" => $code,
			"error_msg" => $error,
	);
	%json = (
			"data" => \%data,
			"status" => \%status,
	);
	$sth->finish();

	make_json_header();
	print $JSON->encode(\%json);
}

sub output_json_boardlist()
{
    my %status = (
        error_msg => '',
        error_code => 200,
    );

    make_json_header();
    print $JSON->encode({
    	boards => get_boards_list(),
    	categories => [split(/\|/, BOARD_CATORDER)],
    	status => \%status
    });
}

#
# RSS Management
#

sub make_rss()
{
	my ($sth, $row);
	my (@items);

	# Retrieve records to be inserted into RSS.
	$sth=$dbh->prepare("SELECT * FROM `".SQL_TABLE."` where section=? ORDER BY timestamp DESC LIMIT ".RSS_LENGTH.";") or make_error(S_SQLFAIL);
	$sth->execute($board->path) or make_error(S_SQLFAIL);

	while ($row=get_decoded_hashref($sth))
	{
		if ($$row{image})
		{
			my $extension = $$row{image};
			$extension =~ s/^.*\.([^\.]+)$/$1/;

			my $mime_types = MIME::Types->new(only_complete => 1);
			my $mime_type_obj = $mime_types->mimeTypeOf($extension);

			if ($mime_type_obj)
			{
				$$row{mime_type} = $mime_type_obj->type();
			}
			else
			{
				# Generic fallback.
				$$row{mime_type} = "application/octet-stream";
			}
		}

		$$row{comment} = resolve_reflinks($$row{comment});

		push(@items, $row);
	}

	# Construct the RSS out of post data, using the usual template approach.
	make_rss_header();
	print encode_string(RSS_TEMPLATE->(
		items=>\@items,
		pub_date=>make_date(time, "http"), # Date RSS was generated.
		board=>$board));

	$sth->finish();
}
