#!/bin/sh
FILE_BACK="wakaba.txz"
user="wat"
pass="wat"
find . -type f -regextype posix-extended -regex ".*.(tpl|css|html|tt2|js|pl)" -print0 | xargs -0 -n 1 -P 4 dos2unix
rm -v other/nyan_toho.txz
chmod +r -v other/nyaha.sql
mysqldump -u$user --password=$pass toho_waka > other/meow.sql
time -f "\n%e real\n" tar cf - \
 * \
 /etc/nginx \
 /etc/mysql/my.cnf \
 --exclude=*.tar \
 --exclude=*.tbz2 \
 --exclude=*.tgz \
 --exclude=*/src/* \
 --exclude=*/orphans/*/* \
 --exclude=$FILE_BACK | pv -s $(du -sb other/$FILE_BACK | awk '{print $1}') | xz -9 > other/$FILE_BACK
chmod -r -v other/meow.sql
