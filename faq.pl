#!/usr/bin/perl

use strict;
use CGI;
use Template;
use HTML::Entities;
use Encode;
use Time::Local;
use utf8;

my $q = CGI->new;
my $page = encode_entities(decode('utf8', $q->param("p")));
my $isframe = 0;

# redirects should have a full URL: http://ernstchan.com/b/
# but this can be tricky if running behind some proxy
if ($page eq "") {
	exit print $q->redirect('/hs/');
}


print $q->header(-charset => 'utf-8');
# print $q->header();

my $tt = ( $isframe ? Template->new({#'[p]'
	INCLUDE_PATH => 'tpl/',
	ERROR => 'error.tt2',
	ENCODING     => 'utf8',
    }) : 
	Template->new({#'[p]'
		INCLUDE_PATH => 'tpl/',
		ERROR => 'error.tt2',
		PRE_PROCESS  => 'header.tt2',
		POST_PROCESS => 'footer.tt2',
		ENCODING     => 'utf8',
    }) ) || print Template->error(), "\n";

my $ttfile = "content/" . $page . ".tt2";

binmode STDOUT, ":utf8";
if (-e 'tpl/' . $ttfile) {
	$tt->process($ttfile, {}) or die($tt->process("error.tt2", {'error' => $tt->error}));
} else {
	die($tt->process("error.tt2", {'error' => {'type' => "HTTP 404", 'info' => "Запрошенный файл не существует или был удален."}}));
}
